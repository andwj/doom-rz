;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

const VERSION_SIZE    = 8
const VERSION_STRING  = "doom-rz\x01"

zero-var save_stream    ^FILE
zero-var savegame_error  bool

; Location where savegames are stored
zero-var savegamedir  ^uchar

zero-var tempsavefile ^uchar
zero-var currsavefile ^uchar

;
; Get the filename of a temporary file to write the savegame to.
; After the file has been successfully saved, it will be renamed to
; the real file.
;
fun P_TempSaveGameFile (-> ^uchar)
	if null? [tempsavefile]
		[tempsavefile] = M_StringJoin [savegamedir] "temp.dsg"
	endif

	return [tempsavefile]
end

;
; Get the filename of the save game file to use for the specified slot.
;
fun P_SaveGameFile (slot s32 -> ^uchar)
	basename = stack-var [32]uchar
	M_FormatNum basename 32 "save%d.dsg" slot

	if ref? [currsavefile]
		I_Free [currsavefile]
	endif

	[currsavefile] = M_StringJoin [savegamedir] basename
	return [currsavefile]
end

;
; Write the header for a savegame
;
fun P_WriteSaveHeader (desc ^uchar van_code s32)
	; clear error indicator
	[savegame_error] = FALSE

	; write the version string
	version ^u8 = VERSION_STRING

	i s32 = 0
	loop while lt? i VERSION_SIZE
		saveg_write8 [version]
		version = padd version 1
		i = iadd i 1
	endloop

	; write the description
	i s32 = 0
	loop while lt? i SAVESTRING_SIZE
		saveg_write8 [desc]

		if some? [desc]
			desc = padd desc 1
		endif

		i = iadd i 1
	endloop

	; write map information
	saveg_write_enum [gameskill]
	saveg_write32    [gameepisode]
	saveg_write32    [gamemap]
	saveg_write32    [leveltime]

	; write which players are in the game
	i s32 = 0
	loop while lt? i MAXPLAYERS
		saveg_write_boolean [players i .in_game]
		i = iadd i 1
	endloop

	; andrewj: extra state compared to vanilla
	saveg_write_boolean	 [nomonsters]
	saveg_write_boolean	 [respawnparm]
	saveg_write_boolean	 [respawnmonsters]
	saveg_write_boolean	 [fastparm]

	saveg_write32 [prndindex]
	saveg_write32 [brain .on]
end

;
; Read the header for a savegame
;
fun P_ReadSaveHeader (van_code s32 -> bool)
	; clear error indicator
	[savegame_error] = FALSE

	ok = P_CheckSaveVersion
	if not ok
		return FALSE
	endif

	; skip the description field
	i s32 = 0
	loop while lt? i SAVESTRING_SIZE
		saveg_read8
		i = iadd i 1
	endloop

	; read map information
	[gameskill]   = saveg_read_enum
	[gameepisode] = saveg_read32
	[gamemap]     = saveg_read32
	[leveltime]   = saveg_read32

	; read which players are in the game
	i s32 = 0
	loop while lt? i MAXPLAYERS
		[players i .in_game] = saveg_read_boolean
		i = iadd i 1
	endloop

	; andrewj: extra state compared to vanilla
	[nomonsters]      = saveg_read_boolean
	[respawnparm]     = saveg_read_boolean
	[respawnmonsters] = saveg_read_boolean
	[fastparm]        = saveg_read_boolean

	[prndindex] = saveg_read32
	[brain .on] = saveg_read32

	if [savegame_error]
		return FALSE
	endif

	return TRUE  ; ok
end

fun P_ReadSaveDescription (slot s32 buf ^[SAVESTRING_SIZE]uchar -> s32)
	; this is for the menu code

	filename = P_SaveGameFile slot

	[save_stream] = I_Fopen filename "rb"

	if null? [save_stream]
		return 0  ; does not exist
	endif

	[savegame_error] = FALSE

	ok = P_CheckSaveVersion
	if not ok
		I_Fclose [save_stream]
		return -1  ; version check failed
	endif

	count = I_Fread buf SAVESTRING_SIZE [save_stream]
	I_Fclose [save_stream]

	if lt? count SAVESTRING_SIZE
		return -1
	endif

	; ensure it is NUL-terminated
	[buf (SAVESTRING_SIZE - 1)] = 0

	return 1  ; ok
end

fun P_CheckSaveVersion (-> bool)
	; check the version string
	version ^u8 = VERSION_STRING

	i s32 = 0
	loop while lt? i VERSION_SIZE
		ch = saveg_read8
		if ne? ch [version]
			return FALSE  ; bad version
		endif
		version = padd version 1
		i = iadd i 1
	endloop

	return TRUE
end

fun P_UnArchiveWorld ()
	; remove all current thinkers (things loaded from the map, etc).
	P_RemoveAllThinkers

	P_UnArchivePlayers
	P_UnArchiveLevel
	P_UnArchiveThinkers

	P_ResolveMobjPointers

	; ensure MAP30 of Doom II is not borked
	where = [brain .on]
	P_BrainCollectSpots
	[brain .on] = where
end

fun P_ArchiveWorld (-> bool)
	; andrewj: force buttons to reset their texture
	P_FinishAllButtons

	P_ArchivePlayers
	P_ArchiveLevel
	P_ArchiveThinkers

	if [savegame_error]
		return FALSE
	endif

	return TRUE  ; ok
end

;----------------------------------------------------------------------

#private

const MARKER_EOF     = 'E'
const MARKER_Thinker = 'T'
const MARKER_Player  = 'P'
const MARKER_Sector  = 'S'
const MARKER_Line    = 'L'
const MARKER_Side    = 'X'

;
; P_ArchivePlayers
;
fun P_ArchivePlayers ()
	i s32 = 0

	loop while lt? i MAXPLAYERS
		p = [addr-of players i]

		if [p .in_game]
			saveg_write_player_t p
		endif

		i = iadd i 1
	endloop
end

;
; P_UnArchivePlayers
;
fun P_UnArchivePlayers ()
	i s32 = 0

	loop while lt? i MAXPLAYERS
		p = [addr-of players i]

		if [p .in_game]
			saveg_read_player_t p
		endif

		i = iadd i 1
	endloop
end

;
; P_ArchiveLevel
;
fun P_ArchiveLevel ()
	; do sectors
	i s32 = 0
	loop while lt? i [numsectors]
		sec = [addr-of [sectors] i]
		saveg_write_sector_t sec
		i = iadd i 1
	endloop

	; do lines
	i s32 = 0
	loop while lt? i [numlines]
		ld = [addr-of [lines] i]
		saveg_write_line_t ld
		i = iadd i 1
	endloop
end

;
; P_UnArchiveLevel
;
fun P_UnArchiveLevel ()
	; do sectors
	i s32 = 0
	loop while lt? i [numsectors]
		sec = [addr-of [sectors] i]
		saveg_read_sector_t sec
		i = iadd i 1
	endloop

	; do lines
	i s32 = 0
	loop while lt? i [numlines]
		ld = [addr-of [lines] i]
		saveg_read_line_t ld
		i = iadd i 1
	endloop
end

;
; P_ArchiveThinkers
;
fun P_ArchiveThinkers ()
	; andrewj: this does ALL types, not just mobj_t

	th = [thinkercap .next]

	loop until eq? th thinkercap
		func = [th .func]

		ch = conv u8 func
		ch = iadd ch '0'

		saveg_write8 MARKER_Thinker
		saveg_write8 ch

		if eq? func THINK_mobj
			saveg_write_mobj_t (raw-cast th)

		elif eq? func THINK_ceil
			saveg_write_ceiling_t (raw-cast th)

		elif eq? func THINK_floor
			saveg_write_floor_t (raw-cast th)

		elif eq? func THINK_plat
			saveg_write_plat_t (raw-cast th)

		elif eq? func THINK_door
			saveg_write_door_t (raw-cast th)

		elif eq? func THINK_flicker
			saveg_write_fireflicker_t (raw-cast th)

		elif eq? func THINK_glow
			saveg_write_glow_t (raw-cast th)

		elif eq? func THINK_flash
			saveg_write_lightflash_t (raw-cast th)

		elif eq? func THINK_strobe
			saveg_write_strobe_t (raw-cast th)

		else
			I_Error "P_ArchiveThinkers: Unknown thinker function"
		endif

		th = [th .next]
	endloop

	; add a terminating marker
	saveg_write8 MARKER_EOF
	saveg_write8 26  ; ^Z
end

;
; P_UnArchiveThinkers
;
fun P_UnArchiveThinkers ()
	; andrewj: this now does ALL types, not just mobj_t

	loop
		; check for the Terminator
		mark = saveg_read8

		if eq? mark MARKER_EOF
			; Hasta la Vista, baby!
			return
		endif

		if [savegame_error]
			I_Error "Bad savegame (read error)"
		elif ne? mark MARKER_Thinker
			I_Error "Bad savegame (expected a thinker)"
		endif

		mark = saveg_read8
		mark = iand mark 15

		if eq? mark THINK_mobj
			mo ^mobj_t = Z_Calloc mobj_t.size PU_LEVEL
			saveg_read_mobj_t mo

			P_SetThingPosition mo

			; killough 2/28/98:
			; Fix for falling down into a wall after savegame loaded
			; [ do not overwrite floorz and ceilingz from save file ]

			P_AddThinker (raw-cast mo)

		elif eq? mark THINK_ceil
			ceil ^ceiling_t = Z_Calloc ceiling_t.size PU_LEVEL
			saveg_read_ceiling_t ceil

			P_AddThinker (raw-cast ceil)
			[[ceil .sector] .specialdata] = ceil

		elif eq? mark THINK_floor
			floor ^floor_t = Z_Calloc floor_t.size PU_LEVEL
			saveg_read_floor_t floor

			P_AddThinker (raw-cast floor)
			[[floor .sector] .specialdata] = floor

		elif eq? mark THINK_plat
			plat ^plat_t = Z_Calloc plat_t.size PU_LEVEL
			saveg_read_plat_t plat

			P_AddThinker (raw-cast plat)
			[[plat .sector] .specialdata] = plat

		elif eq? mark THINK_door
			door ^door_t = Z_Calloc door_t.size PU_LEVEL
			saveg_read_door_t door

			P_AddThinker (raw-cast door)
			[[door .sector] .specialdata] = door

		elif eq? mark THINK_flicker
			flick ^fireflicker_t = Z_Calloc fireflicker_t.size PU_LEVEL
			saveg_read_fireflicker_t flick

			P_AddThinker (raw-cast flick)

		elif eq? mark THINK_glow
			glow ^glow_t = Z_Calloc glow_t.size PU_LEVEL
			saveg_read_glow_t glow

			P_AddThinker (raw-cast glow)

		elif eq? mark THINK_flash
			flash ^lightflash_t = Z_Calloc lightflash_t.size PU_LEVEL
			saveg_read_lightflash_t flash

			P_AddThinker (raw-cast flash)

		elif eq? mark THINK_strobe
			strobe ^strobe_t = Z_Calloc strobe_t.size PU_LEVEL
			saveg_read_strobe_t strobe

			P_AddThinker (raw-cast strobe)

		else
			I_Error "Bad savegame (unknown tclass)"
		endif
	endloop
end

fun P_ResolveMobjPointers ()
	; do sectors
	i s32 = 0

	loop while lt? i [numsectors]
		sec = [addr-of [sectors] i]
		[sec .soundtarget] = saveg_lookup_mobjPtr [sec .soundtarget]
		i = iadd i 1
	endloop

	; do things
	th = [thinkercap .next]

	loop until eq? th thinkercap
		if eq? [th .func] THINK_mobj
			mo = raw-cast ^mobj_t th

			[mo .target] = saveg_lookup_mobjPtr [mo .target]
			[mo .tracer] = saveg_lookup_mobjPtr [mo .tracer]
		endif

		th = [th .next]
	endloop
end

;----------------------------------------------------------------------

#private

;;
;;  LEVEL STRUCTS
;;

;
; sector_t
;
fun saveg_read_sector_t (sec ^sector_t)
	mark = saveg_read8

	if [savegame_error]
		I_Error "Bad savegame (read error)"
	elif ne? mark MARKER_Sector
		I_Error "Bad savegame (expected a sector)"
	endif

	[sec .floorh]   = saveg_read_fixed_t
	[sec .ceilh]    = saveg_read_fixed_t
	[sec .floorpic] = saveg_read32
	[sec .ceilpic]  = saveg_read32

	[sec .light]    = saveg_read16
	[sec .special]  = saveg_read16
	[sec .tag]      = saveg_read16

	[sec .soundtarget] = saveg_read_mobjPtr

	[sec .validcount]  = 0
	[sec .specialdata] = NULL
	[sec .thinglist]   = NULL
end

fun saveg_write_sector_t (sec ^sector_t)
	saveg_write8 MARKER_Sector

	saveg_write_fixed_t [sec .floorh]
	saveg_write_fixed_t [sec .ceilh]
	saveg_write32       [sec .floorpic]
	saveg_write32       [sec .ceilpic]

	saveg_write16 [sec .light]
	saveg_write16 [sec .special]
	saveg_write16 [sec .tag]

	saveg_write_mobjPtr [sec .soundtarget]
end

;
; line_t
;
fun saveg_read_line_t (ld ^line_t)
	mark = saveg_read8

	if [savegame_error]
		I_Error "Bad savegame (read error)"
	elif ne? mark MARKER_Line
		I_Error "Bad savegame (expected a linedef)"
	endif

	[ld .flags]   = saveg_read16
	[ld .special] = saveg_read16
	[ld .tag]     = saveg_read16

	; handle sidedefs here.
	; andrewj: not sure why vanilla does it this way, but I follow suit.

	i s32 = 0
	loop while lt? i 2
		sdnum = [ld .sidenum i]

		if ge? sdnum 0
			sd = [addr-of [sides] sdnum]
			saveg_read_side_t sd
		endif

		i = iadd i 1
	endloop

	[ld .validcount] = 0
end

fun saveg_write_line_t (ld ^line_t)
	saveg_write8 MARKER_Line

	saveg_write16 [ld .flags]
	saveg_write16 [ld .special]
	saveg_write16 [ld .tag]

	i s32 = 0
	loop while lt? i 2
		sdnum = [ld .sidenum i]

		if ge? sdnum 0
			sd = [addr-of [sides] sdnum]
			saveg_write_side_t sd
		endif

		i = iadd i 1
	endloop
end

;
; side_t
;
fun saveg_read_side_t (side ^side_t)
	mark = saveg_read8

	if [savegame_error]
		I_Error "Bad savegame (read error)"
	elif ne? mark MARKER_Side
		I_Error "Bad savegame (expected a sidedef)"
	endif

	[side .x_offset] = saveg_read_fixed_t
	[side .y_offset] = saveg_read_fixed_t
	[side .top]      = saveg_read32
	[side .bottom]   = saveg_read32
	[side .mid]      = saveg_read32
end

fun saveg_write_side_t (side ^side_t)
	saveg_write8 MARKER_Side

	saveg_write_fixed_t [side .x_offset]
	saveg_write_fixed_t [side .y_offset]

	saveg_write32 [side .top]
	saveg_write32 [side .bottom]
	saveg_write32 [side .mid]
end

;----------------------------------------------------------------------

;;
;;  OBJECTS and PLAYERS
;;

;
; mobj_t
;
fun saveg_read_mobj_t (mo ^mobj_t)
	saveg_read_thinker_t [addr-of mo .thinker]

	[mo .x]     = saveg_read_fixed_t
	[mo .y]     = saveg_read_fixed_t
	[mo .z]     = saveg_read_fixed_t
	[mo .angle] = saveg_read32

	; .subsector is recreated by a P_SetThingPosition call
	; .snext and .sprev are recreated
	; .bnext and .bprev are recreated

	plnum = saveg_read32

	if neg? plnum
		[mo .player] = NULL
	else
		p = [addr-of players plnum]
		[mo .player] = p

		; recreate .mo field of player_t
		[p .mo] = mo
	endif

	; NOTE that these two are not real pointers, but casted indices
	; which get resolved to actual pointers later...
	[mo .target] = saveg_read_mobjPtr
	[mo .tracer] = saveg_read_mobjPtr

	[mo .state]  = saveg_read_statePtr
	[mo .tics]   = saveg_read32
	[mo .sprite] = saveg_read_enum
	[mo .frame]  = saveg_read32

	[mo .floorz] = saveg_read_fixed_t
	[mo .ceilz]  = saveg_read_fixed_t
	[mo .radius] = saveg_read_fixed_t
	[mo .height] = saveg_read_fixed_t

	[mo .momx]   = saveg_read_fixed_t
	[mo .momy]   = saveg_read_fixed_t
	[mo .momz]   = saveg_read_fixed_t

	[mo .type]   = saveg_read_enum
	[mo .flags]  = saveg_read_enum
	[mo .health] = saveg_read32

	; recreate the .info pointer
	[mo .info] = [addr-of mobjinfo [mo .type]]

	[mo .movedir]      = saveg_read32
	[mo .movecount]    = saveg_read32
	[mo .reactiontime] = saveg_read32
	[mo .threshold]    = saveg_read32
	[mo .lastlook]     = saveg_read32

	; leave .validcount as zero
	; leave .ref_count  as zero

	saveg_read_mapthing_t [addr-of mo .spawnpoint]
end

fun saveg_write_mobj_t (mo ^mobj_t)
	saveg_write_thinker_t [addr-of mo .thinker]

	saveg_write_fixed_t [mo .x]
	saveg_write_fixed_t [mo .y]
	saveg_write_fixed_t [mo .z]
	saveg_write32       [mo .angle]

	; not needed: .info
	; not needed: .subsector
	; not needed: .snext .sprev
	; not needed: .bnext .bprev

	if ref? [mo .player]
		saveg_write32 [[mo .player] .index]
	else
		saveg_write32 -1
	endif

	saveg_write_mobjPtr  [mo .target]
	saveg_write_mobjPtr  [mo .tracer]

	saveg_write_statePtr [mo .state]
	saveg_write32        [mo .tics]
	saveg_write_enum     [mo .sprite]
	saveg_write32        [mo .frame]

	saveg_write_fixed_t  [mo .floorz]
	saveg_write_fixed_t  [mo .ceilz]
	saveg_write_fixed_t  [mo .radius]
	saveg_write_fixed_t  [mo .height]

	saveg_write_fixed_t  [mo .momx]
	saveg_write_fixed_t  [mo .momy]
	saveg_write_fixed_t  [mo .momz]

	saveg_write_enum [mo .type]
	saveg_write_enum [mo .flags]
	saveg_write32    [mo .health]

	saveg_write32    [mo .movedir]
	saveg_write32    [mo .movecount]
	saveg_write32    [mo .reactiontime]
	saveg_write32    [mo .threshold]
	saveg_write32    [mo .lastlook]

	; not needed: .validcount
	; not needed: .ref_count

	saveg_write_mapthing_t [addr-of mo .spawnpoint]
end

;
; thinker_t
;
fun saveg_read_thinker_t (th ^thinker_t)
	[th .func]   = saveg_read_enum
	[th .remove] = saveg_read32

	; these links get recreated by a P_AddThinker call
	[th .next] = NULL
	[th .prev] = NULL
end

fun saveg_write_thinker_t (th ^thinker_t)
	saveg_write_enum [th .func]
	saveg_write32    [th .remove]
end

;
; mapthing_t
;
fun saveg_read_mapthing_t (mt ^mapthing_t)
	[mt .x]       = saveg_read16
	[mt .y]       = saveg_read16
	[mt .angle]   = saveg_read16
	[mt .type]    = saveg_read16
	[mt .options] = saveg_read16
end

fun saveg_write_mapthing_t (mt ^mapthing_t)
	saveg_write16 [mt .x]
	saveg_write16 [mt .y]
	saveg_write16 [mt .angle]
	saveg_write16 [mt .type]
	saveg_write16 [mt .options]
end

;
; player_t
;
fun saveg_read_player_t (p ^player_t)
	mark = saveg_read8

	if [savegame_error]
		I_Error "Bad savegame (read error)"
	elif ne? mark MARKER_Player
		I_Error "Bad savegame (expected a player)"
	endif

	I_MemSet p 0 player_t.size

	; .mo is recreated in saveg_read_mobj_t
	; .cmd is not needed, left blank
	; .message and .attacker are left NULL

	[p .index]       = saveg_read32
	[p .playerstate] = saveg_read_enum

	[p .viewz]           = saveg_read_fixed_t
	[p .viewheight]      = saveg_read_fixed_t
	[p .deltaviewheight] = saveg_read_fixed_t
	[p .bob]             = saveg_read_fixed_t

	[p .health]      = saveg_read32
	[p .armorpoints] = saveg_read32
	[p .armortype]   = saveg_read32

	i s32 = 0
	loop while lt? i NUMPOWERS
		[p .powers i] = saveg_read32
		i = iadd i 1
	endloop

	i s32 = 0
	loop while lt? i NUMCARDS
		[p .cards i] = saveg_read_boolean
		i = iadd i 1
	endloop

	[p .backpack]  = saveg_read_boolean
	[p .didsecret] = saveg_read_boolean

	i s32 = 0
	loop while lt? i MAXPLAYERS
		[p .frags i] = saveg_read32
		i = iadd i 1
	endloop

	[p .ready]   = saveg_read_enum
	[p .pending] = saveg_read_enum

	i s32 = 0
	loop while lt? i NUMWEAPONS
		[p .owned i] = saveg_read32
		i = iadd i 1
	endloop

	i s32 = 0
	loop while lt? i NUMAMMO
		[p .ammo    i] = saveg_read32
		[p .maxammo i] = saveg_read32
		i = iadd i 1
	endloop

	[p .attackdown] = saveg_read_boolean
	[p .usedown]    = saveg_read_boolean
	[p .refire]     = saveg_read_boolean
	[p .in_game]    = TRUE

	[p .cheats]      = saveg_read_enum
	[p .killcount]   = saveg_read32
	[p .itemcount]   = saveg_read32
	[p .secretcount] = saveg_read32

	[p .damagecount]   = saveg_read32
	[p .bonuscount]    = saveg_read32
	[p .extralight]    = saveg_read32
	[p .fixedcolormap] = saveg_read32

	i s32 = 0
	loop while lt? i NUMPSPRITES
		psp = [addr-of p .psprites i]
		saveg_read_pspdef_t psp
		i = iadd i 1
	endloop
end

fun saveg_write_player_t (p ^player_t)
	saveg_write8 MARKER_Player

	; not needed: .mo
	; not needed: .cmd
	; not needed: .message
	; not needed: .attacker

	saveg_write32       [p .index]
	saveg_write_enum    [p .playerstate]

	saveg_write_fixed_t [p .viewz]
	saveg_write_fixed_t [p .viewheight]
	saveg_write_fixed_t [p .deltaviewheight]
	saveg_write_fixed_t [p .bob]

	saveg_write32 [p .health]
	saveg_write32 [p .armorpoints]
	saveg_write32 [p .armortype]

	i s32 = 0
	loop while lt? i NUMPOWERS
		saveg_write32 [p .powers i]
		i = iadd i 1
	endloop

	i s32 = 0
	loop while lt? i NUMCARDS
		saveg_write_boolean [p .cards i]
		i = iadd i 1
	endloop

	saveg_write_boolean [p .backpack]
	saveg_write_boolean [p .didsecret]

	i s32 = 0
	loop while lt? i MAXPLAYERS
		saveg_write32 [p .frags i]
		i = iadd i 1
	endloop

	saveg_write_enum [p .ready]
	saveg_write_enum [p .pending]

	i s32 = 0
	loop while lt? i NUMWEAPONS
		saveg_write32 [p .owned i]
		i = iadd i 1
	endloop

	i s32 = 0
	loop while lt? i NUMAMMO
		saveg_write32 [p .ammo    i]
		saveg_write32 [p .maxammo i]
		i = iadd i 1
	endloop

	saveg_write_boolean [p .attackdown]
	saveg_write_boolean [p .usedown]
	saveg_write_boolean [p .refire]

	; not needed: .in_game

	saveg_write_enum [p .cheats]

	saveg_write32 [p .killcount]
	saveg_write32 [p .itemcount]
	saveg_write32 [p .secretcount]

	saveg_write32 [p .damagecount]
	saveg_write32 [p .bonuscount]
	saveg_write32 [p .extralight]
	saveg_write32 [p .fixedcolormap]

	i s32 = 0
	loop while lt? i NUMPSPRITES
		psp = [addr-of p .psprites i]
		saveg_write_pspdef_t psp
		i = iadd i 1
	endloop
end

;
; pspdef_t
;
fun saveg_read_pspdef_t (psp ^pspdef_t)
	[psp .state] = saveg_read_statePtr
	[psp .tics]  = saveg_read32
	[psp .sx]    = saveg_read_fixed_t
	[psp .sy]    = saveg_read_fixed_t
end

fun saveg_write_pspdef_t (psp ^pspdef_t)
	saveg_write_statePtr [psp .state]
	saveg_write32        [psp .tics]
	saveg_write_fixed_t  [psp .sx]
	saveg_write_fixed_t  [psp .sy]
end

;----------------------------------------------------------------------

;;
;;  MOVING PLANES
;;

;
; ceiling_t
;
fun saveg_read_ceiling_t (C ^ceiling_t)
	saveg_read_thinker_t [addr-of C .thinker]

	[C .sector]    = saveg_read_sectorPtr
	[C .type]      = saveg_read_enum
	[C .bottom]    = saveg_read_fixed_t
	[C .top]       = saveg_read_fixed_t
	[C .speed]     = saveg_read_fixed_t

	[C .direction] = saveg_read32
	[C .old_dir]   = saveg_read32
	[C .tag]       = saveg_read16
	[C .crush]     = saveg_read_boolean
	[C .silent]    = saveg_read_boolean
end

fun saveg_write_ceiling_t (C ^ceiling_t)
	saveg_write_thinker_t [addr-of C .thinker]
	saveg_write_sectorPtr [C .sector]

	saveg_write_enum    [C .type]
	saveg_write_fixed_t [C .bottom]
	saveg_write_fixed_t [C .top]
	saveg_write_fixed_t [C .speed]

	saveg_write32       [C .direction]
	saveg_write32       [C .old_dir]
	saveg_write16       [C .tag]
	saveg_write_boolean [C .crush]
	saveg_write_boolean [C .silent]
end

;
; floor_t
;
fun saveg_read_floor_t (F ^floor_t)
	saveg_read_thinker_t [addr-of F .thinker]

	[F .sector]    = saveg_read_sectorPtr
	[F .type]      = saveg_read_enum
	[F .dest]      = saveg_read_fixed_t
	[F .speed]     = saveg_read_fixed_t

	[F .direction] = saveg_read32
	[F .texture]   = saveg_read32
	[F .special]   = saveg_read16
	[F .crush]     = saveg_read_boolean
end

fun saveg_write_floor_t (F ^floor_t)
	saveg_write_thinker_t [addr-of F .thinker]
	saveg_write_sectorPtr [F .sector]

	saveg_write_enum    [F .type]
	saveg_write_fixed_t [F .dest]
	saveg_write_fixed_t [F .speed]

	saveg_write32       [F .direction]
	saveg_write32       [F .texture]
	saveg_write16       [F .special]
	saveg_write_boolean [F .crush]
end

;
; plat_t
;
fun saveg_read_plat_t (P ^plat_t)
	saveg_read_thinker_t [addr-of P .thinker]

	[P .sector]   = saveg_read_sectorPtr
	[P .type]     = saveg_read_enum
	[P .status]   = saveg_read_enum
	[P .old_stat] = saveg_read_enum

	[P .low]      = saveg_read_fixed_t
	[P .high]     = saveg_read_fixed_t
	[P .speed]    = saveg_read_fixed_t

	[P .wait]     = saveg_read32
	[P .count]    = saveg_read32
	[P .tag]      = saveg_read16
	[P .crush]    = saveg_read_boolean
end

fun saveg_write_plat_t (P ^plat_t)
	saveg_write_thinker_t [addr-of P .thinker]
	saveg_write_sectorPtr [P .sector]

	saveg_write_enum    [P .type]
	saveg_write_enum    [P .status]
	saveg_write_enum    [P .old_stat]

	saveg_write_fixed_t [P .low]
	saveg_write_fixed_t [P .high]
	saveg_write_fixed_t [P .speed]

	saveg_write32       [P .wait]
	saveg_write32       [P .count]
	saveg_write16       [P .tag]
	saveg_write_boolean [P .crush]
end

;
; door_t
;
fun saveg_read_door_t (D ^door_t)
	saveg_read_thinker_t [addr-of D .thinker]

	[D .sector]    = saveg_read_sectorPtr
	[D .type]      = saveg_read_enum
	[D .speed]     = saveg_read_fixed_t
	[D .top]       = saveg_read_fixed_t

	[D .direction] = saveg_read32
	[D .wait]      = saveg_read32
	[D .count]     = saveg_read32
end

fun saveg_write_door_t (D ^door_t)
	saveg_write_thinker_t [addr-of D .thinker]
	saveg_write_sectorPtr [D .sector]

	saveg_write_enum    [D .type]
	saveg_write_fixed_t [D .speed]
	saveg_write_fixed_t [D .top]

	saveg_write32       [D .direction]
	saveg_write32       [D .wait]
	saveg_write32       [D .count]
end

;----------------------------------------------------------------------

;;
;;  LIGHT THINKERS
;;

;
; lightflash_t
;
fun saveg_read_lightflash_t (flash ^lightflash_t)
	saveg_read_thinker_t [addr-of flash .thinker]

	[flash .sector]   = saveg_read_sectorPtr
	[flash .count]    = saveg_read32
	[flash .maxlight] = saveg_read16
	[flash .minlight] = saveg_read16
	[flash .maxtime]  = saveg_read32
	[flash .mintime]  = saveg_read32
end

fun saveg_write_lightflash_t (flash ^lightflash_t)
	saveg_write_thinker_t [addr-of flash .thinker]
	saveg_write_sectorPtr [flash .sector]

	saveg_write32 [flash .count]
	saveg_write16 [flash .maxlight]
	saveg_write16 [flash .minlight]
	saveg_write32 [flash .maxtime]
	saveg_write32 [flash .mintime]
end

;
; strobe_t
;
fun saveg_read_strobe_t (strobe ^strobe_t)
	saveg_read_thinker_t [addr-of strobe .thinker]

	[strobe .sector]     = saveg_read_sectorPtr
	[strobe .count]      = saveg_read32
	[strobe .minlight]   = saveg_read16
	[strobe .maxlight]   = saveg_read16
	[strobe .darktime]   = saveg_read32
	[strobe .brighttime] = saveg_read32
end

fun saveg_write_strobe_t (strobe ^strobe_t)
	saveg_write_thinker_t [addr-of strobe .thinker]
	saveg_write_sectorPtr [strobe .sector]

	saveg_write32 [strobe .count]
	saveg_write16 [strobe .minlight]
	saveg_write16 [strobe .maxlight]
	saveg_write32 [strobe .darktime]
	saveg_write32 [strobe .brighttime]
end

;
; fireflicker_t
;
fun saveg_read_fireflicker_t (flick ^fireflicker_t)
	saveg_read_thinker_t [addr-of flick .thinker]

	[flick .sector]    = saveg_read_sectorPtr
	[flick .count]     = saveg_read32
	[flick .maxlight]  = saveg_read16
	[flick .minlight]  = saveg_read16
end

fun saveg_write_fireflicker_t (flick ^fireflicker_t)
	saveg_write_thinker_t [addr-of flick .thinker]
	saveg_write_sectorPtr [flick .sector]

	saveg_write32 [flick .count]
	saveg_write16 [flick .maxlight]
	saveg_write16 [flick .minlight]
end

;
; glow_t
;
fun saveg_read_glow_t (glow ^glow_t)
	saveg_read_thinker_t [addr-of glow .thinker]

	[glow .sector]    = saveg_read_sectorPtr
	[glow .minlight]  = saveg_read16
	[glow .maxlight]  = saveg_read16
	[glow .direction] = saveg_read16
end

fun saveg_write_glow_t (glow ^glow_t)
	saveg_write_thinker_t [addr-of glow .thinker]
	saveg_write_sectorPtr [glow .sector]

	saveg_write16 [glow .minlight]
	saveg_write16 [glow .maxlight]
	saveg_write16 [glow .direction]
end

;----------------------------------------------------------------------

; Various helpers....

fun saveg_read_statePtr (-> ^state_t)
	num = saveg_read32

	if neg? num
		return NULL
	endif

	return [addr-of states num]
end

fun saveg_write_statePtr (state ^state_t)
	if ref? state
		; compute index into states array
		idx = conv u32 (pdiff state states)
		idx = idivt idx state_t.size

		saveg_write32 idx
	else
		saveg_write32 -1
	endif
end

fun saveg_read_sectorPtr (-> ^sector_t)
	num = saveg_read32

	if neg? num
		return NULL
	endif

	return [addr-of [sectors] num]
end

fun saveg_write_sectorPtr (sec ^sector_t)
	if ref? sec
		; compute index into sectors array
		idx = conv u32 (pdiff sec [sectors])
		idx = idivt idx sector_t.size

		saveg_write32 idx
	else
		saveg_write32 -1
	endif
end

fun saveg_read_mobjPtr (-> ^mobj_t)
	; NOTE WELL that the return value is not actually a pointer,
	; but an integer index casted to a pointer.  the actual mobj_t
	; will be looked up during a later pass.

	idx  = saveg_read32
	idx2 = conv ssize idx  ; sign-extend to PTR_SIZE

	return raw-cast ^mobj_t idx2
end

fun saveg_write_mobjPtr (mo ^mobj_t)
	; NOTE that it is saved as a 32-bit index into the thinkers list.
	; since we save ALL thinkers, even ones pending removal, the loading
	; code can safely look it up.

	idx = P_IndexForThinker (raw-cast mo)
	saveg_write32 idx
end

fun saveg_lookup_mobjPtr (fake ^mobj_t -> ^mobj_t)
	idx2 = raw-cast ssize fake
	idx  = conv s32 idx2

	th = P_LookupThinker idx

	if null? th
		return NULL
	endif

	; sanity check
	if ne? [th .func] THINK_mobj
		I_Error "Bad savegame (bad mobj_t index)"
	endif

	; ensure reference count is bumped
	mo = raw-cast ^mobj_t th
	P_IncRef mo

	return mo
end

; boolean values are 8-bit integers.

inline-fun saveg_read_boolean (-> bool)
	b = saveg_read8
	return conv bool b
end

inline-fun saveg_write_boolean (b bool)
	saveg_write8 (conv u8 b)
end

; enums and fixed_t are 32-bit integers.

inline-fun saveg_read_enum (-> s32)
	value = saveg_read32
	return value
end

inline-fun saveg_write_enum (value s32)
	saveg_write32 value
end

inline-fun saveg_read_fixed_t (-> s32)
	value = saveg_read32
	return value
end

inline-fun saveg_write_fixed_t (value s32)
	saveg_write32 value
end

;----------------------------------------------------------------------

; Endian-safe integer read/write functions

fun saveg_read32 (-> s32)
	b1 = conv s32 (saveg_read8)
	b2 = conv s32 (saveg_read8)
	b3 = conv s32 (saveg_read8)
	b4 = conv s32 (saveg_read8)

	b2 = ishl b2 8
	b3 = ishl b3 16
	b4 = ishl b4 24

	value = ior b1 b2 b3 b4
	return value
end

fun saveg_write32 (value s32)
	b1 = conv u8 value
	value = ishr value 8

	b2 = conv u8 value
	value = ishr value 8

	b3 = conv u8 value
	value = ishr value 8

	b4 = conv u8 value

	saveg_write8 b1
	saveg_write8 b2
	saveg_write8 b3
	saveg_write8 b4
end

fun saveg_read16 (-> s16)
	b1 = conv s16 (saveg_read8)
	b2 = conv s16 (saveg_read8)

	b2 = ishl b2 8

	value = ior b1 b2
	return value
end

fun saveg_write16 (value s16)
	b1 = conv u8 value
	value = ishr value 8

	b2 = conv u8 value

	saveg_write8 b1
	saveg_write8 b2
end

fun saveg_read8 (-> u8)
	buf = stack-var [4]u8

	count = I_Fread buf 1 [save_stream]

	if lt? count 1
		if not [savegame_error]
			I_Print "Error or unexpected EOF while reading save game\n"
			[savegame_error] = TRUE
		endif

		return 0
	endif

	return [buf 0]
end

fun saveg_write8 (value u8)
	buf = stack-var [4]u8
	[buf 0] = value

	count = I_Fwrite buf 1 [save_stream]

	if lt? count 1
		if not [savegame_error]
			I_Print "Error while writing save game\n"
			[savegame_error] = TRUE
		endif
	endif
end
