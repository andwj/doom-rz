;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const HEIGHTBITS = 12
const HEIGHTUNIT = (1 << HEIGHTBITS)

const COLUMN_DRAWN = 0x7777

type drawseg_t struct
	.x1  s32  ; pixel range, inclusive
	.x2  s32  ;

	.seg  ^seg_t

	.topclip     ^[0]s16  ; clipping info for potential sprite blockers
	.bottomclip  ^[0]s16  ; [ used for masked walls too ]

	.texcolumns  ^[0]s16  ; saved tex column calcs for masked walls

	.solid       bool  ; completely solid, e.g. 1-sided wall
	.masked      bool  ; has a masked wall
	.sil_floor   bool  ; from floor downwards is solid, clips
	.sil_ceil    bool  ; from ceiling upwards is solid, clips

	.scale1      fixed_t
	.scale2      fixed_t
	.scalestep   fixed_t

	.floor_height  fixed_t  ; do not clip sprites above this
	.ceil_height   fixed_t  ; do not clip sprites below this
end

; andrewj: raised this from 256 to 1024
const MAXDRAWSEGS = 1024

zero-var drawsegs    [MAXDRAWSEGS]drawseg_t
zero-var numdrawseg  s32

; andrewj: doubled this limit
const MAXOPENINGS = (SCREENWIDTH * 128)

zero-var openings    [MAXOPENINGS]s16
zero-var numopening  s32

zero-var markfloor      bool
zero-var markceiling    bool

zero-var toptexture     s32
zero-var bottomtexture  s32
zero-var midtexture     s32

;
; regular wall
;
; WISH: encapsulate all these into a `rw` variable
zero-var rw_centerangle  angle_t
zero-var rw_offset       fixed_t
zero-var rw_distance     fixed_t
zero-var rw_scale        fixed_t
zero-var rw_scalestep    fixed_t
zero-var rw_midtexturemid     fixed_t
zero-var rw_toptexturemid     fixed_t
zero-var rw_bottomtexturemid  fixed_t

zero-var rw_normalangle  angle_t
zero-var rw_angle1       angle_t

zero-var topfrac      fixed_t
zero-var topstep      fixed_t
zero-var bottomfrac   fixed_t
zero-var bottomstep   fixed_t

zero-var pixhigh      fixed_t
zero-var pixlow       fixed_t
zero-var pixhighstep  fixed_t
zero-var pixlowstep   fixed_t

zero-var walllight s32

;
; R_ClearDrawSegs
;
fun R_ClearDrawSegs ()
	[numdrawseg] = 0
	[numopening] = 0
end

;
; R_StoreWallRange
;
; A wall segment will be drawn between start and stop pixels (inclusive).
;
fun R_StoreWallRange (x1 s32 x2 s32)
	; don't overflow and crash
	if ge? [numdrawseg] MAXDRAWSEGS
		return
	endif

	; allocate a drawseg
	ds = [addr-of drawsegs [numdrawseg]]
	[numdrawseg] = iadd [numdrawseg] 1

	seg = [curline]

	sidedef = [seg .sidedef]
	linedef = [seg .linedef]

	; mark the segment as visible for automap
	[linedef .flags] = ior [linedef .flags] ML_MAPPED

	; calculate rw_distance for scale calculation
	[rw_normalangle] = iadd [seg .angle] ANG90

	offsetangle s32 = isub [rw_normalangle] [rw_angle1]
	offsetangle     = iabs offsetangle
	offsetangle     = imin offsetangle ANG90

	distangle u32   = isub ANG90 offsetangle
	distangle       = ishr distangle ANGLEFINESHIFT

	hyp = R_PointToDist [viewx] [viewy] [[seg .v1] .x] [[seg .v1] .y]
	[rw_distance] = FixedMul hyp [finesine distangle]

	numcolumn = isub x2 x1 -1

	[ds .x1]  = x1
	[ds .x2]  = x2
	[ds .seg] = seg

	; calculate scale at both ends and step
	curangle = iadd [viewangle] [xtoviewangle x1]
	[ds .scale1] = R_ScaleFromGlobalAngle curangle

	if gt? x2 x1
		curangle = iadd [viewangle] [xtoviewangle x2]
		[ds .scale2] = R_ScaleFromGlobalAngle curangle

		step = isub  [ds .scale2] [ds .scale1]
		step = idivt step (isub x2 x1)

		[ds .scalestep] = step
	else
		[ds .scale2]    = [ds .scale1]
		[ds .scalestep] = 0
	endif

	[rw_scale]     = [ds .scale1]
	[rw_scalestep] = [ds .scalestep]

	; calculate texture boundaries.
	; also decide if floor / ceiling marks are needed.
	frontsector = [[cursubsec] .sector]
	backsector  = [seg .back]

	worldtop      = isub [frontsector .ceilh]  [viewz]
	worldbottom   = isub [frontsector .floorh] [viewz]
	worldhigh s32 = 0
	worldlow  s32 = 0

	[midtexture]    = 0
	[toptexture]    = 0
	[bottomtexture] = 0

	[ds .solid]      = FALSE
	[ds .masked]     = FALSE
	[ds .sil_floor]  = FALSE
	[ds .sil_ceil]   = FALSE

	[ds .bottomclip] = NULL
	[ds .topclip]    = NULL
	[ds .texcolumns] = NULL

	; enough openings left?
	openings_left = isub MAXOPENINGS [numopening]
	openings_left = isub openings_left numcolumn numcolumn numcolumn
	no_openings   = neg? openings_left

	if null? backsector
		; single sided line
		[midtexture] = [[texture_translation] [sidedef .mid]]

		; a single sided line is terminal, so it must mark ends
		[markfloor]   = TRUE
		[markceiling] = TRUE

		if some? (iand [linedef .flags] ML_DONTPEGBOTTOM)
			vtop = R_TextureHeight [sidedef .mid]
			vtop = iadd vtop [frontsector .floorh]

			; bottom of texture at bottom
			[rw_midtexturemid] = isub vtop [viewz]
		else
			; top of texture at top
			[rw_midtexturemid] = worldtop
		endif

		[rw_midtexturemid] = iadd [rw_midtexturemid] [sidedef .y_offset]

		[ds .solid] = TRUE

		; force all sprites to be clipped
		[ds .floor_height]  = FRAC_MAX
		[ds .ceil_height]   = FRAC_MIN
	else
		; two sided line
		worldhigh = isub [backsector .ceilh]  [viewz]
		worldlow  = isub [backsector .floorh] [viewz]

		; hack to allow height changes in outdoor areas
		if eq? [frontsector .ceilpic] [skyflatnum]
			if eq? [backsector .ceilpic] [skyflatnum]
				worldtop = worldhigh
			endif
		endif

		[markfloor] = TRUE

		if eq? worldlow worldbottom
			if eq? [backsector .floorpic] [frontsector .floorpic]
				if eq? [backsector .light] [frontsector .light]
					[markfloor] = FALSE
				endif
			endif
		endif

		[markceiling] = TRUE

		if eq? worldhigh worldtop
			if eq? [backsector .ceilpic] [frontsector .ceilpic]
				if eq? [backsector .light] [frontsector .light]
					[markceiling] = FALSE
				endif
			endif
		endif

		if gt? [frontsector .floorh] [backsector .floorh]
			[ds .sil_floor]    = TRUE
			[ds .floor_height] = [frontsector .floorh]
		elif gt? [backsector .floorh] [viewz]
			[ds .sil_floor]    = TRUE
			[ds .floor_height] = FRAC_MAX
		endif

		if lt? [frontsector .ceilh] [backsector .ceilh]
			[ds .sil_ceil]    = TRUE
			[ds .ceil_height] = [frontsector .ceilh]
		elif lt? [backsector .ceilh] [viewz]
			[ds .sil_ceil]    = TRUE
			[ds .ceil_height] = FRAC_MIN
		endif

		if le? [backsector .ceilh] [frontsector .floorh]
			[ds .solid] = TRUE

			[markfloor]   = TRUE  ; closed door
			[markceiling] = TRUE
		endif

		if ge? [backsector .floorh] [frontsector .ceilh]
			[ds .solid] = TRUE

			[markfloor]   = TRUE  ; closed door
			[markceiling] = TRUE
		endif

		; top texture visible?
		if lt? worldhigh worldtop
			[toptexture] = [[texture_translation] [sidedef .top]]

			if some? (iand [linedef .flags] ML_DONTPEGTOP)
				; top of texture at top
				[rw_toptexturemid] = worldtop
			else
				vtop = R_TextureHeight [sidedef .top]
				vtop = iadd vtop [backsector .ceilh]

				; bottom of texture
				[rw_toptexturemid] = isub vtop [viewz]
			endif
		endif

		; bottom texture visible?
		if gt? worldlow worldbottom
			[bottomtexture] = [[texture_translation] [sidedef .bottom]]

			if some? (iand [linedef .flags] ML_DONTPEGBOTTOM)
				; top of texture at top
				[rw_bottomtexturemid] = worldtop
			else
				[rw_bottomtexturemid] = worldlow
			endif
		endif

		[rw_toptexturemid]    = iadd [rw_toptexturemid]    [sidedef .y_offset]
		[rw_bottomtexturemid] = iadd [rw_bottomtexturemid] [sidedef .y_offset]

		; allocate space for masked texture tables
		if pos? [sidedef .mid]
			if not no_openings
				[ds .masked]     = TRUE
				[ds .texcolumns] = R_AllocOpenings numcolumn
			endif
		endif
	endif

	; calculate rw_offset (only needed for textured lines)
	offsetangle = ishr offsetangle ANGLEFINESHIFT
	[rw_offset] = FixedMul hyp [finesine offsetangle]

	distangle = isub [rw_normalangle] [rw_angle1]
	if lt? distangle ANG180
		[rw_offset] = ineg [rw_offset]
	endif

	[rw_offset] = iadd [rw_offset] [sidedef .x_offset] [seg .offset]

	[rw_centerangle] = iadd [viewangle] ANG90
	[rw_centerangle] = isub [rw_centerangle] [rw_normalangle]

	; calculate light table.
	; Use different shading for horizontal / vertical / diagonal.
	R_WallLighting seg [frontsector .light]

	; if a floor / ceiling plane is on the wrong side of the view plane,
	; it is definitely invisible and doesn't need to be marked.
	if ge? [frontsector .floorh] [viewz]
		[markfloor] = FALSE
	endif

	if ne? [frontsector .ceilpic] [skyflatnum]
		if le? [frontsector .ceilh] [viewz]
			[markceiling] = FALSE
		endif
	endif

	; calculate incremental stepping values for texture edges
	worldtop    = ishr worldtop      (FRACBITS - HEIGHTBITS)
	worldbottom = ishr worldbottom   (FRACBITS - HEIGHTBITS)
	worldhigh   = ishr worldhigh     (FRACBITS - HEIGHTBITS)
	worldlow    = ishr worldlow      (FRACBITS - HEIGHTBITS)

	ctryfrac    = ishr [centeryfrac] (FRACBITS - HEIGHTBITS)

	[topfrac] = FixedMul worldtop [rw_scale]
	[topstep] = FixedMul worldtop [rw_scalestep]

	[topfrac] = isub ctryfrac [topfrac]
	[topstep] = ineg [topstep]

	[bottomfrac] = FixedMul worldbottom [rw_scale]
	[bottomstep] = FixedMul worldbottom [rw_scalestep]

	[bottomfrac] = isub ctryfrac [bottomfrac]
	[bottomstep] = ineg [bottomstep]

	if ref? backsector
		[pixhigh]     = FixedMul worldhigh [rw_scale]
		[pixhighstep] = FixedMul worldhigh [rw_scalestep]

		[pixhigh]     = isub ctryfrac [pixhigh]
		[pixhighstep] = ineg [pixhighstep]

		[pixlow]     = FixedMul worldlow [rw_scale]
		[pixlowstep] = FixedMul worldlow [rw_scalestep]

		[pixlow]     = isub ctryfrac [pixlow]
		[pixlowstep] = ineg [pixlowstep]
	endif

	; render it
	if [markceiling]
		if ref? [ceilingplane]
			[ceilingplane] = R_CheckPlane [ceilingplane] x1 x2
		endif
	endif

	if [markfloor]
		if ref? [floorplane]
			[floorplane] = R_CheckPlane [floorplane] x1 x2
		endif
	endif

	[colfunc] = fun R_DrawColumn

	R_RenderSegLoop x1 x2 [ds .texcolumns]

	; force lines with masked walls to act as silhouette
	if [ds .masked]
		if not [ds .sil_ceil]
			[ds .sil_ceil]    = TRUE
			[ds .ceil_height] = FRAC_MIN
		endif

		if not [ds .sil_floor]
			[ds .sil_floor]    = TRUE
			[ds .floor_height] = FRAC_MAX
		endif
	endif

	; andrewj: don't die if we run out of openings.
	;          instead just disable silhouettes and masked walls.
	;          rendering will be glitched though!
	if no_openings
		[ds .sil_floor] = FALSE
		[ds .sil_ceil]  = FALSE
	endif

	; save sprite clipping info.  needed for masked walls too
	if [ds .sil_ceil]
		[ds .topclip] = R_AllocOpenings numcolumn
		R_CopyClipBuffer [ds .topclip] ceilingclip x1 numcolumn
	endif

	if [ds .sil_floor]
		[ds .bottomclip] = R_AllocOpenings numcolumn
		R_CopyClipBuffer [ds .bottomclip] floorclip x1 numcolumn
	endif
end

;
; R_RenderSegLoop
;
; Draws zero, one, or two textures for walls.
; For masked walls (railings etc) it saves the texture columns.
; Also marks the vertical pixel range of floors and ceilings.
;
fun R_RenderSegLoop (x1 s32 x2 s32 texcolumns ^[0]s16)
	x = x1
	loop while le? x x2
		; mark floor / ceiling areas
		cclip = conv s32 (iadd [ceilingclip x] 1)
		fclip = conv s32 (isub [floorclip   x] 1)

		yl = iadd [topfrac] (HEIGHTUNIT - 1)
		yl = ishr yl HEIGHTBITS
		yl = imax yl cclip

		yh = ishr [bottomfrac] HEIGHTBITS
		yh = imin yh fclip

		if [markceiling]
			if ref? [ceilingplane]
				top    = cclip
				bottom = isub yl 1
				bottom = imin bottom fclip

				if le? top bottom
					[[ceilingplane] .top    x] = conv u8 top
					[[ceilingplane] .bottom x] = conv u8 bottom
				endif
			endif
		endif

		if [markfloor]
			if ref? [floorplane]
				top    = iadd yh 1
				top    = imax top cclip
				bottom = fclip

				if le? top bottom
					[[floorplane] .top    x] = conv u8 top
					[[floorplane] .bottom x] = conv u8 bottom
				endif
			endif
		endif

		; texture column and lighting are independent of wall tiers

		angle = iadd [rw_centerangle] [xtoviewangle x]
		angle = ishr angle ANGLEFINESHIFT
		angle = iand angle 4095

		texcol = FixedMul [finetangent angle] [rw_distance]
		texcol = isub [rw_offset] texcol
		texcol = ishr texcol FRACBITS

		; calculate lighting
		if ref? [fixedcolormap]
			[dc .colormap] = [fixedcolormap]
		else
			index = ishr [rw_scale] LIGHTSCALESHIFT
			index = imax index 0
			index = imin index (MAXLIGHTSCALE - 1)

			[dc .colormap] = [scalelight [walllight] index]
		endif

		iscale u32 = [rw_scale]
		iscale = idivt 0xffffffff iscale

		[dc .x] = x
		[dc .iscale] = iscale

		; draw the wall tiers....

		if some? [midtexture]
			; -- single sided line --

			[dc .yl] = yl
			[dc .yh] = yh
			[dc .texturemid] = [rw_midtexturemid]
			[dc .source] = R_GetColumn [midtexture] texcol FALSE

			call [colfunc]

			[ceilingclip x] = conv s16 [viewheight]
			[floorclip   x] = conv s16 -1
		else
			; -- two sided line --

			if some? [toptexture]
				; top wall
				mid = ishr [pixhigh] HEIGHTBITS
				mid = imin mid fclip

				jump do_mark_ceil if lt? mid yl

				[ceilingclip x] = conv s16 mid

				[dc .yl] = yl
				[dc .yh] = mid
				[dc .texturemid] = [rw_toptexturemid]
				[dc .source] = R_GetColumn [toptexture] texcol FALSE

				call [colfunc]

			elif [markceiling]
				do_mark_ceil:
				[ceilingclip x] = conv s16 (isub yl 1)
			endif

			if some? [bottomtexture]
				; bottom wall
				mid = iadd [pixlow] (HEIGHTUNIT - 1)
				mid = ishr mid HEIGHTBITS
				mid = imax mid cclip

				jump do_mark_floor if gt? mid yh

				[floorclip x] = conv s16 mid

				[dc .yl] = mid
				[dc .yh] = yh
				[dc .texturemid] = [rw_bottomtexturemid]
				[dc .source] = R_GetColumn [bottomtexture] texcol FALSE

				call [colfunc]

			elif [markfloor]
				do_mark_floor:
				[floorclip x] = conv s16 (iadd yh 1)
			endif

			if ref? texcolumns
				; save texture column for masked walls
				mx = isub x x1
				[texcolumns mx] = conv s16 texcol
			endif
		endif

		[rw_scale]   = iadd [rw_scale]   [rw_scalestep]
		[topfrac]    = iadd [topfrac]    [topstep]
		[bottomfrac] = iadd [bottomfrac] [bottomstep]
		[pixhigh]    = iadd [pixhigh]    [pixhighstep]
		[pixlow]     = iadd [pixlow]     [pixlowstep]

		x = iadd x 1
	endloop
end

;
; R_RenderMaskedSegRange
;
; called by sprite drawing code, which handles the depth sorting
; between sprites and masked walls.  the x1/x2 range may be only a
; portion of the seg, those columns are marked as drawn so that any
; future call can skip those columns.
;
fun R_RenderMaskedSegRange (ds ^drawseg_t x1 s32 x2 s32)
	seg = [ds .seg]
	[curline] = seg

	frontsector = [seg .front]
	backsector  = [seg .back]

	sidedef     = [seg .sidedef]
	linedef     = [seg .linedef]

	midtex = [[texture_translation] [sidedef .mid]]

	ystep  = isub x1 [ds .x1]
	ystep  = imul ystep [ds .scalestep]
	yscale = iadd [ds .scale1] ystep

	; find positioning
	if some? (iand [linedef .flags] ML_DONTPEGBOTTOM)
		tex_height = R_TextureHeight midtex
		texturemid = imax [frontsector .floorh] [backsector .floorh]
		texturemid = iadd texturemid tex_height
	else
		texturemid = imin [frontsector .ceilh] [backsector .ceilh]
	endif

	; Use different shading for horizontal / vertical / diagonal.
	R_WallLighting seg [frontsector .light]

	; draw the columns
	texcolumns = [ds .texcolumns]

	texturemid = isub texturemid [viewz]
	texturemid = iadd texturemid [sidedef .y_offset]

	[msk_x1]        = [ds .x1]
	[msk_floorclip] = [ds .bottomclip]
	[msk_ceilclip]  = [ds .topclip]

	[colfunc] = fun R_DrawColumn

	x = x1
	loop while le? x x2
		mx     = isub x [ds .x1]
		texcol = conv s32 [texcolumns mx]

		if ne? texcol COLUMN_DRAWN
			[texcolumns mx] = COLUMN_DRAWN

			; calculate lighting
			if ref? [fixedcolormap]
				[dc .colormap] = [fixedcolormap]
			else
				index = ishr yscale LIGHTSCALESHIFT
				index = imax index 0
				index = imin index (MAXLIGHTSCALE - 1)

				[dc .colormap] = [scalelight [walllight] index]
			endif

			ytop = FixedMul texturemid yscale
			ytop = isub [centeryfrac] ytop

			iscale u32 = yscale
			iscale = idivt 0xffffffff iscale

			[dc .iscale] = iscale

			; draw the texture
			source = R_GetColumn midtex texcol TRUE
			posts ^post_t = raw-cast source

			[dc .x] = x
			[dc .texturemid] = texturemid

			R_DrawMaskedColumn posts ytop yscale
		endif

		yscale = iadd yscale [ds .scalestep]
		x      = iadd x 1
	endloop
end

fun R_WallLighting (seg ^seg_t light s16)
	if null? [fixedcolormap]
		seg = [curline]

		lightnum = conv s32 light
		lightnum = ishr lightnum LIGHTSEGSHIFT
		lightnum = iadd lightnum [extralight]

		if eq? [[seg .v1] .y] [[seg .v2] .y]
			lightnum = isub lightnum 1
		elif eq? [[seg .v1] .x] [[seg .v2] .x]
			lightnum = iadd lightnum 1
		endif

		lightnum = imax lightnum 0
		lightnum = imin lightnum (LIGHTLEVELS - 1)

		[walllight] = lightnum
	endif
end

fun R_AllocOpenings (count s32 -> ^[0]s16)
	start = [numopening]
	[numopening] = iadd [numopening] count

	if gt? [numopening] MAXOPENINGS
		I_Error "R_StoreWallRange: opening overflow"
	endif

	return [addr-of openings start]
end

fun R_CopyClipBuffer (dest ^s16 src ^s16 x1 s32 count s32)
	x1   = imul x1 2
	size = imul count 2
	src  = padd src x1
	I_MemCopy dest src size
end
