
DOOM-RZ README
==============

by Andrew Apted, 2021.


About
-----

Doom-RZ is a port of the classic 3D game DOOM to a programing language
of my own design called Razm.

https://gitlab.com/andwj/razm

Doom-RZ is based on Chocolate Doom by Simon Howard (et al), but with
significant changes.  That includes fixing bugs and issues of vanilla
DOOM which can be safely fixed without compromising demo compatibility,
as well as improving the overall readability of the code.

One goal of porting DOOM was simply to produce a significant body of
code to exercise the razm compiler, and inform the design of the razm
language, and it certainly proved to be invaluable for that.  Moreover,
and the main reason I selected DOOM to port, was for its system of demo
playback.  DOOM demos are merely a recording of the player inputs, and
rely completely on the game engine behaving exactly the same during
playback as during the recording session.  This means that any small
change in the playsim code can cause demos to "desync" and fail to play
properly.  This is a great test for a compiler, since any mistake in
the generated machine code will almost certainly cause such a desync,
and the problem can be tracked down and fixed.

While have I ported about as much as possible of the code from C to razm,
there is still a core module of C code which interfaces with LibSDL to
provide the video, sound and input facilities.  Several parts of the C
library are needed for file access and memory allocation facilities.
Plus the OPL-emulated music playback also remains as C code.

Supported games (IWADs)
-----------------------

- Doom, Ultimate Doom
- Doom II
- FreeDoom (phase 1 and 2)
- TNT Evilution
- Plutonia Experiment
- Chex Quest 1 and 2
- HacX 1.2

Changes compared to Chocolate Doom 3.0.1
----------------------------------------

+ no networking
+ no DeHackEd support
+ no ENDOOM display
+ no low detail rendering mode
+ no emulation of memory overruns

- fixed the medusa bug
- fixed savegames to remember monster targets
- fixed crash after loading a savegame on MAP30
- fixed unconditionally the "Sky never changes in Doom II" bug
- fixed dangling references when a mobj_t is removed
- fixed missing key bug for MAP31 of TNT Evilution
- fixed missing DEMO4 error with Final Doom

+ keep the savegames of different games separate
+ for demos, show message on stdout for each completed map
+ new `-fastforward` option for demo playback
+ faster screen wipes when doing a timed demo
+ better handling of `-width` and `-height` options
+ better sorting code for visible sprites

- no visplane overflow error, limit raised
- no drawseg overflow error, limit raised
- no openings overflow error, limit raised
- no "too many scrolling lines" error, limit raised
- no limit on solidsegs
- no limit on savegame size
- no limit on active ceilings and plats
- no limit on sectors for NextHighestFloor
- limit raised on number of visible sprites

Code Structure
--------------

The code is structured into a group of modules.  The "system" module
represents all the C code, and is conceptually at the bottom of the
dependency chain.  All the other modules are razm code, for example
the "engine" module is the top of the dependency chain.  Each module
of razm code is compiled as a unit, and sections marked as `#private`
are only visible to other razm files of the same module.

Each module of razm code has two interface files, one using the bare
name of the module (like "common.rz") containing types and constants
which are needed by the module itself as well as users of the module,
and one suffixed by the `_api` keyword (like "common_api.rz") which
contains external variable and function definitions for users of that
module.  The implementation of each module are in code files with a
single letter prefix specific to that module, e.g. the prefix "m_"
is used for the common module.

The following is a list of the modules, arranged so that a module
which depends on another one is earlier than it in the list.  For
example, the "engine" module depends on all other modules, so it is
shown first.  The "common" and "system" modules depend strongly on
each other, so both belong at the bottom of the list.  Also shown
are the files belonging to each module, where `*` is a wildcard.

```
   engine  : d_*.rz
   ui      : v_*.rz
   render  : r_*.rz
   play    : p_*.rz
   sound   : s_*.rz
   wad     : w_*.rz
   common  : m_*.rz
   system  : i_*.c/h  opl_*.c/h
```

Compiling
---------

This section documents compiling under Linux (or one of the BSDs).
Compiling under Windows is possible, but unfortunately it is not
something I can help with.

Compiling Doom-RZ requires the following:

+ the Razm compiler
+ the NASM assembler (https://www.nasm.us)
+ a C compiler and linker
+ GNU make
+ SDL2 (https://www.libsdl.org)
+ SDL2_mixer

When everything is fully set up, typing `make` in a shell with
the current directory at the top of the Doom-RZ source code is
enough to compile all the code and create an executable.

Legalese
--------

Doom-RZ is free software, under the terms of the GNU General Public
License (GPL), version 2 or (at your option) any later version.
See the [COPYING](COPYING) file for the complete text.

Doom-RZ comes with NO WARRANTY of any kind, express or implied.
Please read the license for all the details.
