;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

extern-fun R_TextureHeight (tex s32 -> fixed_t)

#private

const FLOORSPEED = FRACUNIT

type floor_type_e s32

const fl_lowerFloor        = 0  ; lower floor to highest surrounding floor
const fl_lowerTurbo        = 1  ; lower floor to highest surrounding floor VERY FAST
const fl_lowerToLowest     = 2  ; lower floor to lowest surrounding floor
const fl_lowerAndChange    = 3  ; lower floor to lowest surrounding floor and change floorpic
const fl_raiseFloor        = 4  ; raise floor to lowest surrounding CEILING
const fl_raiseToNearest    = 5  ; raise floor to next highest surrounding floor
const fl_raiseNearestTurbo = 6  ; raise to next highest floor, turbo-speed
const fl_raiseToTexture    = 7  ; raise floor to shortest height texture around it
const fl_raise24           = 8
const fl_raise24Change     = 9
const fl_raiseFloorCrush   = 10
const fl_raiseFloor512     = 11
const fl_donutRaise        = 12

type stair_type_e s32

const stair_build8  = 0  ; slowly build by 8
const stair_turbo16 = 1  ; quickly build by 16

type floor_t struct
	.thinker    thinker_t
	.sector    ^sector_t

	.type       floor_type_e
	.dest       fixed_t
	.speed      fixed_t

	.direction  s32
	.texture    s32
	.special    s16
	.crush      bool
	._pad       [3]u8
end


;
; Move a floor to it's destination (up or down)
;
fun T_MoveFloor (F ^floor_t)
	sec = [F .sector]
	t   = iand [leveltime] 7

	res = T_MoveFloorPlane sec [F .speed] [F .dest] [F .crush] [F .direction]

	if zero? t
		S_StartSound [addr-of sec .soundorg] sfx_stnmov
	endif

	if eq? res RES_pastdest
		; handle texture changes

		if eq? [F .direction] +1
			if eq? [F .type] fl_donutRaise
					[sec .special]  = [F .special]
					[sec .floorpic] = [F .texture]
			endif
		elif eq? [F .direction] -1
			if eq? [F .type] fl_lowerAndChange
					[sec .special]  = [F .special]
					[sec .floorpic] = [F .texture]
			endif
		endif

		[sec .specialdata] = NULL

		P_RemoveThinker (raw-cast F)

		S_StartSound [addr-of sec .soundorg] sfx_pstop
	endif
end

;
; Handle floor type.
; Donuts are eaten elsewhere.
;
fun EV_DoFloor (ld ^line_t kind floor_type_e -> bool)
	did = FALSE

	secnum s32 = -1

	loop
		secnum = P_FindSectorFromLineTag ld secnum
		break if neg? secnum

		sec = [addr-of [sectors] secnum]

		; already moving?  if so, keep going...
		if null? [sec .specialdata]
			; new floor thinker
			F ^floor_t = Z_Calloc floor_t.size PU_LEVEL

			[F .thinker .func] = THINK_floor

			; set default parameters
			[F .sector]  = sec
			[F .type]    = kind
			[F .speed]   = FLOORSPEED
			[F .crush]   = FALSE

			if le? kind fl_lowerAndChange
				[F .direction] = -1
			else
				[F .direction] = +1
			endif

			if eq? kind fl_lowerFloor
				[F .dest] = P_FindHighestFloorSurrounding sec

			elif eq? kind fl_lowerTurbo
				[F .speed] = (FLOORSPEED * 4)
				[F .dest]  = P_FindHighestFloorSurrounding sec

				if ne? [F .dest] [sec .floorh]
					[F .dest] = iadd [F .dest] (8 * FRACUNIT)
				endif

			elif eq? kind fl_lowerToLowest
				[F .dest] = P_FindLowestFloorSurrounding sec

			elif eq? kind fl_raiseFloor
				[F .dest] = P_FindLowestCeilingSurrounding sec
				[F .dest] = imin [F .dest] [sec .ceilh]

			elif eq? kind fl_raiseFloorCrush
				[F .dest]  = P_FindLowestCeilingSurrounding sec
				[F .dest]  = imin [F .dest] [sec .ceilh]
				[F .dest]  = isub [F .dest] (8 * FRACUNIT)
				[F .crush] = TRUE

			elif eq? kind fl_raiseToNearest
				[F .dest] = P_FindNextHighestFloor sec [sec .floorh]

			elif eq? kind fl_raiseNearestTurbo
				[F .speed] = (FLOORSPEED * 4)
				[F .dest]  = P_FindNextHighestFloor sec [sec .floorh]

			elif eq? kind fl_raise24
				[F .dest] = iadd [sec .floorh] (24 * FRACUNIT)

			elif eq? kind fl_raise24Change
				[F .dest] = iadd [sec .floorh] (24 * FRACUNIT)

				[sec .floorpic] = [[ld .front] .floorpic]
				[sec .special]  = [[ld .front] .special]

			elif eq? kind fl_raiseFloor512
				[F .dest] = iadd [sec .floorh] (512 * FRACUNIT)

			elif eq? kind fl_raiseToTexture
				tex_h = P_FindMinimumLowerTexHeight sec secnum

				[F .dest] = iadd [sec .floorh] tex_h

			elif eq? kind fl_lowerAndChange
				[F .dest] = P_FindLowestFloorSurrounding sec

				model = P_FindModelSectorForChange sec secnum [F .dest]

				if ref? model
					[F .texture] = [model .floorpic]
					[F .special] = [model .special]
				else
					[F .texture] = [sec .floorpic]
					[F .special] = 0
				endif
			endif

			P_AddThinker (raw-cast F)
			[sec .specialdata] = F

			did = TRUE
		endif
	endloop

	return did
end

fun P_FindMinimumLowerTexHeight (sec ^sector_t secnum s32 -> fixed_t)
	tex_h fixed_t = FRAC_MAX

	i s32 = 0
	loop while lt? i [sec .linecount]
		ld = [[sec .lines] i]

		if some? (iand [ld .flags] ML_TWOSIDED)
			side1 = [addr-of [sides] [ld .sidenum 0]]
			side2 = [addr-of [sides] [ld .sidenum 1]]

			if ge? [side1 .bottom] 0
				h = R_TextureHeight [side1 .bottom]
				tex_h = imin tex_h h
			endif

			if ge? [side2 .bottom] 0
				h = R_TextureHeight [side2 .bottom]
				tex_h = imin tex_h h
			endif
		endif

		i = iadd i 1
	endloop

	return tex_h
end

fun P_FindModelSectorForChange (sec ^sector_t secnum s32 dest_h fixed_t -> ^sector_t)
	i s32 = 0
	loop while lt? i [sec .linecount]
		ld = [[sec .lines] i]

		other = getNextSector ld sec

		if ref? other
			if eq? [other .floorh] dest_h
				return other
			endif
		endif

		i = iadd i 1
	endloop

	return NULL
end

;
; Build a staircase!
;
fun EV_BuildStairs (ld ^line_t kind stair_type_e -> bool)
	did = FALSE

	speed fixed_t = (FLOORSPEED / 4)
	rise  fixed_t = (8 * FRACUNIT)

	if eq? kind stair_turbo16
		speed = (FLOORSPEED * 4)
		rise  = (16 * FRACUNIT)
	endif

	secnum s32 = -1

	loop
		secnum = P_FindSectorFromLineTag ld secnum
		break if neg? secnum

		sec = [addr-of [sectors] secnum]

		; already moving?  if so, keep going...
		if null? [sec .specialdata]
			height = iadd [sec .floorh] rise

			P_BuildStairInSector sec height speed

			texture = [sec .floorpic]

			; Find next sector to raise
			;   1  Find 2-sided line with same sector side[0]
			;   2. Other side is the next sector to raise
			;   3. Sector has the same floor texture

			loop
				found bool = FALSE

				i s32 = 0
				loop while lt? i [sec .linecount]
					ld2 = [[sec .lines] i]

					front = [ld2 .front]
					back  = [ld2 .back]

					two_sided   = some? (iand [ld2 .flags] ML_TWOSIDED)
					match_front = eq? front sec
					match_back  = ref? back

					if match_back
						match_back = eq? [back .floorpic] texture
					endif

					if and two_sided match_front match_back
						; andrewj: a vanilla bug here, it bumps the height *before*
						; checking that the neighbor sector has a mover in it.
						height = iadd height rise

						if null? [back .specialdata]
							found  = TRUE

							sec    = back
							secnum = conv s32 (pdiff sec [sectors])
							secnum = idivt secnum sector_t.size

							P_BuildStairInSector sec height speed

							; we have a new sector, stop iterating over old one
							break
						endif
					endif

					i = iadd i 1
				endloop

				break if not found
			endloop

			did = TRUE
		endif
	endloop

	return did
end

fun P_BuildStairInSector (sec ^sector_t dest fixed_t speed fixed_t)
	F ^floor_t = Z_Calloc floor_t.size PU_LEVEL

	[F .thinker .func] = THINK_floor

	[F .sector]    = sec
	[F .type]      = fl_raiseFloor
	[F .dest]      = dest
	[F .speed]     = speed
	[F .direction] = +1
	[F .crush]     = TRUE

	P_AddThinker (raw-cast F)
	[sec .specialdata] = F
end

;
; Special Stuff that can not be categorized
;
fun EV_DoDonut (ld ^line_t -> bool)
	did = FALSE

	secnum s32 = -1

	loop
		secnum = P_FindSectorFromLineTag ld secnum
		break if neg? secnum

		s1 = [addr-of [sectors] secnum]

		; already moving?  if so, keep going...
		if null? [s1 .specialdata]
			did = TRUE

			; andrewj: added this, not sure it can really happen though
			if zero? [s1 .linecount]
				I_Print "EV_DoDonut: sector has no linedefs!\n"
				break
			endif

			s2 = getNextSector [[s1 .lines] 0] s1

			; Vanilla Doom does not check if the linedef is one sided.  The
			; game does not crash, but reads invalid memory and causes the
			; sector floor to move "down" to some unknown height.

			if null? s2
				I_Print "EV_DoDonut: model linedef has no second sidedef!\n"
				break
			endif

			i s32 = 0
			loop while lt? i [s2 .linecount]
				ld3 = [[s2 .lines] i]
				s3  = [ld3 .back]

				if ne? s3 s1
					s3_floorh   fixed_t = 0
					s3_floorpic s32     = 1

					if ref? s3
						s3_floorh   = [s3 .floorh]
						s3_floorpic = [s3 .floorpic]
					else
						; e6y
						; s3 is NULL, so
						; s3->floorh is an int at 0000:0000
						; s3->floorpic is a short at 0000:0008
						; Trying to emulate (values above)

						s3_floorh   = 0
						s3_floorpic = 1
					endif

					; spawn rising slime
					raiser = P_DonutMover s2 +1 s3_floorh

					[raiser .type]    = fl_donutRaise
					[raiser .texture] = s3_floorpic
					[raiser .special] = 0

					; spawn lowering donut-hole
					downer = P_DonutMover s1 -1 s3_floorh
					break
				endif

				i = iadd i 1
			endloop
		endif
	endloop

	return did
end

fun P_DonutMover (sec ^sector_t direction s32 dest fixed_t -> ^floor_t)
	F ^floor_t = Z_Calloc floor_t.size PU_LEVEL

	[F .thinker .func] = THINK_floor

	[F .sector]    = sec
	[F .type]      = fl_lowerFloor
	[F .dest]      = dest
	[F .speed]     = (FLOORSPEED / 2)
	[F .direction] = direction
	[F .crush]     = FALSE

	P_AddThinker (raw-cast F)
	[sec .specialdata] = F

	return F
end

;
; Move a floor plane and check for crushing
;
fun T_MoveFloorPlane (sec ^sector_t speed fixed_t dest fixed_t crush bool direction s32 -> result_e)
	if eq? direction -1
		; DOWN
		last  = [sec .floorh]
		new_h = isub last speed

		if lt? new_h dest
			[sec .floorh] = dest

			nofit = P_ChangeSector sec crush
			if nofit
				[sec .floorh] = last
				P_ChangeSector sec crush
			endif

			return RES_pastdest
		endif

		[sec .floorh] = new_h

		nofit = P_ChangeSector sec crush
		if nofit
			[sec .floorh] = last
			P_ChangeSector sec crush

			return RES_crushed
		endif

	elif eq? direction +1
		; UP
		last  = [sec .floorh]
		new_h = iadd last speed

		if gt? new_h dest
			[sec .floorh] = dest

			nofit = P_ChangeSector sec crush
			if nofit
				[sec .floorh] = last
				P_ChangeSector sec crush
			endif

			return RES_pastdest
		endif

		; COULD GET CRUSHED
		[sec .floorh] = new_h

		nofit = P_ChangeSector sec crush
		if nofit
			if not crush
				[sec .floorh] = last
				P_ChangeSector sec crush
			endif

			return RES_crushed
		endif
	endif

	return RES_ok
end
