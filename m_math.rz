;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

;;
;; R_PointToAngle
;;
;; To get a global angle from cartesian coordinates,
;;  the coordinates are flipped until they are in
;;  the first octant of the coordinate system, then
;;  the y (<=x) is scaled and divided by x to get a
;;  tangent (slope) value which is looked up in the
;;  tantoangle[] table.
;;
fun R_PointToAngle2 (x fixed_t y fixed_t x2 fixed_t y2 fixed_t -> angle_t)
	x = isub x2 x
	y = isub y2 y

	if and (zero? x) (zero? y)
		return 0
	endif

	res fixed_t = 0

	if neg? x
		x = ineg x

		if neg? y
			y = ineg y

			if gt? x y
				; octant 4
				res = M_AtanLookup y x
				res = iadd ANG180 res
			else
				; octant 5
				res = M_AtanLookup x y
				res = isub ANG270 res
				res = isub res 1
			endif
		else
			if gt? x y
				; octant 3
				res = M_AtanLookup y x
				res = isub ANG180 res
				res = isub res 1
			else
				; octant 2
				res = M_AtanLookup x y
				res = iadd ANG90 res
			endif
		endif

	else  ;  x >= 0

		if neg? y
			y = ineg y

			if gt? x y
				; octant 8
				res = M_AtanLookup y x
				res = isub ANG0 res
			else
				; octant 7
				res = M_AtanLookup x y
				res = iadd ANG270 res
			endif
		else
			if gt? x y
				; octant 0
				res = M_AtanLookup y x
			else
				; octant 1
				res = M_AtanLookup x y
				res = isub ANG90 res
				res = isub res 1
			endif
		endif
	endif

	return res
end

#private

fun M_AtanLookup (num fixed_t den fixed_t -> angle_t)
	; the tantoangle array has 2048+1 entries
	idx u32 = 2048

	if ge? den 512
		u_num u32 = num
		u_den u32 = den

		; note that this can overflow, but we cannot "fix" this
		; code without potentially causing some demos to desync.
		u_num = ishl u_num 3
		u_den = ishr u_den 8

		idx = idivt u_num u_den
		idx = imin  idx 2048
	endif

	return [tantoangle idx]
end

#public

fun R_PointToDist (x1 fixed_t y1 fixed_t x2 fixed_t y2 fixed_t -> fixed_t)
	dx = iabs (isub x2 x1)
	dy = iabs (isub y2 y1)

	num = imin dx dy
	den = imax dx dy

	frac fixed_t = 0

	; fix crashes in udm1.wad
	if ne? den 0
		frac = FixedDiv num den
	endif

	; use as cosine
	frac  = ishr frac 5

	angle = [tantoangle frac]
	angle = ishr angle ANGLEFINESHIFT

	dist = FixedDiv den [finecosine angle]

	return dist
end

;;
;; P_ApproxDistance
;; Gives an estimation of distance (not exact)
;;
fun P_ApproxDistance (dx fixed_t dy fixed_t -> fixed_t)
	dx = iabs dx
	dy = iabs dy

	d_min = imin dx dy
	d_min = ishr d_min 1

	res = iadd dx dy
	res = isub res d_min
	return res
end
