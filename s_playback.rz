;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

extern-var gamemap s32

extern-fun D_ConsolePlayerListener (-> ^soundmobj_t)


; when to clip out sounds
; Does not fit the large outdoor areas.
const S_CLIPPING_DIST = (1200 * FRACUNIT)

; Distance tp origin when sounds should be maxed out.
; This should relate to movement clipping resolution
; (see BLOCKMAP handling).
; In the source code release: (160*FRACUNIT).  Changed back to the
; Vanilla value of 200 (why was this changed?)
const S_CLOSE_DIST = (200 * FRACUNIT)

; The range over which sound attenuates
const S_ATTENUATOR = ((S_CLIPPING_DIST - S_CLOSE_DIST) >> FRACBITS)

; Stereo separation
const S_STEREO_SWING = (96 * FRACUNIT)

const NORM_PRIORITY = 64
const NORM_SEP      = 128

type channel_t struct
	.sfxinfo ^sfxinfo_t    ; sound information (if null, channel avail.)
	.origin  ^soundmobj_t  ; origin of sound
	.handle   s32          ; handle of the sound being played
	.pitch    s32
end

#public

; Maximum volume of a sound effect.
; Internal default is max out of 0-15.
var sfxVolume s32 = 8

; Maximum volume of music.
var musicVolume s32 = 8

; Number of channels to use
var snd_channels s32 = 8

#private

; The set of channels available.
zero-var channels [MAX_CHANNELS]channel_t

; Internal volume level, ranging from 0-127
var snd_volume s32 = 0

; Whether songs are mus_paused
var mus_paused bool = FALSE

; Music currently being played
var mus_playing ^musicinfo_t = NULL

#public

;
; Initializes sound stuff, including volume
; Sets up channels, SFX and music volume.
; volume values are 0 - 127.
;
fun S_Init (sfx_vol s32 music_vol s32)
	S_SetSfxVolume   sfx_vol
	S_SetMusicVolume music_vol

	if gt? [snd_channels] MAX_CHANNELS
		[snd_channels] = MAX_CHANNELS
	endif

	; free all channels for use
	c s32 = 0
	loop while lt? c [snd_channels]
		[channels c .sfxinfo] = NULL
		c = iadd c 1
	endloop

	; no sounds are playing, and music is not paused
	[mus_paused] = FALSE

	; Note that sounds have not been cached (yet).
end

fun S_InitHacx ()
	; andrewj: update music names for HacX 1.2
	i s32 = 0
	loop while lt? i 35
		k = iadd i mus_runnin
		[S_music k .name] = [hacx_music i]
		i = iadd i 1
	endloop
end

;
; Per level startup code.
; Kills playing sounds at start of level,
;   determines music if any, changes music.
;
fun S_Start (mus musicenum_t)
	c s32 = 0
	loop while lt? c [snd_channels]
		info = [channels c .sfxinfo]

		if ref? info
			S_StopChannel c
		endif

		c = iadd c 1
	endloop

	; start new music for the level
	[mus_paused] = FALSE

	S_ChangeMusic mus TRUE
end

fun S_StopSound (origin ^soundmobj_t)
	c s32 = 0
	loop while lt? c [snd_channels]
		info = [channels c .sfxinfo]
		org  = [channels c .origin]

		if and (ref? info) (eq? org origin)
			S_StopChannel c
		endif

		c = iadd c 1
	endloop
end

fun S_StartSound (origin ^soundmobj_t sfx_id s32)
	; check for bogus sound #
	if or (lt? sfx_id 1) (ge? sfx_id NUMSFX)
		I_Error "Bad sfx number"
	endif

	sfx = [addr-of S_sfx sfx_id]

	; Initialize sound parameters
	vol   s32 = [snd_volume]
	sep   s32 = NORM_SEP
	pitch s32 = NORM_PITCH

	; Check to see if it is audible, and if not modify the params
	listener = D_ConsolePlayerListener

	if and (ref? origin) (ne? origin listener)
		vol = CalcVolume     listener origin
		sep = CalcSeparation listener origin

		if le? vol 0
			return
		endif
	endif

	; hacks to vary the sfx pitches
	if and (ge? sfx_id sfx_sawup) (le? sfx_id sfx_sawhit)
		diff  = M_Random
		diff  = isub 8 (iand diff 15)
		pitch = iadd pitch diff

	elif and (ne? sfx_id sfx_itemup) (ne? sfx_id sfx_tink)
		diff  = M_Random
		diff  = isub 16 (iand diff 31)
		pitch = iadd pitch diff
	endif

	pitch = ClampPitch pitch

	; kill old sound
	S_StopSound origin

	; load sound data if needed
	data_h = I_FindAllocatedSound sfx
	if null? data_h
		data_h = S_LoadSound sfx

		; loading may fail, e.g. bad lump data
		if null? data_h
			return
		endif
	endif

	; try to find a channel
	cnum = S_GetChannel origin sfx

	if neg? cnum
		return
	endif

	ch = [addr-of channels cnum]

	[ch .pitch]  = pitch
	[ch .handle] = I_StartSound data_h cnum vol sep pitch
end

fun S_StartHudSound (pl_obj ^soundmobj_t sfx_id s32)
	listener = D_ConsolePlayerListener

	if eq? pl_obj listener
		S_StartSound NULL sfx_id
	endif
end

fun S_LoadSound (sfx ^sfxinfo_t -> ^sounddata_t)
	name = stack-var [16]uchar

	M_StringCopy   name "ds" 16
	M_StringConcat name [sfx .name] 16

	lump = W_GetNumForName name

	if neg? lump
		return NULL
	endif

	data   = W_CacheLumpNum lump
	length = W_LumpLength   lump

	handle = I_CreateAllocatedSound sfx data length
	return handle
end

;
; Stop and resume music, during game PAUSE.
;
fun S_PauseSound ()
	if ref? [mus_playing]
		if not [mus_paused]
			I_PauseSong
			[mus_paused] = TRUE
		endif
	endif
end

fun S_ResumeSound ()
	if ref? [mus_playing]
		if [mus_paused]
			I_ResumeSong
			[mus_paused] = FALSE
		endif
	endif
end

;
; Updates music & sounds
;
fun S_UpdateSounds (listener ^soundmobj_t)
	I_UpdateSound

	cnum s32 = 0
	loop while lt? cnum [snd_channels]
		ch = [addr-of channels cnum]

		if ref? [ch .sfxinfo]
			if I_SoundIsPlaying [ch .handle]
				; check non-local sounds for distance clipping
				if ref? [ch .origin]
					vol = CalcVolume     listener [ch .origin]
					sep = CalcSeparation listener [ch .origin]

					if pos? vol
						I_UpdateSoundParams [ch .handle] vol sep
					else
						S_StopChannel cnum
					endif
				endif
			else
				; if channel is allocated but sound has stopped, free it
				S_StopChannel cnum
			endif
		endif

		cnum = iadd cnum 1
	endloop
end

fun S_SetMusicVolume (volume s32)
	if or (neg? volume) (gt? volume 127)
		I_Error "Attempt to set illegal music volume"
	endif

	I_SetMusicVolume volume
end

fun S_SetSfxVolume (volume s32)
	if or (neg? volume) (gt? volume 127)
		I_Error "Attempt to set illegal sfx volume"
	endif

	[snd_volume] = volume
end

;
; Starts some music with the music id found in sounds.h.
;
fun S_StartMusic (mus_id s32)
	S_ChangeMusic mus_id FALSE
end

fun S_ChangeMusic (mus_id s32 looping bool)
	if or (le? mus_id mus_None) (ge? mus_id NUMMUSIC)
		I_Error "Bad music number!"
	endif

	; The Doom IWAD file has two versions of the intro music:
	;    d_intro and d_introa.  The latter is for OPL playback.

	if eq? mus_id mus_intro
		mus_id = mus_introa
	endif

	song = [addr-of S_music mus_id]

	; already playing?
	if eq? [mus_playing] song
		return
	endif

	; shutdown old music
	S_StopMusic

	; lookup lump
	namebuf = stack-var [10]uchar

	M_StringCopy   namebuf "d_" 10
	M_StringConcat namebuf [song .name] 10

	lumpnum = W_GetNumForName namebuf

	length         = W_LumpLength   lumpnum
	[song .data]   = W_LoadLumpNum  lumpnum PU_STATIC
	[song .handle] = I_RegisterSong [song .data] length

	if null? [song .handle]
		Z_Free [song .data]
		[song .data] = NULL
		return
	endif

	I_PlaySong [song .handle] looping

	[mus_playing] = song
end

fun S_StopMusic ()
	song = [mus_playing]

	if ref? song
		if [mus_paused]
			I_ResumeSong
		endif

		I_StopSong
		I_UnRegisterSong [song .handle]

		Z_Free [song .data]

		[song .data]   = NULL
		[song .handle] = NULL
	endif

	[mus_playing] = NULL
	[mus_paused] = FALSE
end

fun S_MusicPlaying (-> bool)
	return I_MusicIsPlaying
end

#private

fun S_StopChannel (cnum s32)
	ch = [addr-of channels cnum]

	if ref? [ch .sfxinfo]
		; stop the sound playing
		handle = [ch .handle]

		if I_SoundIsPlaying handle
			I_StopSound handle
		endif
	endif

	[ch .sfxinfo] = NULL
	[ch .origin]  = NULL
end

;
; S_GetChannel
; If none available, return -1.  Otherwise channel #.
;
fun S_GetChannel (origin ^soundmobj_t sfxinfo ^sfxinfo_t -> s32)
	; Find an open channel
	cnum s32 = 0

	loop while lt? cnum [snd_channels]
		ch = [addr-of channels cnum]
		break if null? [ch .sfxinfo]

		if ref? origin
			if eq? [ch .origin] origin
				S_StopChannel cnum
				break
			endif
		endif

		cnum = iadd cnum 1
	endloop

	; None available, look for lower priority
	if ge? cnum [snd_channels]
		cnum = 0

		loop while lt? cnum [snd_channels]
			ch  = [addr-of channels cnum]
			pri = [[ch .sfxinfo] .priority]

			break if ge? pri [sfxinfo .priority]

			cnum = iadd cnum 1
		endloop

		if ge? cnum [snd_channels]
			; FUCK!  No lower priority.  Sorry, Charlie.
			return -1
		endif

		; Otherwise, kick out lower priority.
		S_StopChannel cnum
	endif

	; channel is decided to be cnum.
	ch = [addr-of channels cnum]

	[ch .sfxinfo] = sfxinfo
	[ch .origin]  = origin

	return cnum
end

fun CalcVolume (listener ^soundmobj_t source ^soundmobj_t -> s32)
	; calculate the distance to sound origin, clip it if necessary
	dx = isub [listener .x] [source .x]
	dy = isub [listener .y] [source .y]

	dx   = iabs dx
	dy   = iabs dy
	dmin = imin dx dy

	; From _GG1_ p.428. Approx. euclidean distance fast.
	approx_dist = iadd dx dy
	approx_dist = isub approx_dist (ishr dmin 1)

	; boss maps let you hear the Cyberdemon stomping around.
	; andrewj: this calc is different than vanilla!
	if eq? [gamemap] 8
		approx_dist = imin approx_dist S_CLIPPING_DIST
		approx_dist = ishr approx_dist 1
	endif

	if gt? approx_dist S_CLIPPING_DIST
		return 0
	endif

	if lt? approx_dist S_CLOSE_DIST
		return [snd_volume]
	endif

	; distance effect
	approx_dist = isub S_CLIPPING_DIST approx_dist

	vol s32 = FixedMul approx_dist [snd_volume]
	vol = idivt vol S_ATTENUATOR

	return vol
end

fun CalcSeparation (listener ^soundmobj_t source ^soundmobj_t -> s32)
	same_x = eq? [listener .x] [source .x]
	same_y = eq? [listener .y] [source .y]

	if and same_x same_y
		return NORM_SEP
	endif

	; angle of source from POV of listener
	angle = R_PointToAngle2 [listener .x] [listener .y] [source .x] [source .y]

	angle = isub angle [listener .angle]
	angle = ishr angle ANGLEFINESHIFT

	swing = FixedMul S_STEREO_SWING [finesine angle]
	swing = ishr swing FRACBITS

	sep = isub NORM_SEP swing
	return sep
end

fun ClampPitch (x s32 -> s32)
	x = imax x 0
	x = imin x 255
	return x
end
