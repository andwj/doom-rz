;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const GLOWSPEED    =  8
const STROBEBRIGHT =  5
const FASTDARK     = 15
const SLOWDARK     = 35

type fireflicker_t struct
	.thinker    thinker_t
	.sector    ^sector_t

	.count      s32
	.maxlight   s16
	.minlight   s16
end

type lightflash_t struct
	.thinker    thinker_t
	.sector    ^sector_t

	.count      s32
	.maxlight   s16
	.minlight   s16

	.maxtime    s32
	.mintime    s32
end

type strobe_t struct
	.thinker    thinker_t
	.sector    ^sector_t

	.count      s32
	.minlight   s16
	.maxlight   s16

	.darktime   s32
	.brighttime s32
end

type glow_t struct
	.thinker    thinker_t
	.sector    ^sector_t

	.minlight   s16
	.maxlight   s16
	.direction  s16
	._pad       s16
end

;
; P_SpawnFireFlicker
;
fun P_SpawnFireFlicker (sec ^sector_t)
	flick ^fireflicker_t = Z_Calloc fireflicker_t.size PU_LEVEL

	[flick .thinker .func] = THINK_flicker

	[flick .sector]   = sec
	[flick .maxlight] = [sec .light]
	[flick .minlight] = P_FindMinSurroundingLight sec [sec .light]
	[flick .minlight] = iadd [flick .minlight] 16
	[flick .count]    = 4

	P_AddThinker (raw-cast flick)

	; note that we are resetting sector attributes.
	; nothing else is special about it during gameplay.
	[sec .special] = 0
end

fun T_FireFlicker (flick ^fireflicker_t)
	[flick .count] = isub [flick .count] 1

	if zero? [flick .count]
		r = P_Random
		r = iand r 3
		r = imul r 16
		amount = conv s16 r

		sec = [flick .sector]

		if lt? (isub [sec .light] amount) [flick .minlight]
			[sec .light] = [flick .minlight]
		else
			[sec .light] = isub [flick .maxlight] amount
		endif

		[flick .count] = 4
	endif
end

;
; P_SpawnLightFlash
; Do flashing lights.
;
fun P_SpawnLightFlash (sec ^sector_t)
	flash ^lightflash_t = Z_Calloc lightflash_t.size PU_LEVEL

	[flash .thinker .func] = THINK_flash

	[flash .sector]   = sec
	[flash .maxlight] = [sec .light]
	[flash .minlight] = P_FindMinSurroundingLight sec [sec .light]
	[flash .maxtime]  = 64
	[flash .mintime]  = 7

	r = P_Random
	r = iand r [flash .maxtime]
	[flash .count] = iadd r 1

	P_AddThinker (raw-cast flash)

	[sec .special] = 0
end

fun T_LightFlash (flash ^lightflash_t)
	[flash .count] = isub [flash .count] 1

	if zero? [flash .count]
		sec = [flash .sector]
		r   = P_Random

		if eq? [sec .light] [flash .maxlight]
			r = iand r [flash .mintime]
			[sec   .light] = [flash .minlight]
			[flash .count] = iadd r 1
		else
			r = iand r [flash .maxtime]
			[sec   .light] = [flash .maxlight]
			[flash .count] = iadd r 1
		endif
	endif
end

;
; P_SpawnStrobeFlash
; We're gonna have a ball tonight...
;
fun P_SpawnStrobeFlash (sec ^sector_t darktime s32 sync bool)
	flash ^strobe_t = Z_Calloc strobe_t.size PU_LEVEL

	[flash .thinker .func] = THINK_strobe

	[flash .sector]     = sec
	[flash .darktime]   = darktime
	[flash .brighttime] = STROBEBRIGHT
	[flash .maxlight]   = [sec .light]
	[flash .minlight]   = P_FindMinSurroundingLight sec [sec .light]

	if eq? [flash .minlight] [flash .maxlight]
		[flash .minlight] = 0
	endif

	if sync
		[flash .count] = 1
	else
		r = P_Random
		r = iand r 7
		[flash .count] = iadd r 1
	endif

	P_AddThinker (raw-cast flash)

	[sec .special] = 0
end

fun T_StrobeFlash (flash ^strobe_t)
	[flash .count] = isub [flash .count] 1

	if zero? [flash .count]
		sec = [flash .sector]

		if eq? [sec .light] [flash .minlight]
			[sec   .light] = [flash .maxlight]
			[flash .count] = [flash .brighttime]
		else
			[sec   .light] = [flash .minlight]
			[flash .count] = [flash .darktime]
		endif
	endif
end

;
; Spawn glowing light
;
fun P_SpawnGlowingLight (sec ^sector_t)
	glow ^glow_t = Z_Calloc glow_t.size PU_LEVEL

	[glow .thinker .func] = THINK_glow

	[glow .sector]    = sec
	[glow .maxlight]  = [sec .light]
	[glow .minlight]  = P_FindMinSurroundingLight sec [sec .light]
	[glow .direction] = -1

	P_AddThinker (raw-cast glow)

	[sec .special] = 0
end

fun T_Glow (glow ^glow_t)
	sec = [glow .sector]

	if neg? [glow .direction]
		; DOWN
		light = isub [sec .light] GLOWSPEED

		if gt? light [glow .minlight]
			[sec .light] = light
		else
			[glow .direction] = +1
		endif

	elif pos? [glow .direction]
		; UP
		light = iadd [sec .light] GLOWSPEED

		if lt? light [glow .maxlight]
			[sec .light] = light
		else
			[glow .direction] = -1
		endif
	endif
end

;
; Start strobing lights (usually from a trigger)
;
fun EV_StartLightStrobing (ld ^line_t)
	secnum s32 = -1

	loop
		secnum = P_FindSectorFromLineTag ld secnum
		break if neg? secnum

		sec = [addr-of [sectors] secnum]

		if null? [sec .specialdata]
			P_SpawnStrobeFlash sec SLOWDARK FALSE
		endif
	endloop
end

;
; TURN LINE'S TAG LIGHTS OFF
;
fun EV_LightTurnOff (ld ^line_t)
	i s32 = 0
	loop while lt? i [numsectors]
		sec = [addr-of [sectors] i]

		if eq? [sec .tag] [ld .tag]
			[sec .light] = P_FindMinSurroundingLight sec [sec .light]
		endif

		i = iadd i 1
	endloop
end

;
; TURN LINE'S TAG LIGHTS ON
; bright = 0 means to search for highest light level in surrounding sectors.
;
fun EV_LightTurnOn (ld ^line_t bright s16)
	i s32 = 0
	loop while lt? i [numsectors]
		sec = [addr-of [sectors] i]

		if eq? [sec .tag] [ld .tag]
			if zero? bright
				bright = P_FindMaxSurroundingLight sec 0
			endif

			[sec .light] = bright
		endif

		i = iadd i 1
	endloop
end

;
; Find maximum light from an adjacent sector
;
fun P_FindMaxSurroundingLight (sec ^sector_t low s16 -> s16)
	high = low

	i s32 = 0
	loop while lt? i [sec .linecount]
		ld    = [[sec .lines] i]
		check = getNextSector ld sec

		if ref? check
			high = imax high [check .light]
		endif

		i = iadd i 1
	endloop

	return low
end

;
; Find minimum light from an adjacent sector
;
fun P_FindMinSurroundingLight (sec ^sector_t high s16 -> s16)
	low = high

	i s32 = 0
	loop while lt? i [sec .linecount]
		ld    = [[sec .lines] i]
		check = getNextSector ld sec

		if ref? check
			low = imin low [check .light]
		endif

		i = iadd i 1
	endloop

	return low
end
