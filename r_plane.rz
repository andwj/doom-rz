;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const FREE_COLUMN = 0xff

type visplane_t struct
	; horizontal range, inclusive.
	; visplane is empty when x1 > x2.
	.x1  s32
	.x2  s32

	; sector properties
	.height   fixed_t
	.pic      s32
	.light    s16
	.__pad    u16

	; vertical range of pixels for each screen column.
	; top value can be FREE_COLUMN for an unused column.
	.top      [SCREENWIDTH]u8
	.bottom   [SCREENWIDTH]u8
end

; Here comes the obnoxious "visplane".
; andrewj: raised limit from 128 to 512.
const MAXVISPLANES = 512

zero-var visplanes [MAXVISPLANES]visplane_t
zero-var numvisplane s32

zero-var floorplane   ^visplane_t
zero-var ceilingplane ^visplane_t

;
; Clip values are the solid pixel bounding the range.
; floorclip starts out SCREENHEIGHT, ceilingclip starts out -1.
;
zero-var floorclip   [SCREENWIDTH]s16
zero-var ceilingclip [SCREENWIDTH]s16

;
; spanstart holds the start of a plane span.
;
zero-var spanstart [SCREENHEIGHT]s32

;
; texture mapping
;
const ANGLESKYSHIFT = 22

zero-var planelight   s32
zero-var planeheight  fixed_t

zero-var basexscale fixed_t
zero-var baseyscale fixed_t

;
; R_ClearPlanes
; At begining of frame.
;
fun R_ClearPlanes ()
	[numvisplane] = 0

	; opening / clipping determination
	x s32 = 0
	loop while lt? x [viewwidth]
		[floorclip   x] = conv s16 [viewheight]
		[ceilingclip x] = -1
		x = iadd x 1
	endloop

	; left to right mapping
	ang = isub [viewangle] ANG90
	ang = ishr ang ANGLEFINESHIFT

	; scale will be unit scale at SCREENWIDTH/2 distance
	[basexscale] = FixedDiv [finecosine ang] [centerxfrac]
	[baseyscale] = FixedDiv [finesine   ang] [centerxfrac]
	[baseyscale] = ineg [baseyscale]
end

;
; R_DrawPlaneSpan
;
; BASIC PRIMITIVE
;
fun R_DrawPlaneSpan (x1 s32 x2 s32 y s32)
	distance = FixedMul [planeheight] [yslope y]

	; andrewj: guard against overflow when 'y' is near center of screen
	distance = imax distance 1

	[ds .xstep] = FixedMul [basexscale] distance
	[ds .ystep] = FixedMul [baseyscale] distance

	length = FixedMul distance [distscale x1]
	ang    = iadd [viewangle] [xtoviewangle x1]
	ang    = ishr ang ANGLEFINESHIFT

	dx     = FixedMul [finecosine ang] length
	dy     = FixedMul [finesine   ang] length

	[ds .xfrac] = iadd   [viewx] dx
	[ds .yfrac] = isub 0 [viewy] dy

	if ref? [fixedcolormap]
		[ds .colormap] = [fixedcolormap]
	else
		index = ishr distance LIGHTZSHIFT
		index = imin index (MAXLIGHTZ - 1)

		[ds .colormap] = [zlight [planelight] index]
	endif

	[ds .y]  = y
	[ds .x1] = x1
	[ds .x2] = x2

	; high or low detail
	call [spanfunc]
end

;
; R_FindPlane
;
fun R_FindPlane (height fixed_t pic s32 light s16 -> ^visplane_t)
	; all skys map together
	if eq? pic [skyflatnum]
		height = 0
		light  = 0
	endif

	check = R_FindCompatiblePlane height pic light

	if null? check
		; andrewj: don't die when we run out of visplanes.
		;          [ rendering will be glitched though ]
		if ge? [numvisplane] MAXVISPLANES
			return NULL
		endif

		check = R_NewVisplane height pic light
	endif

	return check
end

fun R_FindCompatiblePlane (height fixed_t pic s32 light s16 -> ^visplane_t)
	i s32 = [numvisplane]

	loop while pos? i
		i  = isub i 1
		pl = [addr-of visplanes i]

		if eq? height [pl .height]
			if eq? pic [pl .pic]
				if eq? light [pl .light]
					return pl
				endif
			endif
		endif
	endloop

	return NULL  ; not found
end

fun R_NewVisplane (height fixed_t pic s32 light s16 -> ^visplane_t)
	pl = [addr-of visplanes [numvisplane]]

	[numvisplane] = iadd [numvisplane] 1

	[pl .height] = height
	[pl .pic]    = pic
	[pl .light]  = light
	[pl .x1]     = SCREENWIDTH
	[pl .x2]     = -1

	I_MemSet [addr-of pl .top] FREE_COLUMN (SCREENWIDTH * 1)

	return pl
end

;
; R_CheckPlane
;
fun R_CheckPlane (pl ^visplane_t x1 s32 x2 s32 -> ^visplane_t)
	; check that the needed columns are free.
	; if not, we allocate another visplane for this X range.

	cx1 = imax x1 [pl .x1]
	cx2 = imin x2 [pl .x2]

	x = cx1
	loop while le? x cx2
		jump newplane if ne? [pl .top x] FREE_COLUMN
		x = iadd x 1
	endloop

	; use the same one, but update X range.
	; the caller will shortly compute the top and bottom values.
	[pl .x1] = imin [pl .x1] x1
	[pl .x2] = imax [pl .x2] x2

	return pl

newplane:
	; andrewj: it is possible there is a different visplane with the same
	;          properties which could accommodate this X range. but the
	;          extra searching time (and code) is probably not worth it.

	; andrewj: don't die when we run out of visplanes.
	;          [ rendering will be glitched though ]
	if ge? [numvisplane] MAXVISPLANES
		return NULL
	endif

	pl = R_NewVisplane [pl .height] [pl .pic] [pl .light]

	[pl .x1] = x1
	[pl .x2] = x2

	return pl
end

;
; R_DrawPlanes
; At the end of each frame.
;
fun R_DrawPlanes ()
	i s32 = [numvisplane]
	loop while pos? i
		i  = isub i 1
		pl = [addr-of visplanes i]

		if le? [pl .x1] [pl .x2]
			if eq? [pl .pic] [skyflatnum]
				R_DrawSkyPlane pl
			else
				R_DrawNormalPlane pl
			endif
		endif
	endloop
end

fun R_DrawSkyPlane (pl ^visplane_t)
	[dc .iscale] = [pspriteiscale]

	; Sky is always drawn full bright, i.e. colormap table #0.
	; Because of this hack, sky is not affected by INVUL inverse mapping.
	[dc .colormap]   = [colormaps]
	[dc .texturemid] = [skytexturemid]

	x = [pl .x1]
	loop while le? x [pl .x2]
		top = [pl .top x]

		if ne? top FREE_COLUMN
			[dc .x]  = x
			[dc .yl] = conv s32 [pl .top    x]
			[dc .yh] = conv s32 [pl .bottom x]

			ang = iadd [viewangle] [xtoviewangle x]
			ang = ishr ang ANGLESKYSHIFT

			[dc .source] = R_GetColumn [skytexture] ang FALSE

			call [colfunc]
		endif

		x = iadd x 1
	endloop
end

fun R_DrawNormalPlane (pl ^visplane_t)
	[ds .source] = R_GetFlat [pl .pic]

	[spanfunc] = fun R_DrawSpan

	[planeheight] = isub [pl .height] [viewz]
	[planeheight] = iabs [planeheight]

	light = conv s32 [pl .light]
	light = ishr light LIGHTSEGSHIFT
	light = iadd light [extralight]
	light = imax light 0
	light = imin light (LIGHTLEVELS - 1)

	[planelight] = light

	; we iterate over *pairs* of columns, the first pair starts at one
	; pixel to the left of the visplane's X range, and the last pair
	; ends at one pixel to the right of the X range.

	x1 s32 = isub [pl .x1] 1
	x2 s32 = iadd [pl .x2] 1

	x = x1
	loop while lt? x x2
		t1 u8 = FREE_COLUMN
		b1 u8 = 0

		if gt? x x1
			t1 = [pl .top    x]
			b1 = [pl .bottom x]
		endif

		x+1 = iadd x 1

		t2 u8 = FREE_COLUMN
		b2 u8 = 0

		if lt? x+1 x2
			t2 = [pl .top    x+1]
			b2 = [pl .bottom x+1]
		endif

		R_MakeSpans x t1 b1 t2 b2

		x = x+1
	endloop
end

;
; R_MakeSpans
;   t1 = top of column, left side.   b1 = bottom.
;   t2 = top of column, right side.  b2 = bottom.
;
; Either t1 or t2 (or both) can be FREE_COLUMN.  In those cases,
; the corresponding bottom value will always be something less,
; hence these columns are never drawn.
;
fun R_MakeSpans (x s32 t1 u8 b1 u8 t2 u8 b2 u8)
	; check for places where a horizontal span ends...
	; for these, we can finally draw something!

	loop
		break if ge? t1 t2
		break if gt? t1 b1

		R_DrawPlaneSpan [spanstart t1] x (conv s32 t1)
		t1 = iadd t1 1
	endloop

	loop
		break if le? b1 b2
		break if lt? b1 t1

		R_DrawPlaneSpan [spanstart b1] x (conv s32 b1)
		b1 = isub b1 1
	endloop

	; check for places where a horizontal span begins...

	x = iadd x 1

	loop
		break if ge? t2 t1
		break if gt? t2 b2

		[spanstart t2] = x
		t2 = iadd t2 1
	endloop

	loop
		break if le? b2 b1
		break if lt? b2 t2

		[spanstart b2] = x
		b2 = isub b2 1
	endloop
end
