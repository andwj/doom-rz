;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

extern-var skyflatnum s32

extern-fun G_AddPlayerStart (idx s32 mthing ^mapthing_t)
extern-fun G_AddDeathmatchStart (mthing ^mapthing_t)
extern-fun G_SpawnPlayer (mthing ^mapthing_t)
extern-fun G_PlayerReborn (idx s32)

#public

zero-var nomonsters    bool  ; checkparm of -nomonsters
zero-var respawnparm   bool  ; checkparm of -respawn
zero-var fastparm      bool  ; checkparm of -fast

zero-var respawnmonsters bool

zero-var totalkills   s32  ; for intermission
zero-var totalitems   s32  ;
zero-var totalsecret  s32  ;

#private

; Skill flags.
const MTF_EASY   = 1
const MTF_NORMAL = 2
const MTF_HARD   = 4

; Deaf monsters do not react to sound.
const MTF_AMBUSH = 8

const FRICTION = 0xe800  ; 0.90625
const GRAVITY  = FRACUNIT

const MAXMOVE    = (30 * FRACUNIT)
const STOPSPEED  = (FRACUNIT / 16)
const FLOATSPEED = (FRACUNIT * 4)

const BODYQUESIZE = 32
const ITEMQUESIZE = 128

zero-var bodyque      [BODYQUESIZE]^mobj_t
zero-var bodyqueslot  s32

zero-var itemrespawnque  [ITEMQUESIZE]mapthing_t
zero-var itemrespawntime [ITEMQUESIZE]s32
zero-var iquehead  s32
zero-var iquetail  s32

zero-var null_subst_mobj mobj_t

; Use a heuristic approach to detect infinite state cycles: Count the number
; of times the loop in P_SetMobjState() executes and exit with an error once
; an arbitrary very large limit is reached.

const MOBJ_CYCLE_LIMIT = 1000000

;
; P_SetMobjState
; Returns true if the mobj is still present.
;
fun P_SetMobjState (mo ^mobj_t stnum statenum_t -> bool)
	cycle_counter s32 = 0

	loop
		if eq? stnum S_NULL
			[mo .state] = NULL
			P_RemoveMobj mo
			return FALSE
		endif

		P_CopyStateToMobj mo stnum

		st = [mo .state]

		; Call action functions when the state is set.
		func = [st .action .acp1]
		if ref? func
			call func (raw-cast mo)
		endif

		if some? [mo .tics]
			return TRUE
		endif

		cycle_counter = iadd cycle_counter 1
		if gt? cycle_counter MOBJ_CYCLE_LIMIT
			I_Error "P_SetMobjState: Infinite state cycle detected!"
		endif

		stnum = [st .next]
	endloop
end

#private

fun P_CopyStateToMobj (mo ^mobj_t stnum statenum_t)
	st = [addr-of states stnum]

	[mo .state]  = st
	[mo .tics]   = [st .tics]
	[mo .sprite] = [st .sprite]
	[mo .frame]  = [st .frame]

	if some? [st .bright]
		[mo .frame] = ior [mo .frame] FF_FULLBRIGHT
	endif
end

;
; P_ExplodeMissile
;
fun P_ExplodeMissile (mo ^mobj_t)
	info = [mo .info]

	[mo .momx] = 0
	[mo .momy] = 0
	[mo .momz] = 0

	P_SetMobjState mo [info .deathstate]

	r = P_Random
	r = iand r 3
	[mo .tics] = isub [mo .tics] r
	[mo .tics] = imax [mo .tics] 1

	[mo .flags] = iand [mo .flags] (~ MF_MISSILE)

	if some? [info .deathsound]
		S_StartSound (raw-cast mo) [info .deathsound]
	endif
end

;
; P_XYMovement
;
fun P_XYMovement (mo ^mobj_t)
	info = [mo .info]

	skullfly = some? (iand [mo .flags] MF_SKULLFLY)

	if and skullfly (zero? [mo .momx]) (zero? [mo .momy])
		; the skull slammed into something
			[mo .flags] = iand [mo .flags] (~ MF_SKULLFLY)
			[mo .momx]  = 0
			[mo .momy]  = 0
			[mo .momz]  = 0

			P_SetMobjState mo [info .spawnstate]
		return
	endif

	player = [mo .player]

	[mo .momx] = imin [mo .momx] MAXMOVE
	[mo .momy] = imin [mo .momy] MAXMOVE
	[mo .momx] = imax [mo .momx] (- MAXMOVE)
	[mo .momy] = imax [mo .momy] (- MAXMOVE)

	; apply momentum to move the object
	xmove = [mo .momx]
	ymove = [mo .momy]

	loop
		; andrewj: vanilla is buggy here, it ought to check -MAXMOVE/2 too
		if or (gt? xmove (MAXMOVE / 2)) (gt? ymove (MAXMOVE / 2))
			try_x = iadd [mo .x] (idivt xmove 2)
			try_y = iadd [mo .y] (idivt ymove 2)

			xmove = ishr xmove 1
			ymove = ishr ymove 1
		else
			try_x = iadd [mo .x] xmove
			try_y = iadd [mo .y] ymove

			xmove = 0
			ymove = 0
		endif

		moved = P_TryMove mo try_x try_y

		; blocked move?
		if not moved
			if ref? player
				; try to slide along it
				P_SlideMove mo

			elif some? (iand [mo .flags] MF_MISSILE)
				; explode a missile, unless it hit the sky.
				if ref? [ceilingline]
					back = [[ceilingline] .back]
					if ref? back
						if eq? [back .ceilpic] [skyflatnum]
							P_RemoveMobj mo
							return
						endif
					endif
				endif

				P_ExplodeMissile mo
			else
				[mo .momx] = 0
				[mo .momy] = 0
			endif
		endif

		break if zero? (ior xmove ymove)
	endloop

	; slow down

	if some? (iand [mo .flags] (MF_MISSILE | MF_SKULLFLY))
		return  ; no friction for missiles ever
	endif

	if gt? [mo .z] [mo .floorz]
		return  ; no friction when airborne
	endif

	if some? (iand [mo .flags] MF_CORPSE)
		; keep sliding if halfway off a step with some momentum
		px = gt? [mo .momx] (FRACUNIT / 4)
		py = gt? [mo .momy] (FRACUNIT / 4)
		nx = lt? [mo .momx] ((- FRACUNIT) / 4)
		ny = lt? [mo .momy] ((- FRACUNIT) / 4)

		if or px py nx ny
			sec = [[mo .subsector] .sector]
			if ne? [mo .floorz] [sec .floorh]
				return
			endif
		endif
	endif

	px = gt? [mo .momx] (- STOPSPEED)
	py = gt? [mo .momy] (- STOPSPEED)
	nx = lt? [mo .momx] STOPSPEED
	ny = lt? [mo .momy] STOPSPEED

	want_move bool = FALSE
	if ref? player
		if some? [player .cmd .forwardmove]
			want_move = TRUE
		endif
		if some? [player .cmd .sidemove]
			want_move = TRUE
		endif
	endif

	if and px py nx ny (not want_move)
		; if in a walking frame, go to standing frame
		if ref? player
			pmo  = [player .mo]
			run1 = [addr-of states S_PLAY_RUN1]
			run4 = [addr-of states S_PLAY_RUN4]

			if and (ge? [pmo .state] run1) (le? [pmo .state] run4)
				P_SetMobjState pmo S_PLAY
			endif
		endif

		[mo .momx] = 0
		[mo .momy] = 0
	else
		[mo .momx] = FixedMul [mo .momx] FRICTION
		[mo .momy] = FixedMul [mo .momy] FRICTION
	endif
end

;
; P_ZMovement
;
fun P_ZMovement (mo ^mobj_t)
	info   = [mo .info]
	player = [mo .player]
	target = [mo .target]

	skullfly = some? (iand [mo .flags] MF_SKULLFLY)

	; check for smooth step up
	if ref? player
		if lt? [mo .z] [mo .floorz]
			delta = isub [mo .floorz] [mo .z]
			[player .viewheight] = isub [player .viewheight] delta
			[player .deltaviewheight] = ishr (isub VIEWHEIGHT [player .viewheight]) 3
		endif
	endif

	; apply momentum
	[mo .z] = iadd [mo .z] [mo .momz]

	; float down towards target if close enough
	if and (some? (iand [mo .flags] MF_FLOAT)) (ref? target)
		if and (not skullfly) (zero? (iand [mo .flags] MF_INFLOAT))
			dx   = isub [mo .x] [target .x]
			dy   = isub [mo .y] [target .y]
			dist = P_ApproxDistance dx dy

			delta  = iadd [target .z] (idivt [mo .height] 2)
			delta  = isub delta [mo .z]
			delta3 = imul delta 3

			if and (neg? delta) (lt? dist (ineg delta3))
				[mo .z] = isub [mo .z] FLOATSPEED
			endif

			if and (pos? delta) (lt? dist delta3)
				[mo .z] = iadd [mo .z] FLOATSPEED
			endif
		endif
	endif

	is_missile = some? (iand [mo .flags] MF_MISSILE)
	is_solid   = zero? (iand [mo .flags] MF_NOCLIP)

	; clip movement

	if le? [mo .z] [mo .floorz]
		; hit the floor

		; Note (id):
		;  somebody left this after the setting momz to 0,
		;  kinda useless there.

		; cph - This was the a bug in the linuxdoom-1.10 source which
		;       caused it not to sync Doom 2 v1.9 demos. Someone
		;       added the above comment and moved up the following code.
		;       So demos would desync in close lost soul fights.
		;
		; Note that this only applies to original Doom 1 or Doom2 demos - not
		; Final Doom and Ultimate Doom.  So we test demo_compatibility *and*
		; gamemission. (Note we assume that Doom1 is always Ult Doom, which
		; seems to hold for most published demos.)

		; fraggle - cph got the logic here slightly wrong.  There are three
		; versions of Doom 1.9:
		;
		;   * The version used in registered doom 1.9 + doom2 - no bounce
		;   * The version used in ultimate doom - has bounce
		;   * The version used in final doom - has bounce
		;
		; So we need to check that this is either retail or commercial
		; (but not doom2)

		correct_bounce = ge? [gameversion] exe_ultimate

		if and skullfly correct_bounce
			; the skull slammed into something
			[mo .momz] = ineg [mo .momz]
		endif

		if neg? [mo .momz]
			hit_hard = lt? [mo .momz] ((- GRAVITY) * 8)

			if and (ref? player) hit_hard
				; Squat down.
				; Decrease viewheight for a moment
				; after hitting the ground (hard),
				; and utter appropriate sound.

				[player .deltaviewheight] = ishr [mo .momz] 3
				S_StartSound (raw-cast mo) sfx_oof
			endif

			[mo .momz] = 0
		endif

		[mo .z] = [mo .floorz]

		if and skullfly (not correct_bounce)
			[mo .momz] = ineg [mo .momz]
		endif

		if and is_missile is_solid
			P_ExplodeMissile mo
			return
		endif

	elif zero? (iand [mo .flags] MF_NOGRAVITY)
		; apply gravity
		if zero? [mo .momz]
			[mo .momz] = ((- GRAVITY) * 2)
		else
			[mo .momz] = isub [mo .momz] GRAVITY
		endif
	endif

	; hit the ceiling?
	top = iadd [mo .z] [mo .height]

	if gt? top [mo .ceilz]
		[mo .z]    = isub [mo .ceilz] [mo .height]
		[mo .momz] = imin [mo .momz] 0

		if skullfly
			; the skull slammed into something
			[mo .momz] = ineg [mo .momz]
		endif

		if and is_missile is_solid
			P_ExplodeMissile mo
			return
		endif
	endif
end

;
; P_NightmareRespawn
;
fun P_CheckNightmareRespawn (old ^mobj_t)
	if not [respawnmonsters]
		return
	endif

	if zero? (iand [old .flags] MF_COUNTKILL)
		return
	endif

	[old .movecount] = iadd [old .movecount] 1

	if lt? [old .movecount] (12 * TICRATE)
		return
	endif

	if some? (iand [leveltime] 31)
		return
	endif

	r = P_Random
	if gt? r 4
		return
	endif

	; respawn it!

	info = [old .info]

	x = conv fixed_t [old .spawnpoint .x]
	y = conv fixed_t [old .spawnpoint .y]

	x = ishl x FRACBITS
	y = ishl y FRACBITS

	z fixed_t = ONFLOORZ

	if some? (iand [info .flags] MF_SPAWNCEILING)
		z = ONCEILINGZ
	endif

	; somthing is occupying its position?
	if not (P_CheckPosition old x y)
		return
	endif

	; spawn a teleport fog at old spot
	sec = [[old .subsector] .sector]
	fog = P_SpawnMobj [old .x] [old .y] [sec .floorh] MT_TFOG

	S_StartSound (raw-cast fog) sfx_telept

	; spawn a teleport fog at the new spot
	ss  = R_PointInSubsector x y
	sec = [ss .sector]
	fog = P_SpawnMobj x y [sec .floorh] MT_TFOG

	S_StartSound (raw-cast fog) sfx_telept

	; spawn the new monster
	mthing = [addr-of old .spawnpoint]

	mo = P_SpawnMobj x y z [old .type]

	; inherit attributes from deceased one
	I_MemCopy [addr-of mo .spawnpoint] mthing mapthing_t.size

	ang = conv angle_t [mthing .angle]
	ang = imul (idivt ang 45) ANG45
	[mo .angle] = ang

	if some? (iand [mthing .options] MTF_AMBUSH)
		[mo .flags] = ior [mo .flags] MF_AMBUSH
	endif

	[mo .reactiontime] = 18

	; remove the old monster
	P_RemoveMobj old
end

#public

fun P_GetMobjInfo (mtype mobjtype_t -> ^mobjinfo_t)
	return [addr-of mobjinfo mtype]
end

fun P_GetState (stnum statenum_t -> ^state_t)
	return [addr-of states stnum]
end

fun P_ResetBodyQue ()
	[bodyqueslot] = 0
end

fun P_AddPlayerCorpse (mo ^mobj_t)
	pos = [bodyqueslot]
	pos = iremt pos BODYQUESIZE

	; start removing objects once buffer is full
	if ge? [bodyqueslot] BODYQUESIZE
		P_RemoveMobj [bodyque pos]
	endif

	[bodyque pos] = mo
	[bodyqueslot] = iadd [bodyqueslot] 1
end

fun P_ResetItemQue ()
	[iquehead] = 0
	[iquetail] = 0
end

fun P_AddRespawnItem (mo ^mobj_t)
	item = [addr-of itemrespawnque [iquehead]]

	I_MemCopy item [addr-of mo .spawnpoint] mapthing_t.size

	[itemrespawntime [iquehead]] = [leveltime]

	[iquehead] = iadd  [iquehead] 1
	[iquehead] = iremt [iquehead] ITEMQUESIZE

	; lose one off the end?  (i.e. the queue is full)
	if eq? [iquehead] [iquetail]
		[iquetail] = iadd  [iquetail] 1
		[iquetail] = iremt [iquetail] ITEMQUESIZE
	endif
end

#private

;
; P_MobjThinker
;
fun P_MobjThinker (mo ^mobj_t)
	has_mom  = or (some? [mo .momx]) (some? [mo .momy])
	skullfly = some? (iand [mo .flags] MF_SKULLFLY)

	; check references
	if ref? [mo .target]
		if P_MobjIsRemoved [mo .target]
			P_SetTarget mo NULL
		endif
	endif

	if ref? [mo .tracer]
		if P_MobjIsRemoved [mo .tracer]
			P_SetTracer mo NULL
		endif
	endif

	; momentum movement
	if or has_mom skullfly
		P_XYMovement mo

		; mobj was removed?
		if pos? [mo .thinker .remove]
			return
		endif
	endif

	has_mom   = some? [mo .momz]
	off_floor = ne? [mo .z] [mo .floorz]

	if or has_mom off_floor
		P_ZMovement mo

		; mobj was removed?
		if pos? [mo .thinker .remove]
			return
		endif
	endif

	if eq? [mo .tics] -1
		P_CheckNightmareRespawn mo
		return
	endif

	; cycle through states,
	; calling action functions at transitions.

	[mo .tics] = isub [mo .tics] 1

	if zero? [mo .tics]
		; this can cycle through multiple states in a tic
		P_SetMobjState mo [[mo .state] .next]
	endif
end

#public

;
; P_SpawnMobj
;
fun P_SpawnMobj (x fixed_t y fixed_t z fixed_t mtype mobjtype_t -> ^mobj_t)
	info   = [addr-of mobjinfo mtype]

	radius = ishl [info .radius] FRACBITS
	height = ishl [info .height] FRACBITS

	mo ^mobj_t = Z_Calloc mobj_t.size PU_LEVEL

	[mo .x] = x
	[mo .y] = y

	[mo .type]   = mtype
	[mo .info]   = info
	[mo .radius] = radius
	[mo .height] = height
	[mo .flags]  = [info .flags]
	[mo .health] = [info .spawnhealth]

	if ne? [gameskill] sk_nightmare
		[mo .reactiontime] = [info .reactiontime]
	endif

	r = P_Random
	[mo .lastlook] = iremt r MAXPLAYERS

	; do not set the state with P_SetMobjState,
	; because action routines can not be called yet.
	P_CopyStateToMobj mo [info .spawnstate]

	; set subsector and/or block links
	P_SetThingPosition mo

	sec = [[mo .subsector] .sector]

	[mo .floorz] = [sec .floorh]
	[mo .ceilz]  = [sec .ceilh]

	if eq? z ONFLOORZ
		[mo .z] = [mo .floorz]
	elif eq? z ONCEILINGZ
		[mo .z] = isub [mo .ceilz] height
	else
		[mo .z] = z
	endif

	[mo .thinker .func] = THINK_mobj

	P_AddThinker (raw-cast mo)

	return mo
end

#private

;
; P_RemoveMobj
;
fun P_RemoveMobj (mo ^mobj_t)
	if some? (iand [mo .flags] MF_SPECIAL)
		dropped = some? (iand [mo .flags] MF_DROPPED)
		mt_inv  = eq? [mo .type] MT_INV
		mt_ins  = eq? [mo .type] MT_INV

		if or dropped mt_inv mt_ins
			; do not respawn dropped items
		else
			P_AddRespawnItem mo
		endif
	endif

	; unlink from sector and block lists
	P_UnsetThingPosition mo

	; stop any playing sound
	S_StopSound (raw-cast mo)

	; clear references
	P_SetTarget mo NULL
	P_SetTracer mo NULL

	; free block (eventually...)
	P_RemoveThinker (raw-cast mo)
end

;
; P_RespawnSpecials
;
fun P_RespawnSpecials ()
	; nothing available to respawn?
	if eq? [iquehead] [iquetail]
		return
	endif

	; wait at least 30 seconds
	delta = isub [leveltime] [itemrespawntime [iquetail]]
	if lt? delta (30 * TICRATE)
		return
	endif

	; pull next object from the queue
	mthing = [addr-of itemrespawnque [iquetail]]

	[iquetail] = iadd  [iquetail] 1
	[iquetail] = iremt [iquetail] ITEMQUESIZE

	x = conv fixed_t [mthing .x]
	y = conv fixed_t [mthing .y]

	x = ishl x FRACBITS
	y = ishl y FRACBITS

	; spawn a teleport fog at the new spot
	ss  = R_PointInSubsector x y
	sec = [ss .sector]
	fog = P_SpawnMobj x y [sec .floorh] MT_IFOG

	S_StartSound (raw-cast fog) sfx_itmbk

	; find which type to spawn
	i s32 = 0
	loop while lt? i NUMMOBJTYPES
		mtype = conv s32 [mthing .type]
		break if eq? [mobjinfo i .doomednum] mtype
		i = iadd i 1
	endloop

	if ge? i NUMMOBJTYPES
		; andrewj: ignore it instead of bombing out
		; I_Error "P_RespawnSpecials: Failed to find mobj type when respawning thing."
		return
	endif

	; spawn it
	z fixed_t = ONFLOORZ

	if some? (iand [mobjinfo i .flags] MF_SPAWNCEILING)
		z = ONCEILINGZ
	endif

	mo = P_SpawnMobj x y z i

	I_MemCopy [addr-of mo .spawnpoint] mthing mapthing_t.size

	ang = conv angle_t [mthing .angle]
	ang = imul (idivt ang 45) ANG45
	[mo .angle] = ang
end

;
; P_SpawnMapThing
; The fields of mapthing should already be in host byte order.
;
fun P_SpawnMapThing (mthing ^mapthing_t)
	mtype = conv s32 [mthing .type]

	if le? mtype 0
		; Thing type 0 is actually "player -1 start".
		; For some reason, Vanilla Doom accepts/ignores this.
		return
	endif

	; save deathmatch start positions
	if eq? mtype 11
		G_AddDeathmatchStart mthing
		return
	endif

	; check for players specially
	if le? mtype 4
		; save spots for respawning in network games
		G_AddPlayerStart (isub mtype 1) mthing

		if zero? [deathmatch]
			G_SpawnPlayer mthing
		endif

		return
	endif

	; check multiplayer bit
	if and (not [netgame]) (some? (iand [mthing .options] 16))
		return
	endif

	; check for apropriate skill level
	bit u16 = 0

	if eq? [gameskill] sk_baby
		bit = 1
	elif eq? [gameskill] sk_nightmare
		bit = 4
	else
		bit = 1
		bit = ishl bit (isub [gameskill] 1)
	endif

	if zero? (iand [mthing .options] bit)
		return
	endif

	; find which type to spawn
	i s32 = 0
	loop while lt? i NUMMOBJTYPES
		break if eq? [mobjinfo i .doomednum] mtype
		i = iadd i 1
	endloop

	if ge? i NUMMOBJTYPES
		; andrewj: ignore it instead of bombing out
		I_Print3 "P_SpawnMapThing: Unknown thing type %d\n" mtype
		return
	endif

	info = [addr-of mobjinfo i]

	; don't spawn keycards and players in deathmatch
	if some? [deathmatch]
		if some? (iand [info .flags] MF_NOTDMATCH)
			return
		endif
	endif

	; don't spawn any monsters if -nomonsters
	if [nomonsters]
		if eq? i MT_SKULL
			return
		endif

		if some? (iand [info .flags] MF_COUNTKILL)
			return
		endif
	endif

	; spawn it
	x = conv fixed_t [mthing .x]
	y = conv fixed_t [mthing .y]

	x = ishl x FRACBITS
	y = ishl y FRACBITS

	z fixed_t = ONFLOORZ

	if some? (iand [info .flags] MF_SPAWNCEILING)
		z = ONCEILINGZ
	endif

	mo = P_SpawnMobj x y z i

	I_MemCopy [addr-of mo .spawnpoint] mthing mapthing_t.size

	if pos? [mo .tics]
		r = P_Random
		r = iremt r [mo .tics]
		[mo .tics] = iadd r 1
	endif

	if some? (iand [mo .flags] MF_COUNTKILL)
		[totalkills] = iadd [totalkills] 1
	endif

	if some? (iand [mo .flags] MF_COUNTITEM)
		[totalitems] = iadd [totalitems] 1
	endif

	ang = conv angle_t [mthing .angle]
	ang = imul (idivt ang 45) ANG45
	[mo .angle] = ang

	if some? (iand [mthing .options] MTF_AMBUSH)
		[mo .flags] = ior [mo .flags] MF_AMBUSH
	endif
end

;;
;;  GAME SPAWN FUNCTIONS
;;

;
; P_SpawnPuff
;
fun P_SpawnPuff (x fixed_t y fixed_t z fixed_t)
	r = P_SubRandom
	r = ishl r 10
	z = iadd z r

	mo = P_SpawnMobj x y z MT_PUFF

	; make puffs float up
	[mo .momz] = FRACUNIT

	r = P_Random
	r = iand r 3

	[mo .tics] = isub [mo .tics] r
	[mo .tics] = imax [mo .tics] 1

	; don't make punches spark on the wall
	if eq? [la .range] MELEERANGE
		P_SetMobjState mo S_PUFF3
	endif
end

;
; P_SpawnBlood
;
fun P_SpawnBlood (x fixed_t y fixed_t z fixed_t damage s32)
	r = P_SubRandom
	r = ishl r 10
	z = iadd z r

	mo = P_SpawnMobj x y z MT_BLOOD

	[mo .momz] = (FRACUNIT * 2)

	r = P_Random
	r = iand r 3

	[mo .tics] = isub [mo .tics] r
	[mo .tics] = imax [mo .tics] 1

	if lt? damage 9
		P_SetMobjState mo S_BLOOD3
	elif lt? damage 12
		P_SetMobjState mo S_BLOOD2
	endif
end

;
; P_CheckMissileSpawn
;
; Moves the missile forward a bit and
; possibly explodes it right there.
;
fun P_CheckMissileSpawn (mo ^mobj_t)
	r = P_Random
	r = iand r 3

	[mo .tics] = isub [mo .tics] r
	[mo .tics] = imax [mo .tics] 1

	dx = ishr [mo .momx] 1
	dy = ishr [mo .momy] 1
	dz = ishr [mo .momz] 1

	[mo .x] = iadd [mo .x] dx
	[mo .y] = iadd [mo .y] dy
	[mo .z] = iadd [mo .z] dz

	moved = P_TryMove mo [mo .x] [mo .y]
	if not moved
		P_ExplodeMissile mo
	endif
end

; Certain functions assume that a mobj_t pointer is non-NULL,
; causing a crash in some situations where it is NULL.  Vanilla
; Doom did not crash because of the lack of proper memory
; protection.  This function substitutes NULL pointers for
; pointers to a dummy mobj, to avoid a crash.

fun P_SubstNullMobj (mo ^mobj_t -> ^mobj_t)
	if null? mo
		mo = null_subst_mobj

		[mo .x] = 0
		[mo .y] = 0
		[mo .z] = 0
		[mo .flags] = 0
	endif

	return mo
end

;
; Reference counting utilities.
; andrewj: added these, prevent deletion of used objects.
;
fun P_IncRef (mo ^mobj_t)
	[mo .ref_count] = iadd [mo .ref_count] 1
end

fun P_DecRef (mo ^mobj_t)
	if zero? [mo .ref_count]
		I_Error "thing with zero reference count"
	endif

	[mo .ref_count] = isub [mo .ref_count] 1
end

fun P_SetTarget (mo ^mobj_t targ ^mobj_t)
	if ref? targ
		P_IncRef targ
	endif

	if ref? [mo .target]
		P_DecRef [mo .target]
	endif

	[mo .target] = targ
end

fun P_SetTracer (mo ^mobj_t tracer ^mobj_t)
	if ref? tracer
		P_IncRef tracer
	endif

	if ref? [mo .tracer]
		P_DecRef [mo .tracer]
	endif

	[mo .tracer] = tracer
end

fun P_SetSoundTarget (sec ^sector_t targ ^mobj_t)
	if ref? targ
		P_IncRef targ
	endif

	if ref? [sec .soundtarget]
		P_DecRef [sec .soundtarget]
	endif

	[sec .soundtarget] = targ
end

fun P_SetAttacker (p ^player_t attacker ^mobj_t)
	if ref? attacker
		P_IncRef attacker
	endif

	if ref? [p .attacker]
		P_DecRef [p .attacker]
	endif

	[p .attacker] = attacker
end

fun P_MobjIsRemoved (mo ^mobj_t -> bool)
	return pos? [mo .thinker .remove]
end

;
; P_SpawnMissile
;
fun P_SpawnMissile (source ^mobj_t dest ^mobj_t mtype mobjtype_t -> ^mobj_t)
	info = [addr-of mobjinfo mtype]

	z = iadd [source .z] (32 * FRACUNIT)

	mo = P_SpawnMobj [source .x] [source .y] z mtype

	if some? [info .seesound]
		S_StartSound (raw-cast mo) [info .seesound]
	endif

	; remember who originated the missile
	P_SetTarget mo source

	ang = R_PointToAngle2 [source .x] [source .y] [dest .x] [dest .y]

	; fuzzy player
	if some? (iand [dest .flags] MF_SHADOW)
		r   = P_SubRandom
		r   = ishl r 20
		ang = iadd ang r
	endif

	[mo .angle] = ang

	ang   = ishr ang ANGLEFINESHIFT
	speed = ishl [info .speed] FRACBITS

	[mo .momx] = FixedMul [finecosine ang] speed
	[mo .momy] = FixedMul [finesine   ang] speed

	dx   = isub [dest .x] [source .x]
	dy   = isub [dest .y] [source .y]
	dz   = isub [dest .z] [source .z]
	dist = P_ApproxDistance dx dy

	dist = idivt dist speed
	dist = imax  dist 1

	[mo .momz] = idivt dz dist

	P_CheckMissileSpawn mo

	return mo
end

;
; P_SpawnPlayerMissile
; Tries to aim at a nearby monster.
;
fun P_SpawnPlayerMissile (source ^mobj_t mtype mobjtype_t)
	; see which target is to be aimed at
	ang = [source .angle]

	slope = P_AimLineAttack source ang (1024 * FRACUNIT)

	; if nothing directly ahead, try to the left or right
	if null? [la .target]
		ang   = iadd ang (1 << 26)
		slope = P_AimLineAttack source ang (1024 * FRACUNIT)
	endif

	if null? [la .target]
		ang   = isub ang (2 << 26)
		slope = P_AimLineAttack source ang (1024 * FRACUNIT)
	endif

	if null? [la .target]
		ang   = [source .angle]
		slope = 0
	endif

	info = [addr-of mobjinfo mtype]

	x = [source .x]
	y = [source .y]
	z = [source .z]
	z = iadd z (32 * FRACUNIT)

	mo = P_SpawnMobj x y z mtype

	if some? [info .seesound]
		S_StartSound (raw-cast mo) [info .seesound]
	endif

	P_SetTarget mo source

	[mo .angle] = ang

	ang   = ishr ang ANGLEFINESHIFT
	speed = ishl [info .speed] FRACBITS

	[mo .momx] = FixedMul [finecosine ang] speed
	[mo .momy] = FixedMul [finesine   ang] speed
	[mo .momz] = FixedMul slope            speed

	P_CheckMissileSpawn mo
end
