;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 1993-2008 Raven Software
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;


;;
;; DEFAULTS
;;

#public

; Location where all configuration data is stored.
; default.cfg, savegames, etc.
zero-var configdir ^uchar

#private

zero-var defaults_filename ^uchar

type default_type_t s32

const DEFTYPE_INT     = 0
const DEFTYPE_STRING  = 1
const DEFTYPE_KEY     = 2

type default_locptr_u union
	.int  ^s32
	.str  ^^uchar
end

type default_t struct
	; Name of the variable
	.name ^uchar

	; Location in memory of the variable
	.loc default_locptr_u

	; Type of the variable
	.type default_type_t

	; If this is a KEY value, the original integer scancode we read from
	; the config file before translating it to the internal key value.
	; If zero, we didn't read this value from a config file.
	.untranslated s32

	; The value we translated the scancode into when we read the
	; config file on startup.  If the variable value is different from
	; this, it has been changed and needs to be converted; otherwise,
	; use the 'untranslated' value.
	.translated_key s32

	; If true, this config variable has been bound to a variable
	; and is being used.
	.bound bool
end

const NUM_DEFAULTS = 106

var all_defaults [NUM_DEFAULTS]default_t =
	;
	; Mouse sensitivity.  This value is used to multiply input mouse
	; movement to control the effect of moving the mouse.
	;
	; The "normal" maximum value available for this through the
	; in-game options menu is 9. A value of 31 or greater will cause
	; the game to crash when entering the options menu.
	;
	{ "mouse_sensitivity" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Volume of sound effects, range 0-15.
	;
	{ "sfx_volume" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Volume of in-game music, range 0-15.
	;
	{ "music_volume" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, messages are displayed on the heads-up display
	; in the game ("picked up a clip", etc).  If zero, these messages
	; are not displayed.
	;
	{ "show_messages" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Keyboard key to turn right.
	;
	{ "key_right" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to turn left.
	;
	{ "key_left" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to move forward.
	;
	{ "key_up" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to move backward.
	;
	{ "key_down" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to strafe left.
	;
	{ "key_strafeleft" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to strafe right.
	;
	{ "key_straferight" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to fire the currently selected weapon.
	;
	{ "key_fire" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to "use" an object, eg. a door or switch.
	;
	{ "key_use" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to turn on strafing.  When held down, pressing the
	; key to turn left or right causes the player to strafe left or
	; right instead.
	;
	{ "key_strafe" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard key to make the player run.
	;
	{ "key_speed" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; If non-zero, mouse input is enabled.  If zero, mouse input is
	; disabled.
	;
	{ "use_mouse" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to fire the currently selected weapon.
	;

	{ "mouseb_fire" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to turn on strafing.  When held down, the player
	; will strafe left and right instead of turning left and right.
	;
	{ "mouseb_strafe" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to move forward.
	;
	{ "mouseb_forward" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Screen size, range 3-11.
	;
	; A value of 11 gives a full-screen view with the status bar not
	; displayed.  A value of 10 gives a full-screen view with the
	; status bar displayed.
	;
	{ "screenblocks" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Screen detail.  Zero gives normal "high detail" mode, while
	; a non-zero value gives "low detail" mode.
	;
	{ "detaillevel" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Number of sounds that will be played simultaneously.
	;
	{ "snd_channels" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Music output device.  A non-zero value gives MIDI sound output,
	; while a value of zero disables music.
	;
	{ "snd_musicdevice" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Sound effects device.  A value of zero disables in-game sound
	; effects, a value of 1 enables PC speaker sound effects, while
	; a value in the range 2-9 enables the "normal" digital sound
	; effects.
	;
	{ "snd_sfxdevice" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Gamma correction level.  A value of zero disables gamma
	; correction, while a value in the range 1-4 gives increasing
	; levels of gamma correction.
	;
	{ "usegamma" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Name of the SDL video driver to use.  If this is an empty string,
	; the default video driver is used.
	;
	{ "video_driver" { .str NULL } DEFTYPE_STRING 0 0 FALSE }

	;
	; Position of the window on the screen when running in windowed
	; mode. Accepted values are: "" (empty string) - don't care,
	; "center" - place window at center of screen, "x,y" - place
	; window at the specified coordinates.
	;
	{ "window_position" { .str NULL } DEFTYPE_STRING 0 0 FALSE }

	;
	; If non-zero, the game will run in full screen mode.  If zero,
	; the game will run in a window.
	;
	{ "fullscreen" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Index of the display on which the game should run. This has no
	; effect if running in windowed mode (fullscreen=0) and
	; window_position is not set to "center".
	;
	{ "video_display" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, the screen will be stretched vertically to display
	; correctly on a square pixel video mode.
	;
	{ "aspect_ratio_correct" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, forces integer scales for resolution-independent rendering.
	;
	{ "integer_scaling" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	; If non-zero, any pillar/letter boxes drawn around the game area
	; will "flash" when the game palette changes, simulating the VGA
	; "porch"
	{ "vga_porch_flash" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Window width when running in windowed mode.
	;
	{ "window_width" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Window height when running in windowed mode.
	;
	{ "window_height" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Width for screen mode when running fullscreen.
	; If this and fullscreen_height are both set to zero, we run
	; fullscreen as a desktop window that covers the entire screen,
	; rather than ever switching screen modes. It should usually
	; be unnecessary to set this value.
	;
	{ "fullscreen_width" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Height for screen mode when running fullscreen.
	; See documentation for fullscreen_width.
	;
	{ "fullscreen_height" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, force the use of a software renderer. For use on
	; systems lacking hardware acceleration.
	;
	{ "force_software_renderer" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Maximum number of pixels to use for intermediate scaling buffer.
	; More pixels mean that the screen can be rendered more precisely,
	; but there are diminishing returns on quality. The default limits to
	; 16,000,000 pixels, which is enough to cover 4K monitor standards.
	;
	{ "max_scaling_buffer_pixels" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Number of milliseconds to wait on startup after the video mode
	; has been set, before the game will start.  This allows the
	; screen to settle on some monitors that do not display an image
	; for a brief interval after changing video modes.
	;
	{ "startup_delay" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, display the graphical startup screen.
	; Heretic, Hexen and Strife only.
	;
	;
	{ "graphical_startup" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, the ENDOOM text screen is displayed when exiting the
	; game. If zero, the ENDOOM screen is not displayed.
	;
	{ "show_endoom" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Sound output sample rate, in Hz.  Typical values to use are
	; 11025, 22050, 44100 and 48000.
	;
	{ "snd_samplerate" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Maximum number of bytes to allocate for caching converted sound
	; effects in memory. If set to zero, there is no limit applied.
	;
	{ "snd_cachesize" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Maximum size of the output sound buffer size in milliseconds.
	; Sound output is generated periodically in slices. Higher values
	; might be more efficient but will introduce latency to the
	; sound output. The default is 28ms (one slice per tic with the
	; 35fps timer).
	;
	{ "snd_maxslicetime_ms" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, the Vanilla demo size limit is enforced; the game
	; exits with an error when a demo exceeds the demo size limit
	; (128KiB by default).  If this has a value of zero, there is no
	; limit to the size of demos.
	;
	{ "vanilla_demo_limit" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, the game behaves like Vanilla Doom, always assuming
	; an American keyboard mapping.  If this has a value of zero, the
	; native keyboard mapping of the keyboard is used.
	;
	{ "vanilla_keyboard_mapping" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Name to use in network games for identification.  This is only
	; used on the "waiting" screen while waiting for the game to start.
	;
	{ "player_name" { .str NULL } DEFTYPE_STRING 0 0 FALSE }

	;
	; If this is non-zero, the mouse will be "grabbed" when running
	; in windowed mode so that it can be used as an input device.
	; When running full screen, this has no effect.
	;
	{ "grabmouse" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, all vertical mouse movement is ignored.  This
	; emulates the behavior of the "novert" tool available under DOS
	; that performs the same function.
	;
	{ "novert" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse acceleration factor.  When the speed of mouse movement
	; exceeds the threshold value (mouse_threshold), the speed is
	; multiplied by this value and divided by 10.
	;
	{ "mouse_accel_new" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse acceleration threshold.  When the speed of mouse movement
	; exceeds this threshold value, the speed is multiplied by an
	; acceleration factor (mouse_accel_new / 10).
	;
	{ "mouse_threshold" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to strafe left.
	;
	{ "mouseb_strafeleft" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to strafe right.
	;
	{ "mouseb_straferight" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to "use" an object, eg. a door or switch.
	;
	{ "mouseb_use" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to move backwards.
	;
	{ "mouseb_backward" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to cycle to the previous weapon.
	;
	{ "mouseb_prevweapon" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Mouse button to cycle to the next weapon.
	;
	{ "mouseb_nextweapon" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; If non-zero, double-clicking a mouse button acts like pressing
	; the "use" key to use an object in-game, eg. a door or switch.
	;
	{ "dclick_use" { .int NULL } DEFTYPE_INT 0 0 FALSE }

	;
	; Key to pause or unpause the game.
	;
	{ "key_pause" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key that activates the menu when pressed.
	;
	{ "key_menu_activate" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key that moves the cursor up on the menu.
	;
	{ "key_menu_up" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key that moves the cursor down on the menu.
	;
	{ "key_menu_down" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key that moves the currently selected slider on the menu left.
	;
	{ "key_menu_left" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key that moves the currently selected slider on the menu right.
	;
	{ "key_menu_right" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to go back to the previous menu.
	;
	{ "key_menu_back" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to activate the currently selected menu item.
	;
	{ "key_menu_forward" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to answer 'yes' to a question in the menu.
	;
	{ "key_menu_confirm" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to answer 'no' to a question in the menu.
	;
	{ "key_menu_abort" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to bring up the help screen.
	;
	{ "key_menu_help" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to bring up the save game menu.
	;
	{ "key_menu_save" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to bring up the load game menu.
	;
	{ "key_menu_load" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to bring up the sound volume menu.
	;
	{ "key_menu_volume" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to toggle the detail level.
	;
	{ "key_menu_detail" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to quicksave the current game.
	;
	{ "key_menu_qsave" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to end the game.
	;
	{ "key_menu_endgame" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to toggle heads-up messages.
	;
	{ "key_menu_messages" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to load the last quicksave.
	;
	{ "key_menu_qload" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to quit the game.
	;
	{ "key_menu_quit" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to toggle the gamma correction level.
	;
	{ "key_menu_gamma" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to switch view in multiplayer.
	;
	{ "key_spy" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to increase the screen size.
	;
	{ "key_menu_incscreen" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to decrease the screen size.
	;
	{ "key_menu_decscreen" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Keyboard shortcut to save a screenshot.
	;
	{ "key_menu_screenshot" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to toggle the map view.
	;
	{ "key_map_toggle" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to pan north when in the map view.
	;
	{ "key_map_north" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to pan south when in the map view.
	;
	{ "key_map_south" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to pan east when in the map view.
	;
	{ "key_map_east" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to pan west when in the map view.
	;
	{ "key_map_west" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to zoom in when in the map view.
	;
	{ "key_map_zoomin" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to zoom out when in the map view.
	;
	{ "key_map_zoomout" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to zoom out the maximum amount when in the map view.
	;
	{ "key_map_maxzoom" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to toggle follow mode when in the map view.
	;
	{ "key_map_follow" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to toggle the grid display when in the map view.
	;
	{ "key_map_grid" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to set a mark when in the map view.
	;
	{ "key_map_mark" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to clear all marks when in the map view.
	;
	{ "key_map_clearmark" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 1.
	;
	{ "key_weapon1" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 2.
	;
	{ "key_weapon2" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 3.
	;
	{ "key_weapon3" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 4.
	;
	{ "key_weapon4" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 5.
	;
	{ "key_weapon5" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 6.
	;
	{ "key_weapon6" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 7.
	;
	{ "key_weapon7" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to select weapon 8.
	;
	{ "key_weapon8" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to cycle to the previous weapon.
	;
	{ "key_prevweapon" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to cycle to the next weapon.
	;
	{ "key_nextweapon" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to re-display last message.
	;
	{ "key_message_refresh" { .int NULL } DEFTYPE_KEY 0 0 FALSE }

	;
	; Key to quit the game when recording a demo.
	;
	{ "key_demo_quit" { .int NULL } DEFTYPE_KEY 0 0 FALSE }
end

; Search for a variable

fun SearchCollection(name ^uchar -> ^default_t)
	i s32 = 0
	loop while lt? i NUM_DEFAULTS
		cmp = M_StrCmp name [all_defaults i .name]
		if zero? cmp
			return [addr-of all_defaults i]
		endif

		i = iadd i 1
	endloop

	return NULL
end

; Get a configuration file variable by its name

fun GetDefaultForName (name ^uchar -> ^default_t)
	result = SearchCollection name

	if null? result
		I_Error2 "Unknown configuration variable: '%s'" name
	endif

	return result
end

;
; Mapping from DOS ("XT") keyboard scan code to internal key code.
; These scancodes must be used for compatibility with Vanilla.
;
; Some notes:
;  * KEY_PAUSE is wrong - it's in the KEY_NUMLOCK spot. This shouldn't
;    matter in terms of Vanilla compatibility because neither of
;    those were valid for key bindings.
;  * There is no proper scan code for PrintScreen (on DOS machines it
;    sends an interrupt). So I added a fake scan code of 126 for it.
;    The presence of this is important so we can bind PrintScreen as
;    a screenshot key.
;
rom-var scantokey [128]s32 =
	0              27             '1'            '2'            ; 0x00 - 0x0F
	'3'            '4'            '5'            '6'
	'7'            '8'            '9'            '0'
	'-'            '='            KEY_BACKSPACE  9
	'q'            'w'            'e'            'r'            ; 0x10 - 0x1F
	't'            'y'            'u'            'i'
	'o'            'p'            '['            ']'
	13              KEY_CTRL      'a'            's'
	'd'            'f'            'g'            'h'            ; 0x20 - 0x2F
	'j'            'k'            'l'            ';'
	'\''           '`'            KEY_SHIFT      '\\'
	'z'            'x'            'c'            'v'
	'b'            'n'            'm'            ','            ; 0x30 - 0x3F
	'.'            '/'            KEY_SHIFT      KEYP_MULTIPLY
	KEY_ALT        ' '            KEY_CAPSLOCK   KEY_F1
	KEY_F2         KEY_F3         KEY_F4         KEY_F5
	KEY_F6         KEY_F7         KEY_F8         KEY_F9         ; 0x40 - 0x4F
	KEY_F10        KEY_PAUSE      KEY_SCRLCK     KEY_HOME
	KEY_UPARROW    KEY_PGUP       KEY_MINUS      KEY_LEFTARROW
	KEYP_5         KEY_RIGHTARROW KEYP_PLUS      KEY_END
	KEY_DOWNARROW  KEY_PGDN       KEY_INS        KEY_DEL        ; 0x50 - 0x5F
	0              0              0              KEY_F11
	KEY_F12        0              0              0
	0              0              0              0
	0              0              0              0              ; 0x60 - 0x6F
	0              0              0              0
	0              0              0              0
	0              0              0              0
	0              0              0              0              ; 0x70 - 0x7F
	0              0              0              0
	0              0              0              0
	0              0              KEY_PRTSCR     0
end

fun TranslateScan (scan s32 -> s32)
	if neg? scan
		return 0
	endif

	if ge? scan 128
		return 0
	endif

	return [scantokey scan]
end

fun FindScanForKey (key s32 -> s32)
	s s32 = 0
	loop while lt? s 128
		if eq? [scantokey s] key
			return s
		endif
		s = iadd s 1
	endloop

	return key
end

rom-var spaces [32]uchar = { "                                " }

fun SaveDefaultCollection ()
	f = I_Fopen [defaults_filename] "w"

	if null? f
		; can't write the file, but don't complain.
		; [ especially since this is called after an I_Error... ]
		return
	endif

	i s32 = 0
	loop while lt? i NUM_DEFAULTS
		def = [addr-of all_defaults i]

		; Ignore unbound variables
		if [def .bound]
			; Print the name and line up all values at 32 characters
			name = [def .name]
			len  = M_StrLen name

			I_Fwrite name len f

			len = isub 32 len
			len = imax len 1

			I_Fwrite spaces len f

			; Print the value
			WriteDefaultValue def f

			I_Fputc '\r' f
			I_Fputc '\n' f
		endif

		i = iadd i 1
	endloop

	I_Fclose f
end

fun WriteDefaultValue (def ^default_t f ^FILE)
	if eq? [def .type] DEFTYPE_STRING
		strptr = [def .loc .str]
		str    = [strptr]

		I_Fputc '"' f
		I_Fwrite str (M_StrLen str) f
		I_Fputc '"' f

		return
	endif

	intptr = [def .loc .int]
	val    = [intptr]

	if eq? [def .type] DEFTYPE_KEY
		; use the untranslated version if we can, to reduce
		; the possibility of screwing up the user's config file.

		if eq? val KEY_SHIFT
			; special case: for shift, force scan code for RIGHT shift,
			; this is what Vanilla requires.
			val = 54
		elif and (eq? val [def .translated_key]) (pos? [def .untranslated])
			; has not been changed since the last time we read the config file.
			val = [def .untranslated]
		else
			; search for a reverse mapping back to a scancode.
			val = FindScanForKey val
		endif
	endif

	numbuf = stack-var [64]uchar

	M_FormatNum numbuf 64 "%d" val

	I_Fwrite numbuf (M_StrLen numbuf) f
end

fun DoSetVariable (def ^default_t value ^uchar)
	dtype = [def .type]

	if eq? dtype DEFTYPE_STRING
		strptr   = [def .loc .str]
		[strptr] = M_StringDuplicate value

	elif eq? dtype DEFTYPE_INT
		intptr   = [def .loc .int]
		[intptr] = M_Atoi value

	elif eq? dtype DEFTYPE_KEY
		intptr = [def .loc .int]

		; translate scancodes read from config file
		; (save the old value in untranslated).
		intparm = M_Atoi value
		[def .untranslated] = intparm

		intparm = TranslateScan intparm

		[def .translated_key] = intparm
		[intptr]              = intparm
	endif
end

fun LoadDefaultCollection ()
	; read the file in, overriding any set defaults
	f = I_Fopen [defaults_filename] "r"

	if null? f
		; file not opened, but don't complain.
		; it's probably just the first time they ran the game.
		return
	endif

	loop until I_Feof f
		defname = stack-var [100]uchar
		parm    = stack-var [100]uchar

		if not (ParseDefaultLine f defname parm)
			; this line doesn't match
			jump continue
		endif

		; surrounded by quotes?  If so, remove them.
		parm = StripQuotes parm

		; find the setting in the list
		def = SearchCollection defname

		; unknown variable?  Unbound variables are also treated as unknown.
		jump continue if null? def
		jump continue unless [def .bound]

		DoSetVariable def parm

		continue:
	endloop

	I_Fclose f
end

fun ParseDefaultLine (f ^FILE defname ^[100]uchar parm ^[100]uchar -> bool)
	; The three stooges:
	;   0 = parsing defname
	;   1 = whitespace in between
	;   2 = parsing parm
	stage s32 = 0

	d s32 = 0  ; defname length
	p s32 = 0  ; parm length

	buf = stack-var uchar

	loop
		break if I_Feof f

		got = I_Fread buf 1 f
		break unless eq? got 1

		ch = [buf]
		break if eq? ch '\n'

		if eq? ch '\t'
			ch = ' '
		endif

		; strip out non-printable characters (\r characters in DOS text files).
		jump continue if lt? ch ' '

		; prevent buffer overflow
		jump continue if ge? d (100 - 2)
		jump continue if ge? p (100 - 2)

		; collect all chars for the parameter, even whitespace
		if eq? stage 2
			[parm p] = ch
			p = iadd p 1
			jump continue
		endif

		if eq? ch ' '
			if eq? stage 0
				stage = 1
			endif

			jump continue
		endif

		if eq? stage 0
			[defname d] = ch
			d = iadd d 1
		else
			; found beginning of the parameter
			stage = 2
			[parm p] = ch
			p = 1
		endif

		continue:
	endloop

	; require at least one character in parm buffer
	if and (pos? d) (pos? p)
		[defname d] = 0
		[parm    p] = 0

		if eq? stage 2
			return TRUE
		endif
	endif

	return FALSE
end

fun StripQuotes (parm ^[0]uchar -> ^[0]uchar)
	len = M_StrLen parm
	len = isub len 1

	if lt? len 1
		return parm
	endif

	if ne? [parm 0] '"'
		return parm
	endif

	if ne? [parm len] '"'
		return parm
	endif

	[parm len] = 0
	parm = padd parm 1
	return parm
end

; Set the value of a particular variable; an API function for other
; parts of the program to assign values to config variables by name.

fun M_SetVariable (name ^uchar value ^uchar -> bool)
	def = GetDefaultForName name

	if null? def
		return FALSE
	endif

	if not [def .bound]
		return FALSE
	endif

	DoSetVariable def value
	return TRUE
end

; Get the value of a variable.

fun M_GetIntVariable (name ^uchar -> s32)
	def = GetDefaultForName name

	if null? def
		return 0
	endif

	if not [def .bound]
		return 0
	endif

	if eq? [def .type] DEFTYPE_STRING
		return 0
	endif

	intptr ^s32 = [def .loc .int]
	return [intptr]
end

fun M_GetStringVariable (name ^uchar -> ^uchar)
	def = GetDefaultForName name

	if null? def
		return ""
	endif

	if not [def .bound]
		return ""
	endif

	if ne? [def .type] DEFTYPE_STRING
		return ""
	endif

	strptr ^^uchar = [def .loc .str]
	return [strptr]
end

#public

;
; M_SaveDefaults
;
fun M_SaveDefaults ()
	SaveDefaultCollection
end

;
; M_LoadDefaults
;
fun M_LoadDefaults ()
	;
	; load main configuration from the specified file, instead of the
	; default.
	;
	p = M_CheckParmWithArgs "-config" 1

	if pos? p
		[defaults_filename] = M_GetArg (iadd p 1)
	else
		[defaults_filename] = M_StringJoin [configdir] "default.cfg"
	endif

	I_Print2 "saving config in: %s\n" [defaults_filename]

	LoadDefaultCollection
end

;
; Bind a variable to a given configuration file variable, by name.
;
fun M_BindIntVariable (name ^uchar location ^s32)
	def = GetDefaultForName name

	if eq? [def .type] DEFTYPE_STRING
		I_Error "M_BindIntVariable: variable is a string"
	endif

	[def .loc .int] = location
	[def .bound]    = TRUE
end

fun M_BindStringVariable (name ^uchar location ^^uchar)
	def = GetDefaultForName name

	if ne? [def .type] DEFTYPE_STRING
		I_Error "M_BindStringVariable: variable is not a string"
	endif

	[def .loc .str] = location
	[def .bound]    = TRUE
end

;
; SetConfigDir
;
; Sets the location of the configuration directory, where configuration
; files are stored.  Accepts NULL to set the default directory.  Also
; creates the directory if it doesn't exist already.
;
fun M_SetConfigDir (dir ^uchar)
	if null? dir
		dir = "./carobbeans/"
	endif

	[configdir] = dir

	I_Print2 "Using %s for configuration and saves\n" dir

	I_MakeDirectory dir
end
