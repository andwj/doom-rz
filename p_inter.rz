;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

extern-var netgame bool
extern-var deathmatch s32
extern-var consoleplayer s32

extern-fun AM_Stop ()

#private

const BONUSADD = 6

const MAXHEALTH = 100

const BASETHRESHOLD = 100

;
; Power up durations,
;
; how many seconds till expiration,
; assuming TICRATE is 35 ticks/second.
;
const INVULN_TICS = (30  * TICRATE)
const INVIS_TICS  = (60  * TICRATE)
const INFRA_TICS  = (120 * TICRATE)
const IRON_TICS   = (60  * TICRATE)

#public

; a weapon is found with two clip loads,
; a big item has five clip loads.
rom-var maxammo  [NUMAMMO]s32 = { 200 50 300 50 }
rom-var clipammo [NUMAMMO]s32 = {  10  4  20  1 }

#private

;;
;; GET STUFF
;;

;
; P_GiveAmmo
;
; Num is the number of clip loads,
; not the individual count (0 means 1/2 clip).
; Returns false if the ammo can't be picked up at all
;
fun P_GiveAmmo (p ^player_t ammo ammotype_t num s32 -> bool)
	if gt? ammo NUMAMMO
		I_Error "P_GiveAmmo: bad type"
	endif

	if eq? ammo am_noammo
		return FALSE
	endif

	if ge? [p .ammo ammo] [p .maxammo ammo]
		return FALSE
	endif

	if zero? num
		num = idivt [clipammo ammo] 2
	else
		num = imul num [clipammo ammo]
	endif

	; give double ammo in trainer mode,
	; you'll need it in nightmare too.
	if eq? [gameskill] sk_baby sk_nightmare
		num = ishl num 1
	endif

	oldammo = [p .ammo ammo]

	[p .ammo ammo] = iadd [p .ammo ammo] num
	[p .ammo ammo] = imin [p .ammo ammo] [p .maxammo ammo]

	; If non zero ammo,
	; don't change up weapons,
	; player was lower on purpose.

	if some? oldammo
		return TRUE
	endif

	; We were down to zero,
	; so select a new weapon.
	; Preferences are not user selectable.
	ready = [p .ready]

	if eq? ammo am_clip
		if eq? ready wp_fist
			if pos? [p .owned wp_chaingun]
				[p .pending] = wp_chaingun
			else
				[p .pending] = wp_pistol
			endif
		endif

	elif eq? ammo am_shell
		if eq? ready wp_fist wp_pistol
			if pos? [p .owned wp_shotgun]
				[p .pending] = wp_shotgun
			endif
		endif

	elif eq? ammo am_cell
		if eq? ready wp_fist wp_pistol
			if pos? [p .owned wp_plasma]
				[p .pending] = wp_plasma
			endif
		endif

	elif eq? ammo am_misl
		if eq? ready wp_fist
			if pos? [p .owned wp_missile]
				[p .pending] = wp_missile
			endif
		endif
	endif

	return TRUE
end

fun P_GiveBackpack (p ^player_t)
	i s32 = 0

	loop while lt? i NUMAMMO
		if not [p .backpack]
			[p .maxammo i] = imul [p .maxammo i] 2
		endif

		P_GiveAmmo p i 1
		i = iadd i 1
	endloop

	[p .backpack] = TRUE
end

;
; P_GiveWeapon
;
fun P_GiveWeapon (p ^player_t weapon weapontype_t dropped bool -> bool)
	gaveammo   bool = FALSE
	gaveweapon bool = FALSE

	ammo = [weaponinfo weapon .ammo]

	if and [netgame] (ne? [deathmatch] 2) (not dropped)
		; leave placed weapons forever on net games.
		; need to emulate behavior of returning TRUE, but return FALSE.

		if pos? [p .owned weapon]
			return FALSE
		endif

		[p .bonuscount] = iadd [p .bonuscount] BONUSADD
		[p .owned weapon] = 1
		[p .pending] = weapon

		ammo_qty s32 = sel-if (some? [deathmatch]) 5 2
		P_GiveAmmo p ammo ammo_qty

		S_StartHudSound (raw-cast [p .mo]) sfx_wpnup
		return FALSE
	endif

	if ne? ammo am_noammo
		; give one clip with a dropped weapon,
		; two clips with a found weapon.
		clips s32 = sel-if dropped 1 2
		gaveammo = P_GiveAmmo p ammo clips
	endif

	if zero? [p .owned weapon]
		[p .owned weapon] = 1
		[p .pending] = weapon
		gaveweapon = TRUE
	endif

	return or gaveweapon gaveammo
end

;
; P_GiveHealth
; Returns false if the health isn't needed at all.
;
fun P_GiveHealth (p ^player_t num s32 -> bool)
	if ge? [p .health] MAXHEALTH
		return FALSE
	endif

	[p .health] = iadd [p .health] num
	[p .health] = imin [p .health] MAXHEALTH

	; keep player and mobj in sync
	[[p .mo] .health] = [p .health]

	return TRUE
end

fun P_GiveHealthBonus (p ^player_t)
	; this can go over 100%
	[p .health] = iadd [p .health] 1
	[p .health] = imin [p .health] deh_max_health

	[[p .mo] .health] = [p .health]
end

;
; P_GiveArmor
; Returns false if the armor is worse than the current armor.
;
fun P_GiveArmor (p ^player_t armortype s32 -> bool)
	hits = imul armortype 100

	if ge? [p .armorpoints] hits
		return FALSE
	endif

	[p .armortype]   = armortype
	[p .armorpoints] = hits

	return TRUE
end

fun P_GiveArmorBonus (p ^player_t)
	; this can go over 100%
	[p .armorpoints] = iadd [p .armorpoints] 1
	[p .armorpoints] = imin [p .armorpoints] deh_max_armor

	; deh_green_armor_class only applies to the green armor shirt.
	; for the armor helmets, armortype 1 is always used.
	if zero? [p .armortype]
		[p .armortype] = 1
	endif
end

fun P_GiveSoulSphere (p ^player_t)
	[p .health] = iadd [p .health] deh_soulsphere_health
	[p .health] = imin [p .health] deh_max_soulsphere

	[[p .mo] .health] = [p .health]
end

fun P_GiveMegaSphere (p ^player_t)
	mo = [p .mo]

	[p  .health] = deh_megasphere_health
	[mo .health] = deh_megasphere_health

	P_GiveArmor p 2
end

;
; P_GiveCard
;
fun P_GiveCard (p ^player_t card card_t msg msgnum_t)
	if [p .cards card]
		return
	endif

	[p .cards card] = TRUE
	[p .bonuscount] = BONUSADD

	P_PlayerMessage p msg
end

#public

;
; P_GivePower
;
fun P_GivePower (p ^player_t power powertype_t -> bool)
	if eq? power pw_invulnerability
		[p .powers power] = INVULN_TICS
		return TRUE
	endif

	if eq? power pw_invisibility
		mo = [p .mo]
		[mo .flags] = ior [mo .flags] MF_SHADOW
		[p .powers power] = INVIS_TICS
		return TRUE
	endif

	if eq? power pw_infrared
		[p .powers power] = INFRA_TICS
		return TRUE
	endif

	if eq? power pw_ironfeet
		[p .powers power] = IRON_TICS
		return TRUE
	endif

	if eq? power pw_strength
		P_GiveHealth p 100
		[p .powers power] = 1
		return TRUE
	endif

	if pos? [p .powers power]
		return FALSE  ; already got it
	endif

	[p .powers power] = 1
	return TRUE
end

#private

;
; P_TouchSpecialThing
;
fun P_TouchSpecialThing (special ^ mobj_t toucher ^mobj_t)
	p = [toucher .player]

	delta = isub [special .z] [toucher .z]
	too_high = gt? delta [toucher .height]
	too_low  = lt? delta (-8 * FRACUNIT)

	if or too_high too_low
		return   ; out of reach
	endif

	; Dead thing touching.
	; Can happen with a sliding player corpse.
	if le? [toucher .health] 0
		return
	endif

	sound sfxenum_t = sfx_itemup

	sprite  = [special .sprite]
	dropped = some? (iand [special .flags] MF_DROPPED)

	; Identify by sprite.

	if eq? sprite SPR_ARM1
		jump no_give unless P_GiveArmor p 1
		P_PlayerMessage p GOTARMOR

	elif eq? sprite SPR_ARM2
		jump no_give unless P_GiveArmor p 2
		P_PlayerMessage p GOTMEGA

	elif eq? sprite SPR_BON1
		P_GiveHealthBonus p
		P_PlayerMessage p GOTHTHBONUS

	elif eq? sprite SPR_BON2
		P_GiveArmorBonus p
		P_PlayerMessage p GOTARMBONUS

	elif eq? sprite SPR_SOUL
		P_GiveSoulSphere p
		P_PlayerMessage p GOTSUPER
		sound = sfx_getpow

	elif eq? sprite SPR_MEGA
		jump no_give unless eq? [gamemode] commercial
		P_GiveMegaSphere p
		P_PlayerMessage p GOTMSPHERE
		sound = sfx_getpow

	elif eq? sprite SPR_BKEY
		P_GiveCard p it_bluecard GOTBLUECARD
		if [netgame]
			; leave cards for everyone
			return
		endif

	elif eq? sprite SPR_YKEY
		P_GiveCard p it_yellowcard GOTYELWCARD
		if [netgame]
			; leave cards for everyone
			return
		endif

	elif eq? sprite SPR_RKEY
		P_GiveCard p it_redcard GOTREDCARD
		if [netgame]
			; leave cards for everyone
			return
		endif

	elif eq? sprite SPR_BSKU
		P_GiveCard p it_blueskull GOTBLUESKUL
		if [netgame]
			; leave cards for everyone
			return
		endif

	elif eq? sprite SPR_YSKU
		P_GiveCard p it_yellowskull GOTYELWSKUL
		if [netgame]
			; leave cards for everyone
			return
		endif

	elif eq? sprite SPR_RSKU
		P_GiveCard p it_redskull GOTREDSKULL
		if [netgame]
			; leave cards for everyone
			return
		endif

	elif eq? sprite SPR_STIM
		jump no_give unless P_GiveHealth p 10
		P_PlayerMessage p GOTSTIM

	elif eq? sprite SPR_MEDI
		jump no_give unless P_GiveHealth p 25
		msg msgnum_t = sel-if (lt? [p .health] 35) GOTMEDINEED GOTMEDIKIT
		P_PlayerMessage p msg

	elif eq? sprite SPR_PINV
		jump no_give unless P_GivePower p pw_invulnerability
		P_PlayerMessage p GOTINVUL
		sound = sfx_getpow

	elif eq? sprite SPR_PSTR
		jump no_give unless P_GivePower p pw_strength
		P_PlayerMessage p GOTBERSERK
		if ne? [p .ready] wp_fist
			[p .pending] = wp_fist
		endif
		sound = sfx_getpow

	elif eq? sprite SPR_PINS
		jump no_give unless P_GivePower p pw_invisibility
		P_PlayerMessage p GOTINVIS
		sound = sfx_getpow

	elif eq? sprite SPR_SUIT
		jump no_give unless P_GivePower p pw_ironfeet
		P_PlayerMessage p GOTSUIT
		sound = sfx_getpow

	elif eq? sprite SPR_PMAP
		jump no_give unless P_GivePower p pw_allmap
		P_PlayerMessage p GOTMAP
		sound = sfx_getpow

	elif eq? sprite SPR_PVIS
		jump no_give unless P_GivePower p pw_infrared
		P_PlayerMessage p GOTVISOR
		sound = sfx_getpow

	elif eq? sprite SPR_CLIP
		qty s32 = sel-if dropped 0 1
		jump no_give unless P_GiveAmmo p am_clip qty
		P_PlayerMessage p GOTCLIP

	elif eq? sprite SPR_AMMO
		jump no_give unless P_GiveAmmo p am_clip 5
		P_PlayerMessage p GOTCLIPBOX

	elif eq? sprite SPR_ROCK
		jump no_give unless P_GiveAmmo p am_misl 1
		P_PlayerMessage p GOTROCKET

	elif eq? sprite SPR_BROK
		jump no_give unless P_GiveAmmo p am_misl 5
		P_PlayerMessage p GOTROCKBOX

	elif eq? sprite SPR_CELL
		jump no_give unless P_GiveAmmo p am_cell 1
		P_PlayerMessage p GOTCELL

	elif eq? sprite SPR_CELP
		jump no_give unless P_GiveAmmo p am_cell 5
		P_PlayerMessage p GOTCELLBOX

	elif eq? sprite SPR_SHEL
		jump no_give unless P_GiveAmmo p am_shell 1
		P_PlayerMessage p GOTSHELLS

	elif eq? sprite SPR_SBOX
		jump no_give unless P_GiveAmmo p am_shell 5
		P_PlayerMessage p GOTSHELLBOX

	elif eq? sprite SPR_BPAK
		P_GiveBackpack p
		P_PlayerMessage p GOTBACKPACK

	elif eq? sprite SPR_BFUG
		jump no_give unless P_GiveWeapon p wp_bfg FALSE
		P_PlayerMessage p GOTBFG9000
		sound = sfx_wpnup

	elif eq? sprite SPR_MGUN
		jump no_give unless P_GiveWeapon p wp_chaingun dropped
		P_PlayerMessage p GOTCHAINGUN
		sound = sfx_wpnup

	elif eq? sprite SPR_CSAW
		jump no_give unless P_GiveWeapon p wp_chainsaw FALSE
		P_PlayerMessage p GOTCHAINSAW
		sound = sfx_wpnup

	elif eq? sprite SPR_LAUN
		jump no_give unless P_GiveWeapon p wp_missile FALSE
		P_PlayerMessage p GOTLAUNCHER
		sound = sfx_wpnup

	elif eq? sprite SPR_PLAS
		jump no_give unless P_GiveWeapon p wp_plasma FALSE
		P_PlayerMessage p GOTPLASMA
		sound = sfx_wpnup

	elif eq? sprite SPR_SHOT
		jump no_give unless P_GiveWeapon p wp_shotgun dropped
		P_PlayerMessage p GOTSHOTGUN
		sound = sfx_wpnup

	elif eq? sprite SPR_SGN2
		jump no_give unless P_GiveWeapon p  wp_supershotgun dropped
		P_PlayerMessage p GOTSHOTGUN2
		sound = sfx_wpnup

	else
		I_Error "P_SpecialThing: Unknown gettable thing"
	endif

	; count for intermission
	if some? (iand [special .flags] MF_COUNTITEM)
		[p .itemcount] = iadd [p .itemcount] 1
	endif

	[p .bonuscount] = iadd [p .bonuscount] BONUSADD

	P_RemoveMobj special
	S_StartHudSound (raw-cast [p .mo]) sound

	no_give:
end

;
; P_DamageMobj
;
; Damages both enemies and players.
;   "inflictor" is the thing that caused the damage, can be NULL (slime).
;   "source" is the thing to target after taking damage, or NULL.
;
; Source and inflictor are the same for melee attacks.
; Source can be NULL for slime, barrel explosions, etc.
;
fun P_DamageMobj (target ^mobj_t inflictor ^mobj_t source ^mobj_t damage s32)
	if zero? (iand [target .flags] MF_SHOOTABLE)
		return
	endif

	if le? [target .health] 0
		return
	endif

	if some? (iand [target .flags] MF_SKULLFLY)
		[target .momx] = 0
		[target .momy] = 0
		[target .momz] = 0
	endif

	sp ^player_t = NULL
	p  ^player_t = [target .player]

	if ref? source
		sp = [source .player]
	endif

	; take half damage in trainer mode
	if and (ref? p) (eq? [gameskill] sk_baby)
		damage = ishr damage 1
	endif

	info = [target .info]

	; Some close combat weapons should not
	; inflict thrust and push the victim out of reach,
	; thus kick away unless using the chainsaw.

	if ref? inflictor
		solid = zero? (iand [target .flags] MF_NOCLIP)
		kick  = null? sp

		if ref? sp
			kick = ne? [sp .ready] wp_chainsaw
		endif

		if and solid kick
			ang = R_PointToAngle2 [inflictor .x] [inflictor .y] [target .x] [target .y]

			thrust = imul  damage ((FRACUNIT >> 3) * 100)
			thrust = idivt thrust [info .mass]

			; fall forwards sometimes
			delta = isub [target .z] [inflictor .z]

			if and (lt? damage 40) (gt? damage [target .health]) (gt? delta (64 * FRACUNIT))
				r = P_Random
				if some? (iand r 1)
					ang    = iadd ang ANG180
					thrust = imul thrust 4
				endif
			endif

			ang = ishr ang ANGLEFINESHIFT

			dx  = FixedMul thrust [finecosine ang]
			dy  = FixedMul thrust [finesine   ang]

			[target .momx] = iadd [target .momx] dx
			[target .momy] = iadd [target .momy] dy
		endif
	endif

	; player specific

	if ref? p
		; end of game hell hack
		sector = [[target .subsector] .sector]
		if eq? [sector .special] 11
			damage = imin damage (isub [target .health] 1)
		endif

		; Below certain threshold,
		; ignore damage in GOD mode, or with INVUL power.
		god = some? (iand [p .cheats] CF_GODMODE)
		god = or god (pos? [p .powers pw_invulnerability])

		if and god (lt? damage 1000)
			return
		endif

		if pos? [p .armortype]
			divisor s32 = sel-if (eq? [p .armortype] 1) 3 2
			saved = idivt damage divisor

			if le? [p .armorpoints] saved
				; armor is used up
				saved = [p .armorpoints]
				[p .armortype] = 0
			endif

			[p .armorpoints] = isub [p .armorpoints] saved
			damage = isub damage saved
		endif

		; mirror mobj health here for Dave
		[p .health] = isub [p .health] damage
		[p .health] = imax [p .health] 0

		P_SetAttacker p source

		; add damage after armor / invuln
		[p .damagecount] = iadd [p .damagecount] damage
		[p .damagecount] = imin [p .damagecount] 100
	endif

	; do the damage
	[target .health] = isub [target .health] damage

	if le? [target .health] 0
		P_KillMobj target source
		return
	endif

	r = P_Random
	if lt? r [info .painchance]
		if zero? (iand [target .flags] MF_SKULLFLY)
			; fight back!
			[target .flags] = ior [target .flags] MF_JUSTHIT
			P_SetMobjState target [info .painstate]
		endif
	endif

	; we're awake now...
	[target .reactiontime] = 0

	; if not intent on another player, chase after this one
	if ref? source
		t_vile = eq? [target .type] MT_VILE
		s_vile = eq? [source .type] MT_VILE

		willing = or (zero? [target .threshold]) t_vile

		if and willing (ne? source target) (not s_vile)
			P_SetTarget target source

			[target .threshold] = BASETHRESHOLD

			if ne? [info .seestate] S_NULL
				if eq? [target .state] [addr-of states [info .spawnstate]]
					P_SetMobjState target [info .seestate]
				endif
			endif
		endif
	endif
end

;
; KillMobj
;
fun P_KillMobj (target ^mobj_t source ^mobj_t)
	[target .flags] = iand [target .flags] (~ (MF_SHOOTABLE | (MF_FLOAT | MF_SKULLFLY)))

	if ne? [target .type] MT_SKULL
		[target .flags] = iand [target .flags] (~ MF_NOGRAVITY)
	endif

	[target .flags]  = ior  [target .flags] (MF_CORPSE | MF_DROPOFF)
	[target .height] = ishr [target .height] 2

	sp ^player_t = NULL
	tp ^player_t = [target .player]

	if ref? source
		sp = [source .player]
	endif

	if ref? sp
		; count for intermission
		if some? (iand [target .flags] MF_COUNTKILL)
			[sp .killcount] = iadd [sp .killcount] 1
		endif

		if ref? tp
			pnum = [tp .index]
			[sp .frags pnum] = iadd [sp .frags pnum] 1
		endif

	elif and (not [netgame]) (some? (iand [target .flags] MF_COUNTKILL))
		; count all monster deaths,
		; even those caused by other monsters
		[players 0 .killcount] = iadd [players 0 .killcount] 1
	endif

	if ref? tp
		; count environment kills against you
		if null? source
			pnum = [tp .index]
			[tp .frags pnum] = iadd [tp .frags pnum] 1
		endif

		[target .flags] = iand [target .flags] (~ MF_SOLID)
		[tp .playerstate] = PST_DEAD

		P_DropWeapon tp

		if eq? [target .player] [addr-of players [consoleplayer]]
			; don't die in auto map
			AM_Stop
		endif
	endif

	; go into exploding death if damage was large enough
	info = [target .info]
	newstate = [info .deathstate]

	if some? [info .xdeathstate]
		if lt? [target .health] (ineg [info .spawnhealth])
			newstate = [info .xdeathstate]
		endif
	endif

	P_SetMobjState target newstate

	bump = P_Random
	bump = iand bump 3

	[target .tics] = isub [target .tics] bump
	[target .tics] = imax [target .tics] 1

	; In Chex Quest, monsters don't drop items.
	if eq? [gameversion] exe_chex
		return
	endif

	; Drop stuff.
	; determine the kind of object to spawn...

	item mobjtype_t = 0
	montype = [target .type]

	if eq? montype MT_POSSESSED
		item = MT_CLIP
	elif eq? montype MT_WOLFSS
		item = MT_CLIP
	elif eq? montype MT_SHOTGUY
		item = MT_SHOTGUN
	elif eq? montype MT_CHAINGUY
		item = MT_CHAINGUN
	endif

	if some? item
		mo = P_SpawnMobj [target .x] [target .y] ONFLOORZ item
		[mo .flags] = ior [mo .flags] MF_DROPPED
	endif
end
