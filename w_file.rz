;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

type wad_file_t struct
	.fstream  ^FILE   ; I/O handle
	.path     ^uchar  ; file's location on disk
	.length    s32    ; length of the file, in bytes
end

#public

fun W_OpenFile (path ^uchar -> ^wad_file_t)
	fstream = I_Fopen path "rb"

	if null? fstream
		return NULL
	endif

	; allocate a new wad_file_t to hold the file handle
	wad ^wad_file_t = Z_Malloc wad_file_t.size PU_STATIC

	[wad .fstream] = fstream
	[wad .length]  = M_FileLength fstream
	[wad .path]    = M_StringDuplicate path

	return wad
end

fun W_CloseFile (wad ^wad_file_t)
	I_Fclose [wad .fstream]
	I_Free [wad .path]
	Z_Free wad
end

; Read data from the specified position in the file into the
; provided buffer.  Returns the number of bytes read.

fun W_Read (wad ^wad_file_t offset s32 buffer ^raw-mem buflen s32 -> s32)
	I_Fseek [wad .fstream] offset SEEK_SET

	count = I_Fread buffer buflen [wad .fstream]
	return count
end
