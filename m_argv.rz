;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

zero-var myargv ^[0]^uchar
zero-var myargc s32

fun M_InitArgs (argc s32 argv ^[0]^uchar)
	[myargc] = argc
	[myargv] = argv
end

fun M_GetArg (idx s32 -> ^uchar)
	if ge? idx [myargc]
		return ""
	endif

	return [[myargv] idx]
end

;;
;; M_CheckParm
;;
;; Checks for the given parameter
;; in the program's command line arguments.
;; Returns the argument number (1 to argc-1)
;; or 0 if not present
;;
fun M_CheckParm (check ^uchar -> s32)
	p = M_CheckParmWithArgs check 0
	return p
end

fun M_CheckParmWithArgs (check ^uchar num s32 -> s32)
	i s32 = 1

	limit = isub [myargc] num

	loop while lt? i limit
		arg = [[myargv] i]

		cmp = M_StrCaseCmp check arg
		if zero? cmp
			return i
		endif

		i = iadd i 1
	endloop

	return 0
end

;;
;; M_ParmExists
;;
;; Returns true if the given parameter exists in the program's command
;; line arguments, false if not.
;;
fun M_ParmExists (check ^uchar -> bool)
	p = M_CheckParm check
	return pos? p
end

;-----------------------------------------------------------------------

;;
;; Find a response file, if given then read arguments
;;
fun M_FindResponseFile ()
	i s32 = 1

	loop while lt? i [myargc]
		arg = [[myargv] i]

		if eq? [arg] '@'
			M_LoadResponseFile i
			return
		endif

		i = iadd i 1
	endloop
end

#private

const MAXARGVS = 128

type NewArgArray [MAXARGVS]^uchar

fun M_LoadResponseFile (argv_index s32)
	filename = [[myargv] argv_index]
	filename = padd filename 1  ; drop the '@'

	; read in the entire file, get a NUL-terminated string.
	text = M_ReadResponseFile filename

	; create new arguments list array.
	; it has one extra pointer to accommodate a trailing NULL
	oldargv = [myargv]
	oldargc = [myargc]

	size = (conv s32 NewArgArray.size)
	size = iadd size 16

	[myargv] = I_Alloc size
	[myargc] = 0
	[[myargv] 0] = NULL

	; copy all the original arguments up to the response file
	i s32 = 0
	loop while lt? i argv_index
		M_AddResponseArg [oldargv i]
		i = iadd i 1
	endloop

	M_ParseResponseText text

	; add original arguments following the response file
	i s32 = iadd argv_index 1
	loop while lt? i oldargc
		M_AddResponseArg [oldargv i]
		i = iadd i 1
	endloop

	I_Free text
end

fun M_ReadResponseFile (filename ^uchar -> ^uchar)
	handle = I_Fopen filename "rb"
	if null? handle
		I_Error "\nNo such response file!"
	endif

	I_Print2 "Using response file: %s\n" filename

	size = M_FileLength handle
	buf ^uchar = I_Alloc (iadd size 1)
	s = buf

	loop while pos? size
		n = I_Fread s size handle

		if neg? n
			I_Error "Failed to read full contents of the file."
		endif

		s    = padd s n
		size = isub size n
	endloop

	; terminate with a NUL
	[s] = 0

	I_Fclose handle
	return buf
end

fun M_ParseResponseText (s ^uchar)
	loop
		ch = [s]
		break if zero? ch

		if M_IsSpace ch
			s = padd s 1
			jump continue
		endif

		arg = s

		; If the next argument is enclosed in quote marks, treat the
		; contents as a single argument.  This allows long filenames
		; to be specified.
		if eq? ch '"'
			; skip first quote
			s = padd s 1
			arg = s

			; read all characters between quotes
			loop
				ch = [s]
				break if eq? ch '"'

				if eq? ch 0 '\n'
					I_Error "unclosed string in response file"
				endif

				s = padd s 1
			endloop
		else
			loop
				ch = [s]
				break if zero? ch
				break if M_IsSpace ch

				s = padd s 1
			endloop
		endif

		; terminate string (replace final quote / space char)
		if ne? [s] 0
			[s] = 0
			s   = padd s 1
		endif

		M_AddResponseArg arg

		continue:
	endloop
end

fun M_AddResponseArg (s ^uchar)
	if ge? [myargc] MAXARGVS
		I_Error "too many arguments in response file"
	endif

	argv = [myargv]
	[argv [myargc]] = s

	[myargc] = iadd [myargc] 1
	[argv [myargc]] = NULL
end
