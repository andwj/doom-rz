;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

extern-fun R_CheckTextureNumForName (name ^[8]uchar -> s32)

extern-var texture_translation ^[0]s32
extern-var flat_translation    ^[0]s32

;
; Animating textures and planes.
;
type anim_t struct
	.is_tex   bool
	._pad     [3]u8

	.first    s32
	.last     s32
	.count    s32
	.speed    s32
end

;
; source animation definition
;
type animdef_t struct
	.is_tex   s32   ; if zero, it is a flat
	.speed    s32
	.end      ^uchar
	.start    ^uchar
end

; Floor/ceiling/wall animation sequences, defined by first and last frame.
; The full animation sequence is given using all the flats between the start
; and end entry, in the order found in the WAD file.

rom-var animdefs [23]animdef_t =
	; flats...
	{ 0  8  "NUKAGE3"   "NUKAGE1"  }
	{ 0  8  "FWATER4"   "FWATER1"  }
	{ 0  8  "SWATER4"   "SWATER1"  }
	{ 0  8  "LAVA4"     "LAVA1"    }
	{ 0  8  "BLOOD3"    "BLOOD1"   }

	; DOOM II flat animations.
	{ 0  8  "RROCK08"   "RROCK05"  }
	{ 0  8  "SLIME04"   "SLIME01"  }
	{ 0  8  "SLIME08"   "SLIME05"  }
	{ 0  8  "SLIME12"   "SLIME09"  }

	; textures...
	{ 1  8  "BLODGR4"   "BLODGR1"  }
	{ 1  8  "SLADRIP3"  "SLADRIP1" }
	{ 1  8  "BLODRIP4"  "BLODRIP1" }
	{ 1  8  "FIREWALL"  "FIREWALA" }
	{ 1  8  "GSTFONT3"  "GSTFONT1" }
	{ 1  8  "FIRELAVA"  "FIRELAV3" }
	{ 1  8  "FIREMAG3"  "FIREMAG1" }
	{ 1  8  "FIREBLU2"  "FIREBLU1" }
	{ 1  8  "ROCKRED3"  "ROCKRED1" }
	{ 1  8  "BFALL4"    "BFALL1"   }
	{ 1  8  "SFALL4"    "SFALL1"   }
	{ 1  8  "WFALL4"    "WFALL1"   }
	{ 1  8  "DBRAIN4"   "DBRAIN1"  }

	; end marker
	{-1 -1 "" ""}
end

const MAXANIMS = 64

zero-var anims       [MAXANIMS]anim_t
zero-var totalanims  s32

; andrewj: bumped up original limit of 64
const MAXLINEANIMS = (64 * 4)

zero-var linespecials     [MAXLINEANIMS]^line_t
zero-var numlinespecials  s32


fun P_InitPicAnims ()
	in  s32 = 0  ; input animdef_t
	out s32 = 0  ; output anim_t

	loop
		def = [addr-of animdefs in]
		in  = iadd in 1
		break if neg? [def .is_tex]

		anim = [addr-of anims out]

		if pos? [def .is_tex]
			; different game ?
			check = R_CheckTextureNumForName [def .start]
			jump skip if neg? check

			[anim .first] = R_TextureNumForName [def .start]
			[anim .last]  = R_TextureNumForName [def .end]
		else
			; different game ?
			check = W_CheckNumForName [def .start]
			jump skip if neg? check

			[anim .first] = R_FlatNumForName [def .start]
			[anim .last]  = R_FlatNumForName [def .end]
		endif

		[anim .is_tex] = pos? [def .is_tex]
		[anim .count]  = isub [anim .last] [anim .first]
		[anim .count]  = iadd [anim .count] 1
		[anim .speed]  = [def .speed]

		if lt? [anim .count] 2
			I_Error2 "P_InitPicAnims: bad cycle from %s" [def .start]
		endif

		out = iadd out 1

		skip:
	endloop

	[totalanims] = out
end

;;
;;  UTILITIES
;;

;
; getNextSector()
;
; Return sector_t * of sector next to current one.
; NULL if not a two-sided line.
;
fun getNextSector (ld ^line_t sec ^sector_t -> ^sector_t)
	if zero? (iand [ld .flags] ML_TWOSIDED)
		return NULL
	endif

	if eq? [ld .front] sec
		return [ld .back]
	endif

	return [ld .front]
end

;
; P_FindLowestFloorSurrounding
;
fun P_FindLowestFloorSurrounding (sec ^sector_t -> fixed_t)
	floor = [sec .floorh]

	i s32 = 0
	loop while lt? i [sec .linecount]
		check = [[sec .lines] i]
		other = getNextSector check sec

		if ref? other
			floor = imin floor [other .floorh]
		endif

		i = iadd i 1
	endloop

	return floor
end

;
; P_FindHighestFloorSurrounding
;
fun P_FindHighestFloorSurrounding (sec ^sector_t -> fixed_t)
	floor fixed_t = (-500 * FRACUNIT)

	i s32 = 0
	loop while lt? i [sec .linecount]
		check = [[sec .lines] i]
		other = getNextSector check sec

		if ref? other
			floor = imax floor [other .floorh]
		endif

		i = iadd i 1
	endloop

	return floor
end

;
; P_FindNextHighestFloor
;
; andrewj: reworked to not use an array, and not produce a fatal error
;          when the vanilla limit of 22 adjoining sectors is reached.
;
fun P_FindNextHighestFloor (sec ^sector_t base_h fixed_t -> fixed_t)
	found s32 = 0
	current = base_h

	i s32 = 0
	loop while lt? i [sec .linecount]
		check = [[sec .lines] i]
		other = getNextSector check sec

		if ref? other
			if gt? [other .floorh] base_h
				if zero? found
					current = [other .floorh]
				else
					current = imin current [other .floorh]
				endif

				found = iadd found 1
			endif
		endif

		i = iadd i 1
	endloop

	return current
end

;
; P_FindLowestCeilingSurrounding
;
fun P_FindLowestCeilingSurrounding (sec ^sector_t -> fixed_t)
	ceil fixed_t = FRAC_MAX

	i s32 = 0
	loop while lt? i [sec .linecount]
		check = [[sec .lines] i]
		other = getNextSector check sec

		if ref? other
			ceil = imin ceil [other .ceilh]
		endif

		i = iadd i 1
	endloop

	return ceil
end

;
; P_FindHighestCeilingSurrounding
;
fun P_FindHighestCeilingSurrounding (sec ^sector_t -> fixed_t)
	ceil fixed_t = 0

	i s32 = 0
	loop while lt? i [sec .linecount]
		check = [[sec .lines] i]
		other = getNextSector check sec

		if ref? other
			ceil = imax ceil [other .ceilh]
		endif

		i = iadd i 1
	endloop

	return ceil
end

;
; Return next sector # that line tag refers tO
;
fun P_FindSectorFromLineTag (ld ^line_t start s32 -> s32)
	i = iadd start 1

	loop while lt? i [numsectors]
		sec = [addr-of [sectors] i]
		if eq? [sec .tag] [ld .tag]
			return i
		endif
		i = iadd i 1
	endloop

	return -1
end

;;
;;  EVENTS
;;

; Events are operations triggered by using, crossing,
; or shooting special lines, or by timed thinkers.

;
; P_CrossSpecialLine - TRIGGER
; Called every time a thing origin is about
; to cross a line with a non 0 special.
;
fun P_CrossSpecialLine (ld ^line_t side s32 mo ^mobj_t)
	spec = [ld .special]

	; Triggers that monsters can activate
	if null? [mo .player]
		; projectiles should NOT trigger specials...
		kind = [mo .type]
		if eq? kind MT_ROCKET MT_PLASMA MT_BFG MT_TROOPSHOT MT_HEADSHOT MT_BRUISERSHOT
			return
		endif

		jump okay if eq? spec 39  ; TELEPORT TRIGGER
		jump okay if eq? spec 97  ; TELEPORT RETRIGGER
		jump okay if eq? spec 125 ; TELEPORT MONSTERONLY TRIGGER
		jump okay if eq? spec 126 ; TELEPORT MONSTERONLY RETRIGGER
		jump okay if eq? spec 4   ; RAISE DOOR
		jump okay if eq? spec 10  ; PLAT DOWN-WAIT-UP-STAY TRIGGER
		jump okay if eq? spec 88  ; PLAT DOWN-WAIT-UP-STAY RETRIGGER

		return
	endif

okay:
	; TRIGGERS.  All these are only usable once.

	if eq? spec 2  ; Open Door
		EV_DoDoor ld dr_open
		[ld .special] = 0

	elif eq? spec 3  ; Close Door
		EV_DoDoor ld dr_close
		[ld .special] = 0

	elif eq? spec 4  ; Raise Door
		EV_DoDoor ld dr_normal
		[ld .special] = 0

	elif eq? spec 5  ; Raise Floor
		EV_DoFloor ld fl_raiseFloor
		[ld .special] = 0

	elif eq? spec 6  ; Fast Ceiling Crush & Raise
		EV_DoCeiling ld ce_fastCrushAndRaise
		[ld .special] = 0

	elif eq? spec 8  ; Build Stairs
		EV_BuildStairs ld stair_build8
		[ld .special] = 0

	elif eq? spec 10  ; PlatDownWaitUp
		EV_DoPlat ld pl_downWaitUpStay 0
		[ld .special] = 0

	elif eq? spec 12  ; Light Turn On - brightest near
		EV_LightTurnOn ld 0
		[ld .special] = 0

	elif eq? spec 13  ; Light Turn On 255
		EV_LightTurnOn ld 255
		[ld .special] = 0

	elif eq? spec 16  ; Close Door 30
		EV_DoDoor ld dr_close30Open
		[ld .special] = 0

	elif eq? spec 17  ; Start Light Strobing
		EV_StartLightStrobing ld
		[ld .special] = 0

	elif eq? spec 19  ; Lower Floor
		EV_DoFloor ld fl_lowerFloor
		[ld .special] = 0

	elif eq? spec 22  ; Raise floor to nearest height and change texture
		EV_DoPlat ld pl_raiseToNearest 0
		[ld .special] = 0

	elif eq? spec 25  ; Ceiling Crush and Raise
		EV_DoCeiling ld ce_crushAndRaise
		[ld .special] = 0

	elif eq? spec 30  ; Raise floor to shortest texture height on either side
		EV_DoFloor ld fl_raiseToTexture
		[ld .special] = 0

	elif eq? spec 35  ; Lights Very Dark
		EV_LightTurnOn ld 35
		[ld .special] = 0

	elif eq? spec 36  ; Lower Floor (TURBO)
		EV_DoFloor ld fl_lowerTurbo
		[ld .special] = 0

	elif eq? spec 37  ; LowerAndChange
		EV_DoFloor ld fl_lowerAndChange
		[ld .special] = 0

	elif eq? spec 38  ; Lower Floor To Lowest
		EV_DoFloor ld fl_lowerToLowest
		[ld .special] = 0

	elif eq? spec 39  ; TELEPORT!
		EV_Teleport ld side mo
		[ld .special] = 0

	elif eq? spec 40  ; RaiseCeilingLowerFloor
		EV_DoCeiling ld ce_raiseToHighest
		EV_DoFloor   ld fl_lowerToLowest
		[ld .special] = 0

	elif eq? spec 44  ; Ceiling Crush
		EV_DoCeiling ld ce_lowerAndCrush
		[ld .special] = 0

	elif eq? spec 52  ; EXIT!
		G_ExitLevel
		[ld .special] = 0

	elif eq? spec 53  ; Perpetual Platform Raise
		EV_DoPlat ld pl_perpetualRaise 0
		[ld .special] = 0

	elif eq? spec 54  ; Platform Stop
		EV_StopPlat ld
		[ld .special] = 0

	elif eq? spec 56  ; Raise Floor Crush
		EV_DoFloor ld fl_raiseFloorCrush
		[ld .special] = 0

	elif eq? spec 57  ; Ceiling Crush Stop
		EV_CeilingCrushStop ld
		[ld .special] = 0

	elif eq? spec 58  ; Raise Floor 24
		EV_DoFloor ld fl_raise24
		[ld .special] = 0

	elif eq? spec 59  ; Raise Floor 24 And Change
		EV_DoFloor ld fl_raise24Change
		[ld .special] = 0

	elif eq? spec 104  ; Turn lights off in sector(tag)
		EV_LightTurnOff ld
		[ld .special] = 0

	elif eq? spec 108  ; Blazing Door Raise (faster than TURBO!)
		EV_DoDoor ld dr_blazeRaise
		[ld .special] = 0

	elif eq? spec 109  ; Blazing Door Open (faster than TURBO!)
		EV_DoDoor ld dr_blazeOpen
		[ld .special] = 0

	elif eq? spec 100  ; Build Stairs Turbo 16
		EV_BuildStairs ld stair_turbo16
		[ld .special] = 0

	elif eq? spec 110  ; Blazing Door Close (faster than TURBO!)
		EV_DoDoor ld dr_blazeClose
		[ld .special] = 0

	elif eq? spec 119  ; Raise floor to nearest surr. floor
		EV_DoFloor ld fl_raiseToNearest
		[ld .special] = 0

	elif eq? spec 121  ; Blazing PlatDownWaitUpStay
		EV_DoPlat ld pl_downWaitBlaze 0
		[ld .special] = 0

	elif eq? spec 124  ; Secret EXIT
		G_SecretExitLevel
		[ld .special] = 0

	elif eq? spec 125  ; TELEPORT MonsterONLY
		if null? [mo .player]
			EV_Teleport ld side mo
			[ld .special] = 0
		endif

	elif eq? spec 130  ; Raise Floor Turbo
		EV_DoFloor ld fl_raiseNearestTurbo
		[ld .special] = 0

	elif eq? spec 141  ; Silent Ceiling Crush & Raise
		EV_DoCeiling ld ce_silentCrushAndRaise
		[ld .special] = 0
	endif

	; RETRIGGERS.  All of these can be used again.

	if eq? spec 72  ; Ceiling Crush
		EV_DoCeiling ld ce_lowerAndCrush

	elif eq? spec 73  ; Ceiling Crush and Raise
		EV_DoCeiling ld ce_crushAndRaise

	elif eq? spec 74  ; Ceiling Crush Stop
		EV_CeilingCrushStop ld

	elif eq? spec 75  ; Close Door
		EV_DoDoor ld dr_close

	elif eq? spec 76  ; Close Door 30
		EV_DoDoor ld dr_close30Open

	elif eq? spec 77  ; Fast Ceiling Crush & Raise
		EV_DoCeiling ld ce_fastCrushAndRaise

	elif eq? spec 79  ; Lights Very Dark
		EV_LightTurnOn ld 35

	elif eq? spec 80  ; Light Turn On - brightest near
		EV_LightTurnOn ld 0

	elif eq? spec 81  ; Light Turn On 255
		EV_LightTurnOn ld 255

	elif eq? spec 82  ; Lower Floor To Lowest
		EV_DoFloor ld fl_lowerToLowest

	elif eq? spec 83  ; Lower Floor
		EV_DoFloor ld fl_lowerFloor

	elif eq? spec 84  ; LowerAndChange
		EV_DoFloor ld fl_lowerAndChange

	elif eq? spec 86  ; Open Door
		EV_DoDoor ld dr_open

	elif eq? spec 87  ; Perpetual Platform Raise
		EV_DoPlat ld pl_perpetualRaise 0

	elif eq? spec 88  ; PlatDownWaitUp
		EV_DoPlat ld pl_downWaitUpStay 0

	elif eq? spec 89  ; Platform Stop
		EV_StopPlat ld

	elif eq? spec 90  ; Raise Door
		EV_DoDoor ld dr_normal

	elif eq? spec 91  ; Raise Floor
		EV_DoFloor ld fl_raiseFloor

	elif eq? spec 92  ; Raise Floor 24
		EV_DoFloor ld fl_raise24

	elif eq? spec 93  ; Raise Floor 24 And Change
		EV_DoFloor ld fl_raise24Change

	elif eq? spec 94  ; Raise Floor Crush
		EV_DoFloor ld fl_raiseFloorCrush

	elif eq? spec 95  ; Raise floor to nearest height and change texture.
		EV_DoPlat ld pl_raiseToNearest 0

	elif eq? spec 96  ; Raise floor to shortest texture height on either side
		EV_DoFloor ld fl_raiseToTexture

	elif eq? spec 97  ; TELEPORT!
		EV_Teleport ld side mo

	elif eq? spec 98  ; Lower Floor (TURBO)
		EV_DoFloor ld fl_lowerTurbo

	elif eq? spec 105  ; Blazing Door Raise (faster than TURBO!)
		EV_DoDoor ld dr_blazeRaise

	elif eq? spec 106  ; Blazing Door Open (faster than TURBO!)
		EV_DoDoor ld dr_blazeOpen

	elif eq? spec 107  ; Blazing Door Close (faster than TURBO!)
		EV_DoDoor ld dr_blazeClose

	elif eq? spec 120  ; Blazing PlatDownWaitUpStay.
		EV_DoPlat ld pl_downWaitBlaze 0

	elif eq? spec 126  ; TELEPORT MonsterONLY.
		if null? [mo .player]
			EV_Teleport ld side mo
		endif

	elif eq? spec 128  ; Raise To Nearest Floor
		EV_DoFloor ld fl_raiseToNearest

	elif eq? spec 129  ; Raise Floor Turbo
		EV_DoFloor ld fl_raiseNearestTurbo
	endif
end

;
; P_PlayerInSpecialSector
; Called every tic that the player origin is in a special sector.
;
fun P_PlayerInSpecialSector (p ^player_t)
	mo  = [p .mo]
	sec = [[mo .subsector] .sector]

	; Falling, not all the way down yet?
	if ne? [mo .z] [sec .floorh]
		return
	endif

	; Is on the ground.

	do_damage = zero? (iremt [leveltime] 32)

	; player lacks the radiation suit
	no_suit = zero? [p .powers pw_ironfeet]

	spec = [sec .special]

	if eq? spec 5  ; HELLSLIME DAMAGE
		if and do_damage no_suit
			P_DamageMobj mo NULL NULL 10
		endif

	elif eq? spec 7  ; NUKAGE DAMAGE
		if and do_damage no_suit
			P_DamageMobj mo NULL NULL 5
		endif

	elif eq? spec 16 4  ; SUPER HELLSLIME DAMAGE
		if not no_suit
			r = P_Random
			if lt? r 5
				no_suit = TRUE
			endif
		endif

		if and do_damage no_suit
			P_DamageMobj mo NULL NULL 20
		endif

	elif eq? spec 9  ; SECRET SECTOR
		[p .secretcount] = iadd [p .secretcount] 1
		[sec .special]   = 0

	elif eq? spec 11  ; EXIT SUPER DAMAGE! (for E1M8 finale)
		[p .cheats] = iand [p .cheats] (~ CF_GODMODE)

		if do_damage
			P_DamageMobj mo NULL NULL 20
		endif

		if le? [p .health] 10
			G_ExitLevel
		endif
	endif
end

#public

;
; P_UpdateSpecials
; Animate planes, scroll walls, etc.
;
fun P_UpdateSpecials ()
	;  animate flats and textures globally
	i s32 = 0
	loop while lt? i [totalanims]
		anim = [addr-of anims i]

		k = [anim .first]
		loop while le? k [anim .last]
			along = idivt [leveltime] [anim .speed]
			along = iadd  along k
			along = iremt along [anim .count]

			pic = iadd [anim .first] along

			if [anim .is_tex]
				[[texture_translation] k] = pic
			else
				[[flat_translation] k] = pic
			endif

			k = iadd k 1
		endloop

		i = iadd i 1
	endloop

	; animate line specials
	i s32 = 0
	loop while lt? i [numlinespecials]
		ld   = [linespecials i]
		spec = [ld .special]
		side = [addr-of [sides] [ld .sidenum 0]]

		if eq? spec 48
			; SCROLL LEFT
			[side .x_offset] = iadd [side .x_offset] FRACUNIT

		elif eq? spec 85
			; SCROLL RIGHT
			[side .x_offset] = isub [side .x_offset] FRACUNIT
		endif

		i = iadd i 1
	endloop

	; animate switches
	P_UpdateButtons

	; respawn items
	if eq? [deathmatch] 2
		P_RespawnSpecials
	endif
end

#private

;;
;;  SPECIAL SPAWNING
;;

;
; P_SpawnSpecials
; After the map has been loaded, scan for specials that spawn thinkers
;
fun P_SpawnSpecials ()
	; -- Init special SECTORs --

	i s32 = 0
	loop while lt? i [numsectors]
		sec = [addr-of [sectors] i]
		special = [sec .special]

		if eq? special 1
			; FLICKERING LIGHTS
			P_SpawnLightFlash sec

		elif eq? special 2
			; STROBE FAST
			P_SpawnStrobeFlash sec FASTDARK FALSE

		elif eq? special 3
			; STROBE SLOW
			P_SpawnStrobeFlash sec SLOWDARK FALSE

		elif eq? special 4
			; STROBE FAST/DEATH SLIME
			P_SpawnStrobeFlash sec FASTDARK FALSE
			[sec .special] = 4

		elif eq? special 8
			; GLOWING LIGHT
			P_SpawnGlowingLight sec

		elif eq? special 9
			; SECRET SECTOR
			[totalsecret] = iadd [totalsecret] 1

		elif eq? special 10
			; DOOR CLOSE IN 30 SECONDS
			P_SpawnDoorCloseIn30 sec

		elif eq? special 12
			; SYNC STROBE SLOW
			P_SpawnStrobeFlash sec SLOWDARK TRUE

		elif eq? special 13
			; SYNC STROBE FAST
			P_SpawnStrobeFlash sec FASTDARK TRUE

		elif eq? special 14
			; DOOR RAISE IN 5 MINUTES
			P_SpawnDoorRaiseIn5Mins sec

		elif eq? special 17
			P_SpawnFireFlicker sec
		endif

		i = iadd i 1
	endloop

	; -- Init line EFFECTs --

	[numlinespecials] = 0

	i s32 = 0
	loop while lt? i [numlines]
		ld   = [addr-of [lines] i]
		spec = [ld .special]

		if eq? spec 48 85
			; andrewj: ignore overflow here rather than bomb out
			if lt? [numlinespecials] MAXLINEANIMS
				[linespecials [numlinespecials]] = ld
				[numlinespecials] = iadd [numlinespecials] 1
			endif
		endif

		i = iadd i 1
	endloop
end
