;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

zero-var cursubsec ^subsector_t
zero-var curline   ^seg_t

; andrewj: replaced 'solidsegs' (an array of horizontal pixel ranges) with
;          'solidcolumns' (an array with a boolean for each screen column).
;          I was concerned it may be offer worse performance, but PrBoom
;          uses the same approach, and that encouraged me to follow suit.

zero-var solidcolumns [SCREENWIDTH]bool

; first  index is 'boxy' : 0 for viewy >= TOP,  1 central, 2 for viewy <= BOTTOM
; second index is 'boxx' : 0 for viewx <= LEFT, 1 central, 2 for viewx >= RIGHT
; third  index is coordinate to make: x1, y1, x2, y2.
rom-var checkcoord [3][3][4]s32 =
	array
		{ BOXRIGHT BOXTOP    BOXLEFT  BOXBOTTOM }
		{ BOXRIGHT BOXTOP    BOXLEFT  BOXTOP    }
		{ BOXRIGHT BOXBOTTOM BOXLEFT  BOXTOP    }
	endarray

	array
		{ BOXLEFT  BOXTOP    BOXLEFT  BOXBOTTOM }
		{ -1       -1        -1       -1        }  ; unused, view inside bbox
		{ BOXRIGHT BOXBOTTOM BOXRIGHT BOXTOP    }
	endarray

	array
		{ BOXLEFT  BOXTOP    BOXRIGHT BOXBOTTOM }
		{ BOXLEFT  BOXBOTTOM BOXRIGHT BOXBOTTOM }
		{ BOXLEFT  BOXBOTTOM BOXRIGHT BOXTOP    }
	endarray
end

;
; R_ClearClipSegs
;
fun R_ClearClipSegs ()
	I_MemSet solidcolumns 0 SCREENWIDTH
end

;
; R_ClipWallSegment
;
; Clips the current segment to the given range of columns.
; When 'marksolid' is true, also updates the solidcolumns array.
;
fun R_ClipWallSegment (x s32 x2 s32 solid bool)
	loop while le? x x2
		loop while [solidcolumns x]
			x = iadd x 1
			if gt? x x2
				return
			endif
		endloop

		first = x

		loop while not [solidcolumns x]
			if solid
				[solidcolumns x] = TRUE
			endif
			x = iadd x 1
			break if gt? x x2
		endloop

		last = isub x 1

		R_StoreWallRange first last

		x = iadd x 1
	endloop
end

;
; R_AddLine
;
fun R_AddLine (seg ^seg_t)
	[curline] = seg

	; determine angles of left/right edges of the segment
	x1 = [[seg .v1] .x]
	y1 = [[seg .v1] .y]
	x2 = [[seg .v2] .x]
	y2 = [[seg .v2] .y]

	angle1 = R_PointToAngle2 [viewx] [viewy] x1 y1
	angle2 = R_PointToAngle2 [viewx] [viewy] x2 y2

	; global angle needed by segcalc.
	[rw_angle1] = angle1

	angle1 = isub angle1 [viewangle]
	angle2 = isub angle2 [viewangle]

	span = isub angle1 angle2

	; cull back sides
	if ge? span ANG180
		return
	endif

	; clip to view edges...

	clip  = [xtoviewangle 0]  ; close to ANG45, but not exactly the same
	fov   = ishl clip 1  ; close to ANG90

	tspan = iadd clip angle1  ; really 'angle1 - rightclip'

	; is angle1 past the left side of view frustum?
	if gt? tspan fov
		tspan = isub tspan fov

		if ge? tspan span
			; both angles are off the left edge
			return
		endif

		angle1 = clip
	endif

	tspan = isub clip angle2  ; 'leftclip - angle2'

	; is angle2 past the right side of view frustum?
	if gt? tspan fov
		tspan = isub tspan fov

		if ge? tspan span
			; both angles are off the right edge
			return
		endif

		angle2 = isub 0 clip  ; close to ANG315
	endif

	; seg is in the view range, but not necessarily visible
	angle1 = iadd angle1 ANG90
	angle2 = iadd angle2 ANG90

	angle1 = ishr angle1 ANGLEFINESHIFT
	angle2 = ishr angle2 ANGLEFINESHIFT

	x1 = [viewangletox angle1]
	x2 = [viewangletox angle2]
	x2 = isub x2 1

	if lt? x2 x1
		; does not cross a pixel
		return
	endif

	; get sector references
	front = [[cursubsec] .sector]
	back  = [seg .back]

	; single sided line?
	if null? back
		R_ClipWallSegment x1 x2 TRUE
		return
	endif

	; closed door?
	closed1 = le? [back .ceilh]  [front .floorh]
	closed2 = ge? [back .floorh] [front .ceilh]

	if or closed1 closed2
		R_ClipWallSegment x1 x2 TRUE
		return
	endif

	; window?
	jump clippass if ne? [back .ceilh]  [front .ceilh]
	jump clippass if ne? [back .floorh] [front .floorh]

	; Reject empty lines used for triggers and special events.
	; Identical floor and ceiling on both sides,
	; identical light levels on both sides,
	; and no middle texture.
	jump clippass if ne? [back .floorpic] [front .floorpic]
	jump clippass if ne? [back .ceilpic]  [front .ceilpic]
	jump clippass if ne? [back .light]  [front .light]

	midtex = [[seg .sidedef] .mid]
	if zero? midtex
		return
	endif

clippass:
	R_ClipWallSegment x1 x2 FALSE
end

;
; R_CheckBBox
;
; Checks BSP node/subtree bounding box.
; Returns true if some part of the bbox might be visible.
;
fun R_CheckBBox (bbox ^[4]fixed_t -> bool)
	; find corners of the box, defining the edges from current viewpoint.
	boxx s32 = 1
	boxy s32 = 1

	if le? [viewx] [bbox BOXLEFT]
		boxx = 0
	elif ge? [viewx] [bbox BOXRIGHT]
		boxx = 2
	endif

	if ge? [viewy] [bbox BOXTOP]
		boxy = 0
	elif le? [viewy] [bbox BOXBOTTOM]
		boxy = 2
	elif eq? boxx 1
		; viewpoint is inside this bounding box
		return TRUE
	endif

	coords ^[4]s32 = [addr-of checkcoord boxy boxx]

	x1 = [bbox [coords 0]]
	y1 = [bbox [coords 1]]
	x2 = [bbox [coords 2]]
	y2 = [bbox [coords 3]]

	; use angles to see if bbox is outside view area
	angle1 = R_PointToAngle2 [viewx] [viewy] x1 y1
	angle2 = R_PointToAngle2 [viewx] [viewy] x2 y2

	angle1 = isub angle1 [viewangle]
	angle2 = isub angle2 [viewangle]

	span = isub angle1 angle2

	; does the viewpoint sit exactly on an edge of the bbox?
	; note it is not possible for span to be > ANG180 here.
	if ge? span ANG180
		return TRUE
	endif

	clip = [xtoviewangle 0]  ; close to ANG45, but not exactly the same
	fov   = ishl clip 1

	tspan = iadd clip angle1  ; really 'angle1 - rightclip'

	; is angle1 past the left side of view frustum?
	if gt? tspan fov
		tspan = isub tspan fov

		if ge? tspan span
			; both angles are off the left edge
			return FALSE
		endif

		angle1 = clip
	endif

	tspan = isub clip angle2

	; is angle2 past the right side of view frustum?
	if gt? tspan fov
		tspan = isub tspan fov

		if ge? tspan span
			; both angles are off the right edge
			return FALSE
		endif

		angle2 = isub 0 clip  ; close to ANG315
	endif

	; convert angles to pixel range and test clipping
	angle1 = iadd angle1 ANG90
	angle2 = iadd angle2 ANG90

	angle1 = ishr angle1 ANGLEFINESHIFT
	angle2 = ishr angle2 ANGLEFINESHIFT

	x1 = [viewangletox angle1]
	x2 = [viewangletox angle2]
	x2 = isub x2 1

	if lt? x2 x1
		; does not cross a pixel
		return FALSE
	endif

	p     = [addr-of solidcolumns x1]
	p_end = [addr-of solidcolumns x2]

	loop while le? p p_end
		if not [p]
			; at least one column is non-solid
			return TRUE
		endif
		p = padd p 1
	endloop

	return FALSE
end

;
; R_Subsector
;
; Determine floor/ceiling planes.
; Add sprites of things in sector.
; Draw one or more line segments.
;
fun R_Subsector (num s32)
	sub = [addr-of [subsectors] num]
	sec = [sub .sector]

	[cursubsec] = sub

	[floorplane]   = NULL
	[ceilingplane] = NULL

	if lt? [sec .floorh] [viewz]
		[floorplane] = R_FindPlane [sec .floorh] [sec .floorpic] [sec .light]
	endif

	; always create a sky ceiling plane
	jump make_ceiling if eq? [sec .ceilpic] [skyflatnum]

	if gt? [sec .ceilh] [viewz]
		make_ceiling:
		[ceilingplane] = R_FindPlane [sec .ceilh] [sec .ceilpic] [sec .light]
	endif

	R_AddSprites sec

	i s32     = [sub .firstline]
	i_end s32 = iadd i [sub .numlines]

	loop while lt? i i_end
		seg = [addr-of [segs] i]
		R_AddLine seg
		i = iadd i 1
	endloop
end

fun R_RenderBSPNode (bspnum s32)
	; reached a subsector?
	; first case here is for maps with no nodes (only a single subsector)
	if eq? bspnum -1
		R_Subsector 0
		return
	endif

restart:
	if some? (iand bspnum NF_SUBSECTOR)
		bspnum = iand bspnum (~ NF_SUBSECTOR)
		R_Subsector bspnum
		return
	endif

	bsp = [addr-of [nodes] bspnum]

	; decide which side the view point is on
	side = R_PointOnNodeSide [viewx] [viewy] bsp

	; recursively divide the front space
	R_RenderBSPNode [bsp .children side]

	; potentially divide the back space
	side = ixor side 1
	bbox = [addr-of bsp .bbox side]

	visible = R_CheckBBox bbox

	if visible
		bspnum = [bsp .children side]
		jump restart
	endif
end

;------------------------------------------------------------------------

#public

;
; R_PointInSubsector
;
; Traverse BSP (sub) tree, determine subsector containing the
; given 2D point on the map.  always returns a valid subsector_t.
;
fun R_PointInSubsector (x fixed_t y fixed_t -> ^subsector_t)
	; single subsector is a special case
	if zero? [numnodes]
		return [addr-of [subsectors] 0]
	endif

	nodenum = isub [numnodes] 1

	loop while lt? nodenum NF_SUBSECTOR
		bsp     = [addr-of [nodes] nodenum]
		side    = R_PointOnNodeSide x y bsp
		nodenum = [bsp .children side]
	endloop

	nodenum = iand nodenum (~ NF_SUBSECTOR)

	return [addr-of [subsectors] nodenum]
end

;
; R_PointOnNodeSide
;
; Check a point against a partition plane.
; Returns side 0 (front) or 1 (back).
;
; Logic is same as P_PointOnDivlineSide except for the precision
; of the final check (here shifts by 16 bits, other shifts by 8).
; Since playsim uses this via R_PointInSubsector, we cannot use the
; other function instead, it would likely cause demos to desync.
;
fun R_PointOnNodeSide (x fixed_t y fixed_t node ^node_t -> s32)
	if zero? [node .dx]
		if le? x [node .x]
			check = pos? [node .dy]
		else
			check = neg? [node .dy]
		endif

		return conv s32 check
	endif

	if zero? [node .dy]
		if le? y [node .y]
			check = neg? [node .dx]
		else
			check = pos? [node .dx]
		endif

		return conv s32 check
	endif

	dx = isub x [node .x]
	dy = isub y [node .y]

	; try to quickly decide by looking at sign bits
	combo = ixor dx dy [node .dx] [node .dy]

	if some? (iand combo 0x80000000)
		combo = ixor dx [node .dy]
		check = some? (iand combo 0x80000000)
		return conv s32 check
	endif

	left  = ishr [node .dy] FRACBITS
	right = ishr [node .dx] FRACBITS

	left  = FixedMul left  dx
	right = FixedMul right dy

	check = le? left right
	return conv s32 check
end
