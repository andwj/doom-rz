;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;


; The complete set of data for a particular tic.

type ticcmd_set_t struct
	.cmds   [MAXPLAYERS]ticcmd_t
	.ingame [MAXPLAYERS]bool
end

; Maximum time that we wait in TryRunTics() for netgame data to be
; received before we bail out and render a frame anyway.
; Vanilla Doom used 20 for this value, but we use a smaller value
; instead for better responsiveness of the menu when we're stuck.
const MAX_NETGAME_STALL_TICS = 5

;
; gametic is the tic about to (or currently being) run
; maketic is the tic that hasn't had control made for it yet
; recvtic is the latest tic received from the server.
;
; a gametic cannot be run until ticcmds are received for it
; from all players.
;
zero-var ticdata [BACKUPTICS]ticcmd_set_t

; The index of the next tic to be made (with a call to BuildTiccmd).
zero-var maketic s32

; The number of complete tics received from the server so far.
zero-var recvtic s32

; Index of the local player.
zero-var localplayer s32

; Used for original sync code.
zero-var skiptics s32

zero-var lasttime s32
zero-var oldentertics s32

zero-var pl_exitmsg [80]uchar

#public

; The number of tics that have been run (using RunTic) so far.
zero-var gametic s32

; When set to true, a single tic is run each time TryRunTics() is called.
; This is used for -timedemo mode.
zero-var singletics bool

; Reduce the bandwidth needed by sampling game input less and transmitting
; less.  If ticdup is 2, sample half normal, 3 = one third normal, etc.
zero-var ticdup s32

; Amount to offset the timer for game sync.
zero-var offsetms fixed_t

; Current players in the multiplayer game.
; This is distinct from playeringame[] used by the game code, which may
; modify playeringame[] when playing back multiplayer demos.
zero-var local_playeringame [MAXPLAYERS]bool

zero-var netcmds ^[0]ticcmd_t

zero-var net_client_connected bool

#private

; 35 fps clock adjusted by offsetms milliseconds

fun GetAdjustedTime (-> s32)
	time_ms = I_GetTimeMS
	time_ms = imul  time_ms TICRATE
	time_ms = idivt time_ms 1000
	return time_ms
end

fun BuildNewTic (-> bool)
	I_StartTic
	D_ProcessEvents

	; Always run the menu
	M_Ticker

	gameticdiv = idivt [gametic] [ticdup]
	difftics   = isub  [maketic] gameticdiv

	if ge? difftics 5
		return FALSE
	endif

	cmd = stack-var ticcmd_t

	G_BuildTiccmd cmd [maketic]

	;;  if (net_client_connected)
	;;  {
	;;      NET_CL_SendTiccmd(&cmd, maketic)
	;;  }

	idx  = iremt [maketic] BACKUPTICS
	data = [addr-of ticdata idx]

	player = [localplayer]

	I_MemCopy [addr-of data .cmds player] cmd ticcmd_t.size
	[data .ingame player] = TRUE

	[maketic] = iadd [maketic] 1

	return TRUE
end

#public

;
; NetUpdate
; Builds ticcmds for console player,
; sends out a packet
;
fun NetUpdate ()
	; If we are running with singletics (timing a demo), this
	; is all done separately.
	if [singletics]
		return
	endif

	; check time
	nowtime = GetAdjustedTime
	nowtime = idivt nowtime [ticdup]

	newtics = isub nowtime [lasttime]
	[lasttime] = nowtime

	if le? [skiptics] newtics
		newtics = isub newtics [skiptics]
		[skiptics] = 0
	else
		[skiptics] = isub [skiptics] newtics
		newtics = 0
	endif

	; build new ticcmds for console player
	loop
		break if le? newtics 0

		build_res = BuildNewTic
		break if not build_res

		newtics = isub newtics 1
	endloop
end

#public

;; fun D_ReceiveTic (...)
;;    ...
;; end

;
; Start game loop
;
; Called after the screen is set but before the game starts running.
;
fun D_StartGameLoop ()
	nowtime    = GetAdjustedTime
	[lasttime] = idivt nowtime [ticdup]
end

fun D_StartNetGame ()
	[offsetms] = 0
	[recvtic] = 0

	num_players s32 = 1

	;
	; Reduce the resolution of the game by a factor of n, reducing
	; the amount of network bandwidth needed.
	;
	i = M_CheckParmWithArgs "-dup" 1

	if pos? i
		arg      = M_GetArg (iadd i 1)
		[ticdup] = M_Atoi arg
	else
		[ticdup] = 1
	endif

	if lt? [ticdup] 1
		I_Error "D_StartNetGame: invalid ticdup value"
	endif

	;;  if (net_client_connected)
	;;  {
	;;      // Send our game settings and block until game start is received
	;;      // from the server.
	;;
	;;      NET_CL_StartGame(settings)
	;;      BlockUntilStart(settings, callback)
	;;
	;;      // Read the game settings that were received.
	;;
	;;      NET_CL_GetSettings(settings)
	;;  }

	; Set the local player and playeringame[] values.

	[localplayer] = 0

	i s32 = 0
	loop while lt? i MAXPLAYERS
		[local_playeringame i] = lt? i num_players
		i = iadd i 1
	endloop
end

fun D_InitNetGame (-> bool)
	return FALSE
end

#private

fun GetLowTic (-> s32)
	lowtic = [maketic]

	;;  if (net_client_connected)
	;;  {
	;;      if (drone || recvtic < lowtic)
	;;      {
	;;          lowtic = recvtic
	;;      }
	;;  }

	return lowtic
end

; Returns true if there are players in the game:

fun PlayersInGame (-> bool)
	return TRUE
end

; When using ticdup, certain values must be cleared out when running
; the duplicate ticcmds.

fun TicdupSquash (set ^ticcmd_set_t)
	i s32 = 0

	loop while lt? i MAXPLAYERS
		cmd = [addr-of set .cmds i]

		[cmd .chatchar] = 0
		if some? (iand [cmd .buttons] BT_SPECIAL)
			[cmd .buttons] = 0
		endif

		i = iadd i 1
	endloop
end

; When running in single player mode, clear all the ingame[] array
; except the local player.

fun SinglePlayerClear (set ^ticcmd_set_t)
	i s32 = 0

	loop while lt? i MAXPLAYERS
		if ne? i [localplayer]
			[set .ingame i] = FALSE
		endif

		i = iadd i 1
	endloop
end

#public

;
; TryRunTics
;
fun TryRunTics ()
	; get real tics
	entertic = I_GetTime
	entertic = idivt entertic [ticdup]

	realtics = isub entertic [oldentertics]
	[oldentertics] = entertic

	; in singletics mode, run a single tic every time this function
	; is called.
	if [singletics]
		BuildNewTic
	else
		NetUpdate
	endif

	lowtic = GetLowTic

	gameticdiv = idivt [gametic] [ticdup]
	availabletics = isub lowtic gameticdiv

	; decide how many tics to run

	if lt? (iadd realtics 1) availabletics
		counts = iadd realtics 1
	elif lt? realtics availabletics
		counts = realtics
	else
		counts = availabletics
	endif

	counts = imax counts 1

	; wait for new tics if needed
	loop
		if not PlayersInGame
			; ok
		elif lt? lowtic (iadd gameticdiv counts)
			; ok
		else
			break
		endif

		NetUpdate

		lowtic = GetLowTic
		gameticdiv = idivt [gametic] [ticdup]

		if lt? lowtic gameticdiv
			I_Error "TryRunTics: lowtic < gametic"
		endif

		; Still no tics to run? Sleep until some are available.
		if lt? lowtic (iadd gameticdiv counts)
			; If we're in a netgame, we might spin forever waiting for
			; new network data to be received. So don't stay in here
			; forever - give the menu a chance to work.
			curtic = I_GetTime
			curtic = idivt curtic [ticdup]

			if ge? curtic (iadd entertic MAX_NETGAME_STALL_TICS)
				return
			endif

			I_Sleep 1
		endif
	endloop

	; run the count * ticdup dics
	loop until zero? counts
		counts = isub counts 1

		any_players = PlayersInGame
		if not any_players
			return
		endif

		gameticdiv = idivt [gametic] [ticdup]
		pos = iremt gameticdiv BACKUPTICS

		set = [addr-of ticdata pos]

		; if TRUE  ;; not [net_client_connected]
			SinglePlayerClear set
		; endif

		i s32 = [ticdup]
		loop until zero? i
			i = isub i 1

			gameticdiv = idivt [gametic] [ticdup]
			if gt? gameticdiv lowtic
				I_Error "gametic > lowtic"
			endif

			I_MemCopy local_playeringame [addr-of set .ingame] MAXPLAYERS

			D_RunTic set

			[gametic] = iadd [gametic] 1

			; modify command for duplicated tics
			TicdupSquash set
		endloop

		; check for new console commands
		NetUpdate
	endloop
end

#private

fun StrictDemos (-> bool)
	;
	; When recording or playing back demos, disable any extensions
	; of the vanilla demo format - record demos as vanilla would do,
	; and play back demos as vanilla would do.
	;
	check = M_ParmExists "-strictdemos"
	return check
end

; Returns true if the given lump number corresponds to data from a .lmp
; file, as opposed to a WAD.
fun IsDemoFile (lumpnum s32 -> bool)
	path = W_GetPathForLump lumpnum
	return M_HasExtension path "lmp"
end

#public

;
; If the provided conditional value is true, we're trying to record
; a demo file that will include a non-vanilla extension. The function
; will return true if the conditional is true and it's allowed to use
; this extension (no extensions are allowed if -strictdemos is given
; on the command line). A warning is shown on the console using the
; provided string describing the non-vanilla expansion.
;
fun D_NonVanillaRecord (conditional bool feature ^uchar -> bool)
	if not conditional
		return FALSE
	endif

	strict = StrictDemos
	if strict
		return FALSE
	endif

	I_Print  "Warning: Recording a demo file with a non-vanilla extension "
	I_Print2 "(%s). Use -strictdemos to disable this extension.\n" feature

	return TRUE
end

;
; If the provided conditional value is true, we're trying to play back
; a demo that includes a non-vanilla extension. We return true if the
; conditional is true and it's allowed to use this extension, checking
; that:
;  - The -strictdemos command line argument is not provided.
;  - The given lumpnum identifying the demo to play back identifies a
;    demo that comes from a .lmp file, not a .wad file.
;  - Before proceeding, a warning is shown to the user on the console.
;
fun D_NonVanillaPlayback (conditional bool lumpnum s32 feature ^uchar -> bool)
	if not conditional
		return FALSE
	endif

	strict = StrictDemos
	if strict
		return FALSE
	endif

	demo_file = IsDemoFile lumpnum
	if not demo_file
		I_Print  "Warning: WAD contains demo with a non-vanilla extension "
		I_Print2 "(%s)\n" feature
		return FALSE
	endif

	I_Print  "Warning: Playing back a demo file with a non-vanilla extension "
	I_Print2 "(%s). Use -strictdemos to disable this extension.\n" feature

	return TRUE
end

#private

; Called when a player leaves the game

fun PlayerQuitGame (num s32)
	M_StringCopy pl_exitmsg "Player # left the game" 80

	[pl_exitmsg 7] = iadd '1' (conv uchar num)

	D_ConsoleMessage pl_exitmsg

	[players num .in_game] = FALSE

	; Todo: check if it is sensible to do this:

	if [demorecording]
		G_CheckDemoFinished TRUE
	endif
end

#public

fun D_RunTic (set ^ticcmd_set_t)
	; Check for player quits.

	i s32 = 0

	loop while lt? i MAXPLAYERS
		if not [demoplayback]
			if and [players i .in_game] (not [set .ingame i])
				PlayerQuitGame i
			endif
		endif

		i = iadd i 1
	endloop

	[netcmds] = [addr-of set .cmds]

	; check that there are players in the game.  if not, we cannot
	; run a tic.

	if [advancedemo]
		D_DoAdvanceDemo
	endif

	G_Ticker
end

fun D_ConnectNetGame ()
	[netgame] = D_InitNetGame

	;
	; Start the game playing as though in a netgame with a single player.
	; This can also be used to play back single player netgame demos.
	;
	if M_ParmExists "-solo-net"
		[netgame] = TRUE
	endif
end

;
; D_CheckNetGame
; Works out player numbers among the net participants
;
fun D_CheckNetGame ()
	num_players s32 = 1

	if [netgame]
		[autostart] = TRUE
	endif

	pr = M_ParmExists "-record"
	pl = M_ParmExists "-longtics"
	ps = M_ParmExists "-shorttics"

	[lowres_turn] = or ps (and pr (not pl))

	i s32 = 0
	loop while lt? i MAXPLAYERS
		[players i .in_game] = lt? i num_players
		i = iadd i 1
	endloop

	D_StartNetGame

	I_Print3 "startskill %d  "     [startskill]
	I_Print3 "deathmatch: %d  "    [deathmatch]
	I_Print3 "startmap: %d  "      [startmap]
	I_Print3 "startepisode: %d\n"  [startepisode]

	I_Print3 "player %d "  (iadd [consoleplayer] 1)
	I_Print3 "of %d\n"     num_players
end

fun D_ConsolePlayerListener (-> ^soundmobj_t)
	mo = [players [consoleplayer] .mo]
	return raw-cast mo
end

fun D_ConsoleMessage (msg ^uchar)
	[players [consoleplayer] .message] = msg
end
