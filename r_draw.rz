;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

; parameters for R_DrawColumn
type DrawColumnParameters struct
	.source     ^[0]pixel_t
	.colormap   ^lighttable_t
	.x          s32
	.yl         s32
	.yh         s32
	.iscale     fixed_t
	.texturemid fixed_t
end

; parameters for R_DrawSpan
type DrawSpanParameters struct
	.source   ^[0]pixel_t
	.colormap ^lighttable_t
	.y        s32
	.x1       s32
	.x2       s32
	.xfrac    fixed_t
	.yfrac    fixed_t
	.xstep    fixed_t
	.ystep    fixed_t
end

zero-var dc DrawColumnParameters
zero-var ds DrawSpanParameters

;
; A column is a vertical slice/span from a wall texture that,
; given the DOOM style restrictions on the view orientation,
; will always have constant z depth.
;
; Thus a special case loop for very fast rendering can be used.
; It has also been used with Wolfenstein 3D.
;
fun R_DrawColumn ()
	count = isub [dc .yh] [dc .yl]

	; Zero length, column does not exceed a pixel.
	if neg? count
		return
	endif

	; Framebuffer destination address.
	; Use ylookup LUT to avoid multiply with ScreenWidth.
	dest = [ylookup [dc .yl]]
	dest = padd dest [viewwindowx] [dc .x]

	; Determine scaling,
	; which is the only mapping to be done.
	fracstep = [dc .iscale]
	fracofs  = imul (isub [dc .yl] [centery]) fracstep
	frac     = iadd [dc .texturemid] fracofs

	; Inner loop that does the actual texture mapping,
	; e.g. a DDA-like scaling.
	source   = [dc .source]
	colormap = [dc .colormap]

	loop
		; Re-map color indices from wall texture column
		; using a lighting/special effects LUT.
		row = ishr frac FRACBITS
		row = iand row 127

		pix    = [source   row]
		pix    = [colormap pix]
		[dest] = pix

		break if zero? count

		dest  = padd dest SCREENWIDTH
		frac  = iadd frac fracstep
		count = isub count 1
	endloop
end

;
; Spectre/Invisibility.
;
const FUZZTABLE = 50
const +FUZZ = SCREENWIDTH
const -FUZZ = (0 - SCREENWIDTH)

#private

var fuzzpos s32 = 0

rom-var fuzzoffset [FUZZTABLE]s32 =
	+FUZZ -FUZZ +FUZZ -FUZZ +FUZZ +FUZZ -FUZZ
	+FUZZ +FUZZ -FUZZ +FUZZ +FUZZ +FUZZ -FUZZ
	+FUZZ +FUZZ +FUZZ -FUZZ -FUZZ -FUZZ -FUZZ
	+FUZZ -FUZZ -FUZZ +FUZZ +FUZZ +FUZZ +FUZZ -FUZZ
	+FUZZ -FUZZ +FUZZ +FUZZ -FUZZ -FUZZ +FUZZ
	+FUZZ -FUZZ -FUZZ -FUZZ -FUZZ +FUZZ +FUZZ
	+FUZZ +FUZZ -FUZZ +FUZZ +FUZZ -FUZZ +FUZZ
end

#public

;
; Framebuffer postprocessing.
;
; Creates a fuzzy image by copying pixels from
; adjacent ones (vertically) and darkening them.
;
fun R_DrawFuzzColumn ()
	yl = [dc .yl]
	yh = [dc .yh]

	; Adjust borders. Low...
	yl = imax yl 1

	; ... and high.
	yh = imin yh (isub [viewheight] 2)

	count = isub yh yl

	; Zero length.
	if neg? count
		return
	endif

	dest = [ylookup yl]
	dest = padd dest [viewwindowx] [dc .x]

	; Uses colormap #6 (of 0-31) to darken pixels

	colormap = [addr-of [colormaps] 6]

	fuzz = [fuzzpos]

	loop
		; Lookup framebuffer, and retrieve a pixel that is either one
		; row above or below of the current one.
		source = padd dest [fuzzoffset fuzz]

		pix    = [source]
		pix    = [colormap pix]
		[dest] = pix

		break if zero? count

		dest  = padd dest SCREENWIDTH
		count = isub count 1

		; Clamp table lookup index.
		fuzz = iadd fuzz 1
		if ge? fuzz FUZZTABLE
			fuzz = 0
		endif
	endloop

	[fuzzpos] = fuzz
end

;
; R_DrawSpan
;
; With DOOM style restrictions on view orientation,
; the floors and ceilings consist of horizontal slices
; or spans with constant z depth.
;
; However, rotation around the world z axis is possible,
; thus this mapping, while simpler and faster than
; perspective correct texture mapping, has to traverse
; the texture at an angle in all but a few cases.
;
; In consequence, flats are not stored by column (like walls),
; and the inner loop has to step in texture space u and v.
;
fun R_DrawSpan ()
	; Pack position and step variables into a single 32-bit integer,
	; with x in the top 16 bits and y in the bottom 16 bits.  For
	; each 16-bit part, the top 6 bits are the integer part and the
	; bottom 10 bits are the fractional part of the pixel position.

	posx = iand (ishl [ds .xfrac] 10) 0xffff0000
	posy = iand (ishr [ds .yfrac]  6) 0x0000ffff
	position u32 = ior posx posy

	stepx = iand (ishl [ds .xstep] 10) 0xffff0000
	stepy = iand (ishr [ds .ystep]  6) 0x0000ffff
	step u32 = ior stepx stepy

	dest = [ylookup [ds .y]]
	dest = padd dest [viewwindowx] [ds .x1]

	; the X range is inclusive, do at least one loop
	count = isub [ds .x2] [ds .x1]

	source   = [ds .source]
	colormap = [ds .colormap]

	loop
		; Calculate current texture index in u,v.
		spotx = ishr position 26
		spoty = iand (ishr position 4) 0x0fc0
		spot  = ior spotx spoty

		; Lookup pixel from flat texture tile,
		; re-index using light/colormap.
		pix    = [source  spot]
		pix    = [colormap pix]
		[dest] = pix

		break if zero? count

		dest     = padd dest 1
		position = iadd position step
		count    = isub count 1
	endloop
end

;
; R_InitBuffer
;
; Creats lookup tables that avoid multiplies and other hassles
; for getting the framebuffer address of a pixel to draw.
;
fun R_InitBuffer (width s32 height s32)
	[viewwidth]   = width
	[viewheight]  = height

	view_x = isub SCREENWIDTH width
	view_x = ishr view_x 1

	[viewwindowx] = view_x
	[viewwindowy] = 0

	if lt? width SCREENWIDTH
		view_y = isub (SCREENHEIGHT - ST_HEIGHT) height
		view_y = ishr view_y 1

		[viewwindowy] = view_y
	endif

	i s32 = 0
	loop while lt? i height
		offset = iadd [viewwindowy] i
		offset = imul offset SCREENWIDTH

		[ylookup i] = padd [videoBuffer] offset
		i = iadd i 1
	endloop
end
