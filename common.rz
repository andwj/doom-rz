;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;


;;
;; Types
;;

type byte    u8
type pixel_t u8

const INT_MIN = -0x80000000
const INT_MAX =  0x7fffffff

;;
;; Strings
;;
type uchar u8

const PATH_SIZE = 1024

;;
;; Angles
;;
type angle_t u32

const ANG0    = 0x00000000
const ANG45   = 0x20000000
const ANG90   = 0x40000000
const ANG180  = 0x80000000
const ANG270  = 0xc0000000
const ANG315  = 0xe0000000
const ANG_MAX = 0xffffffff

const ANGLEFINESHIFT = 19

const FINEANGLES = 8192
const FINEMASK   = (FINEANGLES - 1)

;;
;; Fixed point, 32bit as 16.16.
;;
const FRACBITS = 16
const FRACUNIT = (1 << FRACBITS)
const FRAC_MIN = -0x80000000
const FRAC_MAX =  0x7fffffff

type fixed_t s32

inline-fun FixedMul (a fixed_t b fixed_t -> fixed_t)
	a2 = conv s64 a
	b2 = conv s64 b
	res = ishr (imul a2 b2) FRACBITS
	return conv fixed_t res
end

fun FixedDiv (a fixed_t b fixed_t -> fixed_t)
	a1 = iabs a
	b1 = iabs b
	if ge? (ishr a1 14) b1
		is_neg = neg? (ixor a b)
		val fixed_t = sel-if is_neg FRAC_MIN FRAC_MAX
		return val
	endif
	a2 = conv s64 a
	b2 = conv s64 b
	res = idivt (ishl a2 FRACBITS) b2
	return conv fixed_t res
end

;;
;; Memory I/O
;;
type mem_rel_t s32

const MEM_SEEK_SET = 0
const MEM_SEEK_CUR = 1
const MEM_SEEK_END = 2

;;
;; Bounding box
;;
const BOXTOP    = 0
const BOXBOTTOM = 1
const BOXLEFT   = 2
const BOXRIGHT  = 3

const ONFLOORZ   = FRAC_MIN
const ONCEILINGZ = FRAC_MAX

;;
;; Zone Memory
;; PU = purge tags.
;;

const PU_FREE   = 1   ; a free block
const PU_CACHE  = 2   ; purgeable whenever needed
const PU_STATIC = 3   ; static entire execution time
const PU_LEVEL  = 4   ; static until level exited

;;
;; Thinkers
;;
type actionf_p0 fun ()
type actionf_p1 fun (t ^thinker_t)
type actionf_p2 fun (player ^raw-mem psp ^raw-mem)

type actionf_t union
	.acp0 ^actionf_p0  ; only used to check for NULL
	.acp1 ^actionf_p1  ; used for most stuff
	.acp2 ^actionf_p2  ; only used for player weapons
end

type think_func_e s32

const THINK_INVALID = 0
const THINK_mobj    = 1
const THINK_ceil    = 2
const THINK_floor   = 3
const THINK_plat    = 4
const THINK_door    = 5
const THINK_flicker = 6
const THINK_glow    = 7
const THINK_flash   = 8
const THINK_strobe  = 9

; a doubly linked list of actors.
type thinker_t struct
	.prev   ^thinker_t
	.next   ^thinker_t
	.func    think_func_e
	.remove  s32
end

;;
;; Global parameters/defines.
;;

; The "mission" controls what game we are playing.
type GameMission_t s32

const mi_doom     = 0  ; Doom 1
const mi_doom2    = 1  ; Doom 2
const mi_tnt      = 2  ; Final Doom: TNT: Evilution
const mi_plutonia = 3  ; Final Doom: The Plutonia Experiment
const mi_chex     = 4  ; Chex Quest
const mi_hacx     = 5  ; HacX
const mi_unknown  = 6

; the "mode" allows more accurate specification of the game mode we are
; in: eg. shareware vs. registered.  So doom1.wad and doom.wad are the
; same mission, but a different mode.
type GameMode_t s32

const shareware    = 0  ; Doom shareware
const registered   = 1  ; Doom registered
const commercial   = 2  ; Doom II
const retail       = 3  ; Ultimate Doom
const unknown_mode = 4

; what version are we emulating?
type GameVersion_t s32

const exe_doom_1_2    = 0  ; Doom 1.2: shareware and registered
const exe_doom_1_666  = 1  ; Doom 1.666: for shareware, registered and commercial
const exe_doom_1_7    = 2  ; Doom 1.7/1.7a: "
const exe_doom_1_8    = 3  ; Doom 1.8: "
const exe_doom_1_9    = 4  ; Doom 1.9: "
const exe_hacx        = 5  ; Hacx
const exe_ultimate    = 6  ; Ultimate Doom (retail)
const exe_final       = 7  ; Final Doom
const exe_final2      = 8  ; Final Doom (alternate exe)
const exe_chex        = 9  ; Chex Quest executable (based on Final Doom)

; what IWAD variant are we using?
type GameVariant_t s32

const gv_vanilla    = 0  ; Vanilla Doom
const gv_freedoom   = 1  ; FreeDoom: Phase 1 + 2
const gv_freedm     = 2  ; FreeDM
const gv_bfgedition = 3  ; Doom Classic (Doom 3: BFG Edition)
const gv_chex2      = 4  ; Chex Quest 2

; skill level.
type skill_t s32

const sk_noitems   = -1  ; the "-skill 0" hack
const sk_baby      = 0
const sk_easy      = 1
const sk_medium    = 2
const sk_hard      = 3
const sk_nightmare = 4

; DOOM version
const DOOM_VERSION = 109

; Version code for cph's longtics hack ("v1.91")
const DOOM_191_VERSION = 111

; The maximum number of players, multiplayer/networking.
const MAXPLAYERS = 4

; The current state of the game: whether we are
; playing, gazing at the intermission screen,
; the game final animation, or a demo.
type gamestate_t s32

const GS_LEVEL        = 0
const GS_INTERMISSION = 1
const GS_FINALE       = 2
const GS_DEMOSCREEN   = 3

type gameaction_t s32

const ga_nothing     = 0
const ga_loadlevel   = 1
const ga_newgame     = 2
const ga_loadgame    = 3
const ga_savegame    = 4
const ga_playdemo    = 5
const ga_completed   = 6
const ga_victory     = 7
const ga_worlddone   = 8
const ga_screenshot  = 9

; Key cards.
type card_t s32

const it_bluecard     = 0
const it_yellowcard   = 1
const it_redcard      = 2
const it_blueskull    = 3
const it_yellowskull  = 4
const it_redskull     = 5
const NUMCARDS        = 6

; The defined weapons,
type weapontype_t s32

const wp_fist          = 0
const wp_pistol        = 1
const wp_shotgun       = 2
const wp_chaingun      = 3
const wp_missile       = 4
const wp_plasma        = 5
const wp_bfg           = 6
const wp_chainsaw      = 7
const wp_supershotgun  = 8
const NUMWEAPONS       = 9
const wp_nochange      = 10  ; No pending weapon change.

; Ammunition types defined.
type ammotype_t s32

const am_clip   = 0   ; Pistol / chaingun ammo.
const am_shell  = 1   ; Shotgun / double barreled shotgun.
const am_cell   = 2   ; Plasma rifle, BFG.
const am_misl   = 3   ; Missile launcher.
const NUMAMMO   = 4
const am_noammo = 5   ; Unlimited for chainsaw / fist.

; Power up artifacts.
type powertype_t s32

const pw_invulnerability = 0
const pw_strength        = 1
const pw_invisibility    = 2
const pw_ironfeet        = 3
const pw_allmap          = 4
const pw_infrared        = 5
const NUMPOWERS          = 6

; plane movement values
type result_e s32

const RES_ok        = 0
const RES_crushed   = 1
const RES_pastdest  = 2
