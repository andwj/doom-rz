;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;

extern-fun D_ForceWipe ()
extern-fun G_WorldNext ()

#private

const TEXTSPEED = 3
const TEXTWAIT  = 250

type finalestage_t s32

const F_STAGE_TEXT  = 0
const F_STAGE_ART   = 1
const F_STAGE_CAST  = 2

type FinaleState struct
	.stage  finalestage_t
	.count  s32
	.text  ^uchar
	.flat  ^uchar
	.shot   s32
end

zero-var finale FinaleState

type textscreen_t struct
	.mission  GameMission_t
	.episode  s32
	.map      s32
	.flat    ^uchar
	.text    ^uchar
end

const NUMTEXTSCREENS = 28

rom-var textscreens [NUMTEXTSCREENS]textscreen_t =
	{ mi_doom      1  8   "FLOOR4_8"   E1TEXT }
	{ mi_doom      2  8   "SFLR6_1"    E2TEXT }
	{ mi_doom      3  8   "MFLR8_4"    E3TEXT }
	{ mi_doom      4  8   "MFLR8_3"    E4TEXT }

	{ mi_doom2     1  6   "SLIME16"    C1TEXT }
	{ mi_doom2     1  11  "RROCK14"    C2TEXT }
	{ mi_doom2     1  20  "RROCK07"    C3TEXT }
	{ mi_doom2     1  30  "RROCK17"    C4TEXT }
	{ mi_doom2     1  15  "RROCK13"    C5TEXT }
	{ mi_doom2     1  31  "RROCK19"    C6TEXT }

	{ mi_tnt       1  6   "SLIME16"    T1TEXT }
	{ mi_tnt       1  11  "RROCK14"    T2TEXT }
	{ mi_tnt       1  20  "RROCK07"    T3TEXT }
	{ mi_tnt       1  30  "RROCK17"    T4TEXT }
	{ mi_tnt       1  15  "RROCK13"    T5TEXT }
	{ mi_tnt       1  31  "RROCK19"    T6TEXT }

	{ mi_plutonia  1  6   "SLIME16"    P1TEXT }
	{ mi_plutonia  1  11  "RROCK14"    P2TEXT }
	{ mi_plutonia  1  20  "RROCK07"    P3TEXT }
	{ mi_plutonia  1  30  "RROCK17"    P4TEXT }
	{ mi_plutonia  1  15  "RROCK13"    P5TEXT }
	{ mi_plutonia  1  31  "RROCK19"    P6TEXT }

	{ mi_hacx      1  6   "CEIL4_3"    HACX_TEXT1 }
	{ mi_hacx      1  11  "CEIL5_2"    HACX_TEXT2 }
	{ mi_hacx      1  20  "CEIL5_2"    HACX_TEXT3 }
	{ mi_hacx      1  15  "CEIL5_2"    HACX_TEXT5 }
	{ mi_hacx      1  31  "CEIL5_2"    HACX_TEXT5 }

	{ mi_chex      1  5   "FLAT14"     CHEX_END_TEXT }
end

type castinfo_t struct
	.name  ^uchar
	.type   mobjtype_t
end

const CC_ZOMBIE   = "ZOMBIEMAN"
const CC_SHOTGUN  = "SHOTGUN GUY"
const CC_HEAVY    = "HEAVY WEAPON DUDE"
const CC_IMP      = "IMP"
const CC_DEMON    = "DEMON"
const CC_LOST     = "LOST SOUL"
const CC_CACO     = "CACODEMON"
const CC_HELL     = "HELL KNIGHT"
const CC_BARON    = "BARON OF HELL"
const CC_ARACH    = "ARACHNOTRON"
const CC_PAIN     = "PAIN ELEMENTAL"
const CC_REVEN    = "REVENANT"
const CC_MANCU    = "MANCUBUS"
const CC_ARCH     = "ARCH-VILE"
const CC_SPIDER   = "THE SPIDER MASTERMIND"
const CC_CYBER    = "THE CYBERDEMON"
const CC_HERO     = "OUR HERO"

rom-var castorder [18]castinfo_t =
	{ CC_ZOMBIE  MT_POSSESSED }
	{ CC_SHOTGUN MT_SHOTGUY   }
	{ CC_HEAVY   MT_CHAINGUY  }
	{ CC_IMP     MT_TROOP     }
	{ CC_DEMON   MT_SERGEANT  }
	{ CC_LOST    MT_SKULL     }
	{ CC_CACO    MT_HEAD      }
	{ CC_HELL    MT_KNIGHT    }
	{ CC_BARON   MT_BRUISER   }
	{ CC_ARACH   MT_BABY      }
	{ CC_PAIN    MT_PAIN      }
	{ CC_REVEN   MT_UNDEAD    }
	{ CC_MANCU   MT_FATSO     }
	{ CC_ARCH    MT_VILE      }
	{ CC_SPIDER  MT_SPIDER    }
	{ CC_CYBER   MT_CYBORG    }
	{ CC_HERO    MT_PLAYER    }

	{ NULL 0 }  ; terminator
end

#public

;
; F_StartFinale
;
fun F_StartFinale ()
	if [doom1ish]
		S_ChangeMusic mus_victor TRUE
	else
		S_ChangeMusic mus_read_m TRUE
	endif

	; Find the right screen and set the text and background
	epi = [gameepisode]
	if not [doom1ish]
		epi = 1
	endif

	screen ^textscreen_t = NULL

	i s32 = 0
	loop while lt? i NUMTEXTSCREENS
		screen = [addr-of textscreens i]

		if eq? [gamemission] [screen .mission]
			if eq? epi [screen .episode]
				if eq? [gamemap] [screen .map]
					; ok!
					break
				endif
			endif
		endif

		i = iadd i 1
	endloop

	[finale .stage] = F_STAGE_TEXT
	[finale .count] = 0
	[finale .text]  = [screen .text]
	[finale .flat]  = [screen .flat]
end

fun F_Responder (ev ^event_t -> bool)
	if eq? [finale .stage] F_STAGE_CAST
		return F_CastResponder ev
	endif

	return FALSE
end

;
; F_Ticker
;
fun F_Ticker ()
	; check for skipping
	if eq? [gamemode] commercial
		if gt? [finale .count] 50
			if F_AnyPlayerButton
				if eq? [gamemap] 30
					F_StartCast
				else
					; go on to the next level
					G_WorldNext
				endif
			endif
		endif
	endif

	; advance animation
	[finale .count] = iadd [finale .count] 1

	if eq? [finale .stage] F_STAGE_ART
		return
	endif

	if eq? [finale .stage] F_STAGE_CAST
		F_CastTicker
		return
	endif

	if ne? [gamemode] commercial
		; time to move to the art screen?
		len   = M_StrLen [finale .text]

		delay = conv s32 len
		delay = imul delay TEXTSPEED
		delay = iadd delay TEXTWAIT

		if gt? [finale .count] delay
			[finale .stage] = F_STAGE_ART
			[finale .count] = 0

			D_ForceWipe

			if eq? [gameepisode] 3
				S_StartMusic mus_bunny
			endif
		endif
	endif
end

fun F_AnyPlayerButton (-> bool)
	i s32 = 0

	loop while lt? i MAXPLAYERS
		p = [addr-of players i]
		if some? [p .cmd .buttons]
			return TRUE
		endif
		i = iadd i 1
	endloop

	return FALSE
end

;
; F_Drawer
;
fun F_Drawer ()
	if eq? [finale .stage] F_STAGE_TEXT
		V_FlatBackground [finale .flat]
		F_TextWrite

	elif eq? [finale .stage] F_STAGE_ART
		F_ArtScreenDrawer

	elif eq? [finale .stage] F_STAGE_CAST
		F_CastDrawer
	endif
end

;----------------------------------------------------------------------

#private

;
; Final DOOM 2 animation.
; Casting by id Software, in order of appearance.
;

type CastScreenState struct
	.info   ^mobjinfo_t  ; entry in the mobjinfo[] table
	.name   ^uchar       ; name to draw underneath

	.state  ^state_t     ; current state in states[] table
	.tics    s32         ; countdown for current state

	.who     s32         ; index into castorder
	.frames  s32         ; number of frames shown so far

	.death   bool        ; in death frames
	.attack  bool        ; in attack frames
	.melee   bool        ; attack is melee instead of missile
end

zero-var cast CastScreenState

;
; F_StartCast
;
fun F_StartCast ()
	[finale .stage] = F_STAGE_CAST
	[finale .count] = 0

	F_ChangeCast 0
	S_ChangeMusic mus_evil TRUE
	D_ForceWipe
end

fun F_ChangeCast (who s32)
	mtype = [castorder who .type]
	info  = P_GetMobjInfo mtype

	[cast .who]    = who
	[cast .info]   = info
	[cast .name]   = [castorder who .name]
	[cast .frames] = 0

	[cast .state]  = P_GetState [info .seestate]
	[cast .tics]   = [[cast .state] .tics]

	[cast .death]  = FALSE
	[cast .attack] = FALSE
end

;
; F_CastTicker
;
fun F_CastTicker ()
	[cast .tics] = isub [cast .tics] 1

	if pos? [cast .tics]
		; not time to change state yet
		return
	endif

	info = [cast .info]
	st   = [cast .state]

	is_dead = or (eq? [st .tics] -1) (eq? [st .next] S_NULL)

	if is_dead
		; switch from deathstate to next monster
		who = iadd [cast .who] 1
		if null? [castorder who .name]
			who = 0
		endif

		F_ChangeCast who

		; play the sight sound, if it has one
		info = [cast .info]
		if some? [info .seesound]
			S_StartSound NULL [info .seesound]
		endif

		return
	endif

	; advance to next state in animation

	if eq? st (P_GetState S_PLAY_ATK1)
		jump stopattack  ; Oh, gross hack!
	endif

	stnum = [st .next]

	[cast .state]  = P_GetState stnum
	[cast .frames] = iadd [cast .frames] 1

	; sound hacks....
	F_CastSound stnum

	if eq? [cast .frames] 12
		; go into attack frame
		[cast .attack] = TRUE

		if [cast .melee]
			atk_num = [info .meleestate]
		else
			atk_num = [info .missilestate]
		endif

		if eq? atk_num S_NULL
			atk_num = imax [info .meleestate] [info .missilestate]
		endif

		[cast .state] = P_GetState atk_num
		[cast .melee] = not [cast .melee]
	endif

	if [cast .attack]
		is_time  = eq? [cast .frames] 24
		is_sight = eq? [cast .state] (P_GetState [info .seestate])

		if or is_time is_sight
			stopattack:

			[cast .attack] = FALSE
			[cast .frames] = 0
			[cast .state]  = P_GetState [info .seestate]
		endif
	endif

	st = [cast .state]
	[cast .tics] = [st .tics]

	if neg? [cast .tics]
		[cast .tics] = 15
	endif
end

fun F_CastSound (stnum statenum_t)
	; IDEA: compare against the action function instead of state number

	if eq? stnum S_PLAY_ATK1
		S_StartSound NULL sfx_dshtgn
	elif eq? stnum S_POSS_ATK2
		S_StartSound NULL sfx_pistol
	elif eq? stnum S_SPOS_ATK2
		S_StartSound NULL sfx_shotgn
	elif eq? stnum S_VILE_ATK2
		S_StartSound NULL sfx_vilatk
	elif eq? stnum S_SKEL_FIST2
		S_StartSound NULL sfx_skeswg
	elif eq? stnum S_SKEL_FIST4
		S_StartSound NULL sfx_skepch
	elif eq? stnum S_SKEL_MISS2
		S_StartSound NULL sfx_skeatk
	elif eq? stnum S_FATT_ATK2 S_FATT_ATK5 S_FATT_ATK8
		S_StartSound NULL sfx_firsht
	elif eq? stnum S_CPOS_ATK2 S_CPOS_ATK3 S_CPOS_ATK4
		S_StartSound NULL sfx_shotgn
	elif eq? stnum S_TROO_ATK3
		S_StartSound NULL sfx_claw
	elif eq? stnum S_SARG_ATK2
		S_StartSound NULL sfx_sgtatk
	elif eq? stnum S_BOSS_ATK2 S_BOS2_ATK2 S_HEAD_ATK2
		S_StartSound NULL sfx_firsht
	elif eq? stnum S_SKULL_ATK2
		S_StartSound NULL sfx_sklatk
	elif eq? stnum S_SPID_ATK2 S_SPID_ATK3
		S_StartSound NULL sfx_shotgn
	elif eq? stnum S_BSPI_ATK2
		S_StartSound NULL sfx_plasma
	elif eq? stnum S_CYBER_ATK2 S_CYBER_ATK4 S_CYBER_ATK6
		S_StartSound NULL sfx_rlaunc
	elif eq? stnum S_PAIN_ATK3
		S_StartSound NULL sfx_sklatk
	endif
end

;
; F_CastResponder
;
fun F_CastResponder (ev ^event_t -> bool)
	; only care about key presses
	if ne? [ev .type] ev_keydown
		return FALSE
	endif

	info = [cast .info]

	if [cast .death]
		; already in dying frames
	else
		; go into death frame
		[cast .state]  = P_GetState [info .deathstate]
		[cast .tics]   = [[cast .state] .tics]
		[cast .frames] = 0

		[cast .death]  = TRUE
		[cast .attack] = FALSE

		if some? [info .deathsound]
			S_StartSound NULL [info .deathsound]
		endif
	endif

	return TRUE
end

;
; F_CastDrawer
;
fun F_CastDrawer ()
	; erase the entire screen to a background
	V_DrawPatchName 0 0 "BOSSBACK"

	; draw the monster's name
	width = HU_TextWidth [cast .name]
	cx    = isub  SCREENWIDTH width
	cx    = idivt cx 2

	HU_DrawText cx 180 [cast .name]

	; draw the current frame in the middle of the screen
	lump = R_GetCastSprite [cast .state]

	flip = neg? lump
	lump = iabs lump

	patch ^patch_t = W_CacheLumpNum lump

	sx s32 = (SCREENWIDTH / 2)
	sy s32 = 170

	if flip
		V_DrawPatchFlipped sx sy patch
	else
		V_DrawPatch sx sy patch
	endif
end

;----------------------------------------------------------------------

fun F_ArtScreenDrawer ()
	lumpname ^uchar = "CREDIT"

	if eq? [gameepisode] 1
		if lt? [gameversion] exe_ultimate
			lumpname = "HELP2"
		endif

	elif eq? [gameepisode] 2
		lumpname = "VICTORY2"

	elif eq? [gameepisode] 3
		F_BunnyScroll
		return

	elif eq? [gameepisode] 4
		lumpname = "ENDPIC"

	else
		; invalid episode
		return
	endif

	V_DrawPatchName 0 0 lumpname
end

const THEEND_X = ((SCREENWIDTH  - (13 * 8)) / 2)
const THEEND_Y = ((SCREENHEIGHT - ( 8 * 8)) / 2)

;
; F_BunnyScroll
;
fun F_BunnyScroll ()
	scrolled = isub [finale .count] 230
	scrolled = idivt scrolled 2
	scrolled = isub SCREENWIDTH scrolled
	scrolled = imin scrolled SCREENWIDTH
	scrolled = imax scrolled 0

	; note that PFUB2 is on left side, PFUB1 is on right side
	x1 = isub 0 scrolled
	V_DrawPatchName x1 0 "PFUB2"

	x2 = isub SCREENWIDTH scrolled
	V_DrawPatchName x1 0 "PFUB1"

	; time for the shot-up END graphic?

	if lt? [finale .count] 1130
		return
	endif

	if lt? [finale .count] 1180
		[finale .shot] = 0
		return
	endif

	shot = isub  [finale .count] 1180
	shot = idivt shot 5
	shot = imin  shot 6

	if gt? shot [finale .shot]
		S_StartSound NULL sfx_pistol
		[finale .shot] = shot
	endif

	picname = stack-var [32]uchar
	M_FormatNum picname 32 "END%d" shot

	V_DrawPatchName THEEND_X THEEND_Y picname
end

;
; F_TextWrite
;
fun F_TextWrite ()
	; draw some of the text onto the screen
	count = isub  [finale .count] 10
	count = idivt count TEXTSPEED
	count = imax  count 0

	s = [finale .text]

	HU_DrawTextLimit 10 10 s count
end

