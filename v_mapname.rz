;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

;
; Doom I
;
const HUSTR_E1M1  = "E1M1: Hangar"
const HUSTR_E1M2  = "E1M2: Nuclear Plant"
const HUSTR_E1M3  = "E1M3: Toxin Refinery"
const HUSTR_E1M4  = "E1M4: Command Control"
const HUSTR_E1M5  = "E1M5: Phobos Lab"
const HUSTR_E1M6  = "E1M6: Central Processing"
const HUSTR_E1M7  = "E1M7: Computer Station"
const HUSTR_E1M8  = "E1M8: Phobos Anomaly"
const HUSTR_E1M9  = "E1M9: Military Base"

const HUSTR_E2M1  = "E2M1: Deimos Anomaly"
const HUSTR_E2M2  = "E2M2: Containment Area"
const HUSTR_E2M3  = "E2M3: Refinery"
const HUSTR_E2M4  = "E2M4: Deimos Lab"
const HUSTR_E2M5  = "E2M5: Command Center"
const HUSTR_E2M6  = "E2M6: Halls of the Damned"
const HUSTR_E2M7  = "E2M7: Spawning Vats"
const HUSTR_E2M8  = "E2M8: Tower of Babel"
const HUSTR_E2M9  = "E2M9: Fortress of Mystery"

const HUSTR_E3M1  = "E3M1: Hell Keep"
const HUSTR_E3M2  = "E3M2: Slough of Despair"
const HUSTR_E3M3  = "E3M3: Pandemonium"
const HUSTR_E3M4  = "E3M4: House of Pain"
const HUSTR_E3M5  = "E3M5: Unholy Cathedral"
const HUSTR_E3M6  = "E3M6: Mt. Erebus"
const HUSTR_E3M7  = "E3M7: Limbo"
const HUSTR_E3M8  = "E3M8: Dis"
const HUSTR_E3M9  = "E3M9: Warrens"

const HUSTR_E4M1  = "E4M1: Hell Beneath"
const HUSTR_E4M2  = "E4M2: Perfect Hatred"
const HUSTR_E4M3  = "E4M3: Sever The Wicked"
const HUSTR_E4M4  = "E4M4: Unruly Evil"
const HUSTR_E4M5  = "E4M5: They Will Repent"
const HUSTR_E4M6  = "E4M6: Against Thee Wickedly"
const HUSTR_E4M7  = "E4M7: And Hell Followed"
const HUSTR_E4M8  = "E4M8: Unto The Cruel"
const HUSTR_E4M9  = "E4M9: Fear"

const HUSTR_E5M1  = "E5M1"
const HUSTR_E5M2  = "E5M2"
const HUSTR_E5M3  = "E5M3"
const HUSTR_E5M4  = "E5M4"
const HUSTR_E5M5  = "E5M5"
const HUSTR_E5M6  = "E5M6"
const HUSTR_E5M7  = "E5M7"
const HUSTR_E5M8  = "E5M8"
const HUSTR_E5M9  = "E5M9"

rom-var mapnames_doom [5][9]^uchar =
	array
		HUSTR_E1M1 HUSTR_E1M2 HUSTR_E1M3
		HUSTR_E1M4 HUSTR_E1M5 HUSTR_E1M6
		HUSTR_E1M7 HUSTR_E1M8 HUSTR_E1M9
	endarray

	array
		HUSTR_E2M1 HUSTR_E2M2 HUSTR_E2M3
		HUSTR_E2M4 HUSTR_E2M5 HUSTR_E2M6
		HUSTR_E2M7 HUSTR_E2M8 HUSTR_E2M9
	endarray

	array
		HUSTR_E3M1 HUSTR_E3M2 HUSTR_E3M3
		HUSTR_E3M4 HUSTR_E3M5 HUSTR_E3M6
		HUSTR_E3M7 HUSTR_E3M8 HUSTR_E3M9
	endarray

	array
		HUSTR_E4M1 HUSTR_E4M2 HUSTR_E4M3
		HUSTR_E4M4 HUSTR_E4M5 HUSTR_E4M6
		HUSTR_E4M7 HUSTR_E4M8 HUSTR_E4M9
	endarray

	array
		HUSTR_E5M1 HUSTR_E5M2 HUSTR_E5M3
		HUSTR_E5M4 HUSTR_E5M5 HUSTR_E5M6
		HUSTR_E5M7 HUSTR_E5M8 HUSTR_E5M9
	endarray
end

;
; Chex Quest I
;
const CHEX1_E1M1  = "E1M1: Landing Zone"
const CHEX1_E1M2  = "E1M2: Storage Facility"
const CHEX1_E1M3  = "E1M3: Laboratory"
const CHEX1_E1M4  = "E1M4: Arboretum"
const CHEX1_E1M5  = "E1M5: Caverns of Bazoik"

rom-var mapnames_chex1 [5]^uchar =
	CHEX1_E1M1
	CHEX1_E1M2
	CHEX1_E1M3
	CHEX1_E1M4
	CHEX1_E1M5
end

;
; Chex Quest II
;
const CHEX2_E1M1  = "E1M1: Spaceport"
const CHEX2_E1M2  = "E1M2: Cinema"
const CHEX2_E1M3  = "E1M3: Chex Museum"
const CHEX2_E1M4  = "E1M4: City Streets"
const CHEX2_E1M5  = "E1M5: Sewer System"

rom-var mapnames_chex2 [5]^uchar =
	CHEX2_E1M1
	CHEX2_E1M2
	CHEX2_E1M3
	CHEX2_E1M4
	CHEX2_E1M5
end

;
; Doom II
;
const HUSTR_1   = "level 1: entryway"
const HUSTR_2   = "level 2: underhalls"
const HUSTR_3   = "level 3: the gantlet"
const HUSTR_4   = "level 4: the focus"
const HUSTR_5   = "level 5: the waste tunnels"
const HUSTR_6   = "level 6: the crusher"
const HUSTR_7   = "level 7: dead simple"
const HUSTR_8   = "level 8: tricks and traps"
const HUSTR_9   = "level 9: the pit"
const HUSTR_10  = "level 10: refueling base"
const HUSTR_11  = "level 11: 'o' of destruction!"

const HUSTR_12  = "level 12: the factory"
const HUSTR_13  = "level 13: downtown"
const HUSTR_14  = "level 14: the inmost dens"
const HUSTR_15  = "level 15: industrial zone"
const HUSTR_16  = "level 16: suburbs"
const HUSTR_17  = "level 17: tenements"
const HUSTR_18  = "level 18: the courtyard"
const HUSTR_19  = "level 19: the citadel"
const HUSTR_20  = "level 20: gotcha!"

const HUSTR_21  = "level 21: nirvana"
const HUSTR_22  = "level 22: the catacombs"
const HUSTR_23  = "level 23: barrels o' fun"
const HUSTR_24  = "level 24: the chasm"
const HUSTR_25  = "level 25: bloodfalls"
const HUSTR_26  = "level 26: the abandoned mines"
const HUSTR_27  = "level 27: monster condo"
const HUSTR_28  = "level 28: the spirit world"
const HUSTR_29  = "level 29: the living end"
const HUSTR_30  = "level 30: icon of sin"

const HUSTR_31  = "level 31: wolfenstein"
const HUSTR_32  = "level 32: grosse"
const HUSTR_33  = "level 33: betray"
const HUSTR_34  = "level 34"
const HUSTR_35  = "level 35"

rom-var mapnames_doom2 [35]^uchar =
	HUSTR_1  HUSTR_2  HUSTR_3  HUSTR_4  HUSTR_5
	HUSTR_6  HUSTR_7  HUSTR_8  HUSTR_9  HUSTR_10
	HUSTR_11 HUSTR_12 HUSTR_13 HUSTR_14 HUSTR_15
	HUSTR_16 HUSTR_17 HUSTR_18 HUSTR_19 HUSTR_20
	HUSTR_21 HUSTR_22 HUSTR_23 HUSTR_24 HUSTR_25
	HUSTR_26 HUSTR_27 HUSTR_28 HUSTR_29 HUSTR_30
	HUSTR_31 HUSTR_32 HUSTR_33 HUSTR_34 HUSTR_35
end

;
; Plutonia Experiment
;
const PHUSTR_1   = "level 1: congo"
const PHUSTR_2   = "level 2: well of souls"
const PHUSTR_3   = "level 3: aztec"
const PHUSTR_4   = "level 4: caged"
const PHUSTR_5   = "level 5: ghost town"
const PHUSTR_6   = "level 6: baron's lair"
const PHUSTR_7   = "level 7: caughtyard"
const PHUSTR_8   = "level 8: realm"
const PHUSTR_9   = "level 9: abattoire"
const PHUSTR_10  = "level 10: onslaught"
const PHUSTR_11  = "level 11: hunted"

const PHUSTR_12  = "level 12: speed"
const PHUSTR_13  = "level 13: the crypt"
const PHUSTR_14  = "level 14: genesis"
const PHUSTR_15  = "level 15: the twilight"
const PHUSTR_16  = "level 16: the omen"
const PHUSTR_17  = "level 17: compound"
const PHUSTR_18  = "level 18: neurosphere"
const PHUSTR_19  = "level 19: nme"
const PHUSTR_20  = "level 20: the death domain"

const PHUSTR_21  = "level 21: slayer"
const PHUSTR_22  = "level 22: impossible mission"
const PHUSTR_23  = "level 23: tombstone"
const PHUSTR_24  = "level 24: the final frontier"
const PHUSTR_25  = "level 25: the temple of darkness"
const PHUSTR_26  = "level 26: bunker"
const PHUSTR_27  = "level 27: anti-christ"
const PHUSTR_28  = "level 28: the sewers"
const PHUSTR_29  = "level 29: odyssey of noises"
const PHUSTR_30  = "level 30: the gateway of hell"

const PHUSTR_31  = "level 31: cyberden"
const PHUSTR_32  = "level 32: go 2 it"
const PHUSTR_33  = "level 33"
const PHUSTR_34  = "level 34"
const PHUSTR_35  = "level 35"

rom-var mapnames_plutonia [35]^uchar =
	PHUSTR_1  PHUSTR_2  PHUSTR_3  PHUSTR_4  PHUSTR_5
	PHUSTR_6  PHUSTR_7  PHUSTR_8  PHUSTR_9  PHUSTR_10
	PHUSTR_11 PHUSTR_12 PHUSTR_13 PHUSTR_14 PHUSTR_15
	PHUSTR_16 PHUSTR_17 PHUSTR_18 PHUSTR_19 PHUSTR_20
	PHUSTR_21 PHUSTR_22 PHUSTR_23 PHUSTR_24 PHUSTR_25
	PHUSTR_26 PHUSTR_27 PHUSTR_28 PHUSTR_29 PHUSTR_30
	PHUSTR_31 PHUSTR_32 PHUSTR_33 PHUSTR_34 PHUSTR_35
end

;
; TNT Evilution
;
const THUSTR_1   = "level 1: system control"
const THUSTR_2   = "level 2: human bbq"
const THUSTR_3   = "level 3: power control"
const THUSTR_4   = "level 4: wormhole"
const THUSTR_5   = "level 5: hanger"
const THUSTR_6   = "level 6: open season"
const THUSTR_7   = "level 7: prison"
const THUSTR_8   = "level 8: metal"
const THUSTR_9   = "level 9: stronghold"
const THUSTR_10  = "level 10: redemption"
const THUSTR_11  = "level 11: storage facility"

const THUSTR_12  = "level 12: crater"
const THUSTR_13  = "level 13: nukage processing"
const THUSTR_14  = "level 14: steel works"
const THUSTR_15  = "level 15: dead zone"
const THUSTR_16  = "level 16: deepest reaches"
const THUSTR_17  = "level 17: processing area"
const THUSTR_18  = "level 18: mill"
const THUSTR_19  = "level 19: shipping/respawning"
const THUSTR_20  = "level 20: central processing"

const THUSTR_21  = "level 21: administration center"
const THUSTR_22  = "level 22: habitat"
const THUSTR_23  = "level 23: lunar mining project"
const THUSTR_24  = "level 24: quarry"
const THUSTR_25  = "level 25: baron's den"
const THUSTR_26  = "level 26: ballistyx"
const THUSTR_27  = "level 27: mount pain"
const THUSTR_28  = "level 28: heck"
const THUSTR_29  = "level 29: river styx"
const THUSTR_30  = "level 30: last call"

const THUSTR_31  = "level 31: pharaoh"
const THUSTR_32  = "level 32: caribbean"
const THUSTR_33  = "level 33"
const THUSTR_34  = "level 34"
const THUSTR_35  = "level 35"

rom-var mapnames_tnt [35]^uchar =
	THUSTR_1  THUSTR_2  THUSTR_3  THUSTR_4  THUSTR_5
	THUSTR_6  THUSTR_7  THUSTR_8  THUSTR_9  THUSTR_10
	THUSTR_11 THUSTR_12 THUSTR_13 THUSTR_14 THUSTR_15
	THUSTR_16 THUSTR_17 THUSTR_18 THUSTR_19 THUSTR_20
	THUSTR_21 THUSTR_22 THUSTR_23 THUSTR_24 THUSTR_25
	THUSTR_26 THUSTR_27 THUSTR_28 THUSTR_29 THUSTR_30
	THUSTR_31 THUSTR_32 THUSTR_33 THUSTR_34 THUSTR_35
end

;
; HacX 1.2
;
const HACX_MAP01 = "GenEmp Corp."
const HACX_MAP02 = "Tunnel Town"
const HACX_MAP03 = "Lava Annex"
const HACX_MAP04 = "Alcatraz"
const HACX_MAP05 = "Cyber Circus"
const HACX_MAP06 = "Digi-Ota"
const HACX_MAP07 = "The Great Wall"
const HACX_MAP08 = "Garden of Delights"
const HACX_MAP09 = "Hidden Fort"

const HACX_MAP10 = "Anarchist Dream"
const HACX_MAP11 = "Notus Us!"
const HACX_MAP12 = "Gothik Gauntlet"
const HACX_MAP13 = "The Sewers"
const HACX_MAP14 = "'Trode Wars"
const HACX_MAP15 = "Twilight of Enk's"
const HACX_MAP16 = "Protean Cybex"
const HACX_MAP17 = "River of Blood"
const HACX_MAP18 = "Bizarro"
const HACX_MAP19 = "The War Rooms"

const HACX_MAP20 = "Intruder Alert!"
const HACX_MAP21 = "21"
const HACX_MAP22 = "22"
const HACX_MAP23 = "23"
const HACX_MAP24 = "24"
const HACX_MAP25 = "25"
const HACX_MAP26 = "26"
const HACX_MAP27 = "27"
const HACX_MAP28 = "28"
const HACX_MAP29 = "29"

const HACX_MAP30 = "30"
const HACX_MAP31 = "Desiccant Room"
const HACX_MAP32 = "32"
const HACX_MAP33 = "33"
const HACX_MAP34 = "34"
const HACX_MAP35 = "35"

rom-var mapnames_hacx [35]^uchar =
	HACX_MAP01 HACX_MAP02 HACX_MAP03 HACX_MAP04 HACX_MAP05
	HACX_MAP06 HACX_MAP07 HACX_MAP08 HACX_MAP09 HACX_MAP10
	HACX_MAP11 HACX_MAP12 HACX_MAP13 HACX_MAP14 HACX_MAP15
	HACX_MAP16 HACX_MAP17 HACX_MAP18 HACX_MAP19 HACX_MAP20
	HACX_MAP21 HACX_MAP22 HACX_MAP23 HACX_MAP24 HACX_MAP25
	HACX_MAP26 HACX_MAP27 HACX_MAP28 HACX_MAP29 HACX_MAP30
	HACX_MAP31 HACX_MAP32 HACX_MAP33 HACX_MAP34 HACX_MAP35
end
